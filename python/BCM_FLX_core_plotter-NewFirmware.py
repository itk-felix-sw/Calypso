#!/usr/bin/env python
#######################################################
# BCM plot run
# Ignacio.Asensi@cern.ch
# March 2022
#######################################################
import os
import sys
import json
import config
import argparse
import ROOT
import array
import re


ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch()
ROOT.gROOT.SetStyle("ATLAS")


parser=argparse.ArgumentParser()
parser.add_argument("-f","--file",default="sb.data.xml",help="database file")
parser.add_argument("-v","--verbose",help="verbose",action='store_true')
parser.add_argument("-o","--outputdir",help="Output directory",default=".")


args=parser.parse_args()
IsROOT=False;
rInFile=None
file1= None
lines= None
#Fist try if input file is root file or text file ("Can handle both")
try:
    if ".root" in args.file:
        rInFile=ROOT.TFile(args.file)
        IsROOT=True
    else:
        file1= open(args.file, 'r')
        lines= file1.readlines()
except:
    file1= open(args.file, 'r')
    lines= file1.readlines()
    


outputdir=args.outputdir
 
filename=args.file if "/" not in args.file else args.file.split("/")[-1]
rf=outputdir+"/root_core_"+filename+".root"
hit_counter= 0
limit= 999999
v=args.verbose


InTOA=array.array('i',(0,))
InTOT=array.array('i',(0,))
InBCID=array.array('i',(0,))
InL1ID=array.array('i',(0,))
InL1SubID=array.array('i',(0,))

InData=None
if IsROOT:
    InData=rInFile.Get("data")
    InData.SetBranchAddress("ToA",InTOA)
    InData.SetBranchAddress("ToT",InTOT)
    InData.SetBranchAddress("BCID",InBCID)
    InData.SetBranchAddress("L1ID",InL1ID)
    InData.SetBranchAddress("L1SubID",InL1SubID)


# fw=ROOT.TFile(rf, "CREATE")
# ntuple = ROOT.TTree("tree","")
# TOA = array.array('i',(0,))
# TOT = array.array('i',(0,))
# SubL1ID = array.array('i',(0,))
# ntuple.Branch("TOA",TOA,"TOA/i")        
# ntuple.Branch("TOT",TOT,"TOT/i")
# ntuple.Branch("SubL1ID",SubL1ID,"SubL1ID/i")

#c1.SetLogy()
#Legend=ROOT.TLegend(0.9,0.9,0.7,0.1)
Data=[]
a_TOT=[]
a_TOA=[]
a_L1Sub=[]
maxTOA=0
maxTOT=0
minTOA=999999
minTOT=999999
startOn=1
lineCounter=0
lastBCID=0
SameBCIDCount=0
MaxBCID=3562
if v : print ("Getting the Data")
if IsROOT:
    NEntries=InData.GetEntries()
    for ind in range(0,NEntries):
        InData.GetEntry(ind)


        m_TOT=InTOT[0]
        m_TOA=InTOA[0]
        m_BCID=InBCID[0]
        m_L1Sub=InL1SubID[0]

        if (m_TOT==0 and m_TOA==0): continue
        
        FoundMatching=False;
        if m_TOA==0:
            PrevInd=-1;
            try:
                if Data[PrevInd][2] ==  (m_BCID-1 + (MaxBCID+1)*(m_BCID==0)) and (Data[PrevInd][0]+Data[PrevInd][1])==32:

                    #print(m_BCID , Data[PrevInd][2], m_TOA, m_TOT, Data[PrevInd][0], (Data[PrevInd][1]), PrevInd)
                    Data[PrevInd][1] += m_TOT

                    FoundMatching=True;
            except:
                continue;

        if not FoundMatching:

            if lastBCID != m_BCID:
                lastBCID=m_BCID
                SameBCIDCount=0
                Data.append([m_TOA,m_TOT,m_BCID,m_L1Sub])
            else:
                SameBCIDCount+=1

                Data.append([m_TOA,m_TOT,m_BCID,m_L1Sub])



else:
    with open(args.file, 'r') as file:
        contents = file.read()
        for l in contents.split('\n'):
            #print(l)
            lineCounter+=1
            if startOn>lineCounter:continue
            hit_counter += 1
            if limit == hit_counter: break
            if "," not in l or "CSV" in l: continue
            if v: print(l)
            try:
                m_TOT=int(l.split(",")[0])
                m_TOA=int(l.split(",")[1])
                m_BCID=int(l.split(",")[2])
                pass
            except:
                print("Error in %s" % l)
                pass
            #if m_TOA<2000: print("Low TOA [%i] at line: [%s]" % (m_TOA,l))


            ## Checking for events that cross BCID's

            FoundMatching=False;
            if m_TOA==0:
                PrevInd=-1;
                try:
                    if Data[PrevInd][2] ==  (m_BCID-1 + (MaxBCID+1)*(m_BCID==0)) and (Data[PrevInd][0]+Data[PrevInd][1])==32:

                        #print(m_BCID , Data[PrevInd][2], m_TOA, m_TOT, Data[PrevInd][0], (Data[PrevInd][1]), PrevInd)
                        Data[PrevInd][1] += m_TOT

                        FoundMatching=True;
                except:
                    continue;

            if not FoundMatching:

                if lastBCID != m_BCID:
                    lastBCID=m_BCID
                    SameBCIDCount=0
                    Data.append([m_TOA,m_TOT,m_BCID])
                else:
                    SameBCIDCount+=1

                    Data.append([m_TOA,m_TOT,m_BCID])

if v: print ("ReCalculating ToA")

for ind in range(0,len(Data)):
    try:
        Data[ind].append(Data[ind][0])
        Data[ind][0] += ( (MaxBCID+1)*(Data[ind][2]<Data[ind-1][2]) + Data[ind][2]-Data[ind-1][2])*32  - Data[ind-1][3]
    except:
        continue


if v: print ("Making plots")
### Time to process the events

for Datum in Data[1:]:
    
    
    
    m_TOA = Datum[0] 
    m_TOT = Datum[1] 
    m_L1Sub = Datum[3]
    
    # TOA[0]=m_TOA
    # TOT[0]=m_TOT
    a_TOA.append(m_TOA)
    a_TOT.append(m_TOT)
    a_L1Sub.append(m_L1Sub)

    if m_TOA>maxTOA: maxTOA=m_TOA
    if m_TOT>maxTOT: maxTOT=m_TOT
    if m_TOA<minTOA: minTOA=m_TOA
    if m_TOT<minTOT: minTOT=m_TOT

    # if (v) :
    #     print( m_TOA, m_TOT,Datum[2],Datum[3])
    # ntuple.Fill()

# ntuple.Write()
c1 = ROOT.TCanvas("cSummaryHist","cSummaryHist",800,600)
hist=ROOT.TH1D("hist","",maxTOA+20-minTOA,minTOA-10,maxTOA+10)
for x in a_TOA:
    hist.Fill(x)
    pass

Stats = hist.GetEntries()
ToAStd = hist.GetStdDev()
AvgToA = hist.GetMean()



hist.SetTitle("TOA  (%s)(%i hits); ToA (Bits [0.78 ns]); Number of Hits / 0.78 ns" % (filename, hit_counter))
hist.Draw()
#c1.SetLogy()
c1.Update()

c1.Draw("hist")
c1.SaveAs("%s/%s_TOA.pdf" % (outputdir,filename))
c1.Write("c1")

c2 = ROOT.TCanvas("c2SummaryHist","c2SummaryHist",800,600)
hist_e=ROOT.TH1D("hist_e","",maxTOT+20-minTOT,minTOT-10,maxTOT+10)
hist_eT=ROOT.TH1D("hist_eT","",maxTOT+20-minTOT,(minTOT-10)/1.28,(maxTOT+10)/1.28)
hist_e.SetTitle("TOT  (%s)(%i hits); ToT (Bits [0.78 ns]); Number of Hits / 0.78 ns" % (filename, hit_counter))
hist_eT.SetTitle("TOT  (%s)(%i hits); ToT [ns]; Number of hits / 0.78 ns" % (filename, hit_counter))
for x in a_TOT:
    hist_e.Fill(x)
    hist_eT.Fill(x/1.28)
    pass


ToTStd = hist_e.GetStdDev()
AvgToT = hist_e.GetMean()


hist_e.Draw()
#c2.SetLogy()
c2.Update()

c2.Draw("hist")
c2.SaveAs("%s/%s_TOT.pdf" % (outputdir,filename))
c2.Write("c2")

hist_eT.Draw()
#c2.SetLogy()
c2.Update()

c2.Draw("hist")
c2.SaveAs("%s/%s_TOT_Time.pdf" % (outputdir,filename))
c2.Write("c2_T")



c3 = ROOT.TCanvas("c3SummaryHist","c3SummaryHist",800,600)
hist_e=ROOT.TH1D("hist_e","",16,0,16)
hist_et=ROOT.TH1D("hist_et","",16,0,16*25)
hist_e.SetTitle("L1Sub  (%s)(%i hits); Time between Delayed Signal and Trigger (Bits [25 ns]); Number of hits / 25 ns" % (filename, hit_counter))
hist_et.SetTitle("L1Sub  (%s)(%i hits); Time between the delayed signal and trigger [ns]; Number of hits / 25 ns" % (filename, hit_counter))
for x in a_L1Sub:
    hist_e.Fill(x)
    hist_et.Fill(x*25)
    pass


hist_e.Draw()
c3.SetLogy()
c3.Update()
c3.Draw("hist")
c3.SaveAs("%s/%s_L1SubId.pdf" % (outputdir,filename))
c3.Write("c3")

hist_et.Draw()
c3.SetLogy()
c3.Update()
c3.Draw("hist")
c3.SaveAs("%s/%s_L1SubId_Time.pdf" % (outputdir,filename))
c3.Write("c3_Time")




# fw.Close()
#print(stream)
    
        
if v: print("Stats\t minTOT\t maxTOT\t AvgTOT\t TOTStd\t minTOA\t maxTOA\t AvgTOA\t TOAStd")
print("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % ( args.file, Stats, minTOT, maxTOT, AvgToT, ToTStd ,  minTOA, maxTOA, AvgToA, ToAStd))
