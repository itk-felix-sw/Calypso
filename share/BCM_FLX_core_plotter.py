#!/usr/bin/env python
#######################################################
# BCM plot run
# Ignacio.Asensi@cern.ch
# March 2022
#######################################################
import os
import sys
import json
import config
import argparse
import ROOT
import array
import re


parser=argparse.ArgumentParser()
parser.add_argument("-f","--file",default="sb.data.xml",help="database file")
parser.add_argument("-v","--verbose",help="verbose",action='store_true')
args=parser.parse_args()

file1= open(args.file, 'r')
lines= file1.readlines()
 
filename=args.file if "/" not in args.file else args.file.split("/")[-1]
rf="root_core_"+filename+".root"
hit_counter= 0
limit= 999999
v=args.verbose


fw=ROOT.TFile(rf, "RECREATE")
ntuple = ROOT.TTree("tree","")
TOA = array.array('i',(0,))
TOT = array.array('i',(0,))
ntuple.Branch("TOA",TOA,"TOA/i")        
ntuple.Branch("TOT",TOT,"TOT/i")

#c1.SetLogy()
#Legend=ROOT.TLegend(0.9,0.9,0.7,0.1)
a_TOA=[]
a_TOT=[]
maxTOA=0
maxTOT=0
minTOA=999999
minTOT=999999
startOn=50
lineCounter=0
with open(args.file, 'r') as file:
    contents = file.read()
    for l in contents.split('\n'):
        #print(l)
        lineCounter+=1
        if startOn>lineCounter:continue
        hit_counter += 1
        if limit == hit_counter: break
        if "," not in l or "CSV" in l: continue
        if v: print(l)
        try:
            m_TOA=int(l.split(",")[0])
            m_TOT=int(l.split(",")[1])
            pass
        except:
            print("Error in %s" % l)
            pass
        if m_TOA<2000: print("Low TOA [%i] at line: [%s]" % (m_TOA,l))
        TOA[0]=m_TOA
        TOT[0]=m_TOT
        a_TOA.append(m_TOA)
        a_TOT.append(m_TOT)
        if m_TOA>maxTOA: maxTOA=m_TOA
        if m_TOT>maxTOT: maxTOT=m_TOT
        if m_TOA<minTOA: minTOA=m_TOA
        if m_TOT<minTOT: minTOT=m_TOT
        if v: print("%i \t TOA[%i]\tTOT[%i]" % (hit_counter, m_TOA, m_TOT))
        ntuple.Fill()
    ntuple.Write()
    c1 = ROOT.TCanvas("cSummaryHist","cSummaryHist",800,600)
    hist=ROOT.TH1D("hist","",maxTOA+20-minTOA,minTOA-10,maxTOA+10)
    for x in a_TOA:
        hist.Fill(x)
        pass
    hist.SetTitle("TOA (%s)(%i hits);Bits;N" % (filename, hit_counter))
    hist.Draw()
    c1.Update()
    c1.Draw("hist")
    c1.SaveAs("%s_TOA.pdf" % filename)
    c1.Write("c1")
    c2 = ROOT.TCanvas("c2SummaryHist","c2SummaryHist",800,600)
    hist_e=ROOT.TH1D("hist_e","",maxTOT+20-minTOT,minTOT-10,maxTOT+10)
    hist_e.SetTitle("TOT  (%s)(%i hits);Bits;N" % (filename, hit_counter))
    for x in a_TOT:
        hist_e.Fill(x)
        pass
    hist_e.Draw()
    c2.Update()
    c2.Draw("hist")
    c2.SaveAs("%s_TOT.pdf" % filename)
    c2.Write("c2")
    input()
    fw.Close()
    #print(stream)
    
        
