#!/usr/bin/env python
#######################################################
# BCM plot run
# Ignacio.Asensi@cern.ch
# March 2022
#######################################################
import os
import sys
import json
import config
import argparse
import ROOT
import array
import re


parser=argparse.ArgumentParser()
parser.add_argument("-f","--file",default="sb.data.xml",help="database file")
parser.add_argument("-l","--bl",default=3,help="Blocks limit", type=int)
parser.add_argument("-fd","--fedump",help="fedump log",action='store_true')
parser.add_argument("-v","--verbose",help="verbose",action='store_true')
parser.add_argument("-s","--showplot",help="Show plots",action='store_true')
parser.add_argument("-z","--zeroes",help="Plot lenght of empty streams",action='store_true')

args=parser.parse_args()

displayr=not args.showplot

show_eb=True
eb_l=500

# Using readlines()
file1= open(args.file, 'r')
lines= file1.readlines()
 
filename=args.file if "/" not in args.file else args.file.split("/")[-1]
rf="root_"+filename+".root"
block_counter= 0
limit= args.bl
v=args.verbose
pz=args.zeroes

fw=ROOT.TFile(rf, "RECREATE")
ntuple = ROOT.TTree("IV","")
a_zL = array.array('i',(0,))
a_bL = array.array('i',(0,))
ntuple.Branch("a_zL",a_zL,"a_zL/i")        
ntuple.Branch("a_bL",a_bL,"a_bL/i")

#c1.SetLogy()
#Legend=ROOT.TLegend(0.9,0.9,0.7,0.1)
rdp=[]
vsblock=[]# vector of blocks as strings
pulses1=[]
bL=[]
zL=[]
max_z=0
zeroeBytes_count=0
Bnum=0
stream=""
with open(args.file, 'r') as file:
    contents = file.read()
    for b in contents.split('databytes\n'):
        block_counter += 1
        if limit == block_counter: break
        if block_counter==1 or "&&T" in b :continue
        dp=b[:3111]#.split(" ")
        dp=dp.replace(" \n","")
        if v: print(len(dp))
        sblock=""
        Bc=0
        for x in range(0,3048,3):
            s=x
            e=x+3
            try:
                num=int(dp[s:e],16)
            except:
                print("Error at [%i]\tformating: [%s]" % (x,dp[s:e]))
                continue
            Bc+=1
            number_of_bits=bin(num).count("1")
            sblock+=format(num, '08b') if format(num, '08b')!="00000000" else "_"
            stream+=format(num, '08b')
            pass
        pass
    #Legend.Draw("hIvsV")
    ntuple.Write()
    prev=stream[0]
    zcount=0
    icount=0
    zsizes=[]
    isizes=[]
    maxz=0
    print("Done reading data. Fill root")
    for b in stream:
        if b=="0": 
            zcount+=1
            if prev=="1":
                isizes.append(icount) 
                a_bL[0]=icount
            icount=0
        if b=="1":
            icount+=1
            if prev=="0":
                zsizes.append(zcount)
                if zcount>maxz: maxz=zcount
                a_zL[0]=zcount
            zcount=0
        prev=b
        ntuple.Fill()
    ntuple.Write()
    if v:
        print(zsizes)
        print(isizes)
        print("Pulses lengths")
        print(bL)
        print("Unique zeroes distances:")
        show=[]
        for x in zsizes:
            if x not in show:
                show.append(x)
        print(show)    
    if displayr:
        c1 = ROOT.TCanvas("cSummaryHist","cSummaryHist",800,600)
        hist=ROOT.TH1D("hist","",10,0,10)
        for x in isizes:
            hist.Fill(x)
        hist.SetTitle("Pulse lenght (%s)(%i blocks);Bits;N" % (filename, block_counter))
        hist.Draw()
        c1.Update()
        c1.Draw("hist")
        c1.SaveAs("%s.pdf" % filename)
        c1.Write("c1")
        if pz:#Empty spaces
            c2 = ROOT.TCanvas("c2SummaryHist","c2SummaryHist",800,600)
            hist_e=ROOT.TH1D("hist_e","",300,0,maxz+50)
            hist_e.SetTitle("Lenght between data  (%s)(%i blocks);Bits;N" % (filename, block_counter))
            for x in zsizes:
                hist_e.Fill(x)
            hist_e.Draw()
            c2.Update()
            c2.Draw("hist")
            c2.SaveAs("%s_e.pdf" % filename)
            c2.Write("c2")
        input()
    fw.Close()
    #print(stream)
    
        
