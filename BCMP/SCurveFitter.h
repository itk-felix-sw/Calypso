#ifndef FITTER_SCURVEFITTER_H
#define FITTER_SCURVEFITTER_H

#include "BCMP/ErrorFunction.h"
#include "TH1.h"


namespace Fitter {

  /**
     Class to perform s-curve fits using the LM algorithm
     Designed by Brain Mosser for ITk Pixels
   **/

  struct SCurve {
    double mu;
    double sigma;
    double norm;
  };

  struct LMstat{
    int verbose;
    int max_it;
    double init_lambda;
    double up_factor;
    double down_factor;
    double target_derr;
    int final_it;
    double final_err;
    double final_derr;
  };


  class SCurveFitter {
  
  private:
    ErrorFunction m_erf;
    SCurve m_curve;
    LMstat m_lmstat;
    double m_chi2;
    int m_fit_status;
    double* m_xvals;
    double* m_yvals;
    double* m_pars;
    unsigned int m_npoints;
    double getClosest(double v1, double v2, double t);
    int findClosestPos(double* arr, int n, double target);
    double getFunctionValue(double x, double* par);
    void getDerivatives(double x, double* par, double*g );
    //FIXME this is very hardcoded and ugly
    void solve_axb_cholesky(int n, double l[3][3], double *x, double *b);
    //FIXME this one as well
    int cholesky_decomp(int n, double l[3][3], double a[3][3]);
    void levmarq_init();
    double chi2();

  public:
    SCurveFitter();
    ~SCurveFitter();
    void setDataFromHist(TH1* h);
    void setDataFromHistIgnoreBin(TH1* h, float skipbin);
    void guessInitialParameters();
    SCurve getSCurve(){
      return m_curve;
    };
    int fit();

  };

}
#endif
