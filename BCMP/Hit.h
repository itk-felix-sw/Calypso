#ifndef BCMP_HIT_H
#define BCMP_HIT_H

#include <cstdint>

namespace BCMP{

  class Hit{

  public:
    Hit();
    Hit(uint8_t toa, uint8_t tot, uint16_t BCID=0, uint32_t L1ID=0, uint8_t L1SubID=0);
    ~Hit();
    Hit * Clone();
    uint8_t GetTOT();
    uint8_t GetTOA();
    uint16_t GetBCID();
    uint32_t GetL1ID();
    uint8_t GetL1SubID();
    
    void SetTOT(uint8_t tot);
    void SetTOA(uint8_t toa);
    void SetBCID(uint16_t BCID);
    void SetL1ID(uint32_t L1ID);
    void SetL1SubID(uint8_t L1SubID);
    void Update(uint8_t toa, uint8_t tot, uint16_t BCID=0, uint32_t L1ID=0, uint8_t L1SubID=0);
  private:
    uint8_t m_tot;
    uint8_t m_toa;
    uint16_t m_BCID;
    uint32_t m_L1ID;
    uint8_t m_L1SubID;
  };

}

#endif 
