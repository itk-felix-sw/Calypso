#ifndef BCMP_FRONTEND_H
#define BCMP_FRONTEND_H

#include "BCMP/Sensor.h"
//#include "BCMP/Configuration_C.h"
#include "BCMP/Configuration_D.h"
#include <cstdint>
#include <vector>
#include <string>




namespace BCMP{


  using Configuration = Configuration_D;
  //using Configuration = Configuration_C;
  
  
  class FrontEnd{
    
  public:
    
    FrontEnd();

    ~FrontEnd();

    void SetVerbose(bool enable);

    void SetName(std::string name);

    std::string GetName();

    Configuration * GetConfig();
    
    void SetConfig(std::map<std::string,uint32_t> config);

    bool IsActive();

    void SetActive(bool active);

    void EnableAll();

    void DisableAll();

    Sensor * GetSensor(uint32_t index);

    void SetSensor(uint32_t index, Sensor *sens);


    void ReadLpGBTRegisters(uint16_t Reg);
    void WriteRegisters();
    void WriteToCalypso(uint8_t regInd, uint16_t Data, uint8_t I2C);
    void EnableChan(bool enable, unsigned chan);
    void Encode();
    std::vector<uint8_t> prepareICNetioFrame(const bool read, const uint16_t startAddr, const std::vector<uint8_t>& data);

    uint32_t GetLength();
    uint32_t GetSeqLength(unsigned);
    uint8_t  *GetBytes(unsigned);

    void SetLpGBTi2c(uint16_t i2c);
    void SetLpGBTi2cMaster(uint16_t i2c);
    void SetCalypsoi2c(uint16_t i2c);
    void SetLpGBTVer(uint16_t ver);

    uint16_t GetLpGBTi2c();
    uint16_t GetLpGBTi2cMaster();
    uint16_t GetCalypsoi2c();
    uint8_t GetLpGBTVer();

    void HandleData(const uint8_t * recv_data, const uint32_t recv_size);
    size_t GetReplySize();
    uint8_t  GetReply(unsigned );
    void ClearReplies();
    

  private:

    std::string m_name;
    bool m_active;
    bool m_verbose;
    Configuration * m_config;
    Sensor *m_sensors[4];

    std::vector< std::pair< uint16_t, int16_t > > m_LpGBTRegisters;
    std::vector<std::vector<uint8_t> > m_bytes;
    std::vector<uint8_t> m_replies;


    uint16_t m_LpGBT_ULDATASOURCE;
    
    uint8_t m_LpGBTVer;
    uint16_t m_LpGBTi2cAddr;
    uint16_t m_LpGBTi2cMaster;
    uint16_t m_BCMi2cAddr;

    

  };

}

#endif 
