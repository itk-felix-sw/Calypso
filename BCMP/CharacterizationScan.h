#ifndef BCMP_THRESHOLDSCAN_H
#define BCMP_THERSHOLDSCAN_H
#include "BCMP/Handler.h"
#include "BCMP/HitTree.h"
#include "TH2I.h"
#include "TH1D.h"
#include "TH1I.h"
#include "TTree.h"

namespace BCMP{

  /**
   * This is a test scan. It is used to debug the code.
   *
   * @brief BCMP CharacterizationScan
   * @author ismet.siral@cern.ch
   * @author Ignacio.Asensi@cern.ch
   * @author Carlos.Solans@cern.ch
   * @date April 2022
   */

  class CharacterizationScan: public Handler{

  public:

    /**
     * Create the scan
     */
    CharacterizationScan();

    /**
     * Delete the scan
     */
    virtual ~CharacterizationScan();

    /**
     * Set the value of the variables for this scan, create histograms.
     */
    virtual void PreRun();

    /**
     * Set the lower edge for the threshold for threshold scan, this is done to mitigate noise range.
     */
    virtual void SetLowThreshold(unsigned Thr);
    

    
    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger.
     * An occupancy histogram is filled.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     */
    virtual void Analysis();



  private:

    //Tree
    std::map<std::string,std::vector<HitTree*>> m_hits;
    std::map<std::string,std::vector<uint64_t>> m_NHeaders;
    
    std::string m_variable_thr, m_variable_atten;
    uint16_t m_start_thr, m_stop_thr, m_step_thr, m_step_thr_hg, m_nsteps_thr;
    uint16_t m_start_atten, m_stop_atten, m_step_atten, m_nsteps_atten;
    unsigned m_ntriggers; 
  };

}

#endif
