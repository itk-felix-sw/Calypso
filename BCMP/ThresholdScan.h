#ifndef BCMP_THRESHOLDSCAN_H
#define BCMP_THERSHOLDSCAN_H
#include "BCMP/Handler.h"
#include "BCMP/HitTree.h"
#include "TH2I.h"
#include "TH1D.h"
#include "TH1I.h"
#include "TTree.h"

namespace BCMP{

  /**
   * This is a test scan. It is used to debug the code.
   *
   * @brief BCMP ThresholdScan
   * @author ismet.siral@cern.ch
   * @author Ignacio.Asensi@cern.ch
   * @author Carlos.Solans@cern.ch
   * @date April 2022
   */

  class ThresholdScan: public Handler{

  public:

    /**
     * Create the scan
     */
    ThresholdScan();

    /**
     * Delete the scan
     */
    virtual ~ThresholdScan();

    /**
     * Set the value of the variables for this scan, create histograms.
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger.
     * An occupancy histogram is filled.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     */
    virtual void Analysis();

    static void StopScan(int);


  private:
    uint32_t m_ntrigs;
    std::map<std::string,TH1I*> m_h_tot_real;
    std::map<std::string,TH1I*> m_h_tot_raw;
    std::map<std::string,TH1I*> m_h_toa_real;
    std::map<std::string,TH1I*> m_h_toa_raw;
    std::map<std::string,TH1I*> m_h_bcid;
    std::map<std::string,TH1D*> m_h_L1ID;
    std::map<std::string,TH1I*> m_h_L1SubID;

    //Tree
    std::map<std::string,std::vector<HitTree*>> m_hits;
    std::map<std::string,std::vector<uint64_t>> m_NHeaders;
    
    std::string m_variable;
    uint16_t m_start, m_stop, m_step;
    uint16_t m_nsteps;
    uint32_t m_longdelay, m_shortdelay;
    unsigned m_ntriggers; 
    std::vector<std::string> special_regs;
    static bool m_run_forever;
  };

}

#endif
