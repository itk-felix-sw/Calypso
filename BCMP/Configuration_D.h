#ifndef ITK_CONFIGURATION_H
#define ITK_CONFIGURATION_H

#include "BCMP/Field.h"
#include "BCMP/Register.h"
#include <map>

#include <vector>
#include <array>
#include <string>









/**
 * The Configuration of the ITk is contained in a Register tables/
 * A Field is associated to a number of bits of a Register. 
 * The behavior of the ITk is determined by the values of the registers.
 * The default value is the same one as on the real RD53B.
 * 
 * Each Register can be accessed (Configuration::GetRegister, Configuration::SetRegister),
 * and also each Field (Configuration::GetField). 
 * Note the index to access a given Field is given by Configuration_D::FieldType.
 *
 * @brief ITk Configuration_D
 * @author Carlos.Solans@cern.ch
 * @author Ismet.Siral@cern.ch
 * @date  2024
 **/
  
class Configuration_D{
    


public:
  /**
   * Create a new configuration object. Initialize the fields to their default values.
   **/
  Configuration_D();

  /**
   * Coppy of configuration object
   **/  
  Configuration_D( const Configuration_D & obj);
    
  /**
   * Clear internal arrays
   **/
  ~Configuration_D();

  /**
   * Enable the verbose mode
   * @param enable Enable the verbose mode if true
   */
  void SetVerbose(bool enable);

  /**
   * Get the number of registers in the configuration
   * @return The configuration size
   **/
  uint32_t Size();

  /**
   * Get the register object corresponsing to the given index
   * @param index Register index
   * @return The register object
   **/
  Register<uint16_t> * GetRegister(uint32_t index);

  /**
   * Get the whole 16-bit value of a register
   * @param index Register index
   * @param update force the update of the register. Default false.
   * @return The 16-bit register value
   **/
  uint16_t GetRegisterValue(uint32_t index, bool update);

  /**
   * Get the whole 16-bit value of a register without updating, this is defined as const
   * @param index Register index
   * @return The 16-bit register value
   **/
  uint16_t GetRegisterValue(uint32_t index) const;

  /**
   * Set the whole 16-bit value of a register
   * @param index Register index
   * @param value The 16-bit register value
   **/
  void SetRegisterValue(uint32_t index, uint16_t value);

  /**
   * Set the Field value from a string. Note it is not necessarily the whole register
   * @param name Field name
   * @param value The new value
   **/
  void SetField(std::string name, uint16_t value);

  /**
   * Set the Field value from a string. Note it is not necessarily the whole register
   * @param index Field index
   * @param value The new value
   **/
  void SetField(uint32_t index, uint16_t value);

  /**
   * Get a Field inside the Configuration_D given
   * its Field index (ie: Configuration_D::GateHitOr).
   * @param index The Field index (Configuration_D::FieldType)
   * @return pointer to the field
   **/
  Field<uint16_t> * GetField(uint32_t index);

  /**
   * Get a Field inside the Configuration_D by name
   * @param name The Field name (Configuration_D::m_name2index)
   * @return pointer to the field
   **/
  Field<uint16_t> * GetField(std::string name);

  /**
   * Get list of addresses that have been updated
   * @return vector of addresses that have been updated
   **/
  std::vector<uint32_t> GetUpdatedRegisters();

  /**
   * Get map of addresses
   * @return map of strings to addresses
   **/
  std::map<std::string,uint32_t> GetFields();

  /**
   * Set all registers from the map
   * @return map of strings to addresses
   **/
  void SetFields(std::map<std::string,uint32_t> config);

  /**
   * Make a back-up copy of the registers.
   * This is called by the constructor and contains the default values
   * The back-up is restored by calling Configuration_D::Restore().
   **/
  void Backup();

  /**
   * Restore the back-up copy of the registers.
   * This updates the update flag of the registers.
   * Then the registers that have been updated can be retrieved with Configuration_D::GetUpdatedRegisters().
   * The back-up can be made anytime by calling Configuration_D::Backup().
   **/
  void Restore();

  static std::map<std::string, uint32_t> m_name2index; //Intentionally made public to be accesible by python Bindings
private:

  bool m_verbose;
  std::vector<uint16_t> m_backup;
  //LpGBT Register and their addresses
  std::array<Register<uint16_t> , 14> m_registers = {
    Register<uint16_t>(0),
    Register<uint16_t>(1),
    Register<uint16_t>(2),
    Register<uint16_t>(3),
    Register<uint16_t>(4),
    Register<uint16_t>(5),
    Register<uint16_t>(6),
    Register<uint16_t>(7),
    Register<uint16_t>(8),
    Register<uint16_t>(9),
    Register<uint16_t>(10),
    Register<uint16_t>(11),
    Register<uint16_t>(12),
    Register<uint16_t>(13)
  };


  //LpGBT Fields
  std::array<Field<uint16_t> , 96> m_fields = {
    Field<uint16_t>(&m_registers[  0],0, 1,0),
    Field<uint16_t>(&m_registers[  0],1, 1,0),
    Field<uint16_t>(&m_registers[  0],2, 1,0),
    Field<uint16_t>(&m_registers[  0],3, 1,0), 
    Field<uint16_t>(&m_registers[  0],4, 1,0),
    Field<uint16_t>(&m_registers[  0],5, 1,0),
    Field<uint16_t>(&m_registers[  0],6, 1,0),   
    Field<uint16_t>(&m_registers[  0],7, 1,0), 
    Field<uint16_t>(&m_registers[  0],8, 1,0),
    Field<uint16_t>(&m_registers[  0],9, 1,0),
    Field<uint16_t>(&m_registers[  0],10, 1,0),
    Field<uint16_t>(&m_registers[  0],11, 1,0), 
    Field<uint16_t>(&m_registers[  0],12, 1,0),
    Field<uint16_t>(&m_registers[  0],13, 1,0),
    Field<uint16_t>(&m_registers[  0],14, 1,0),
    Field<uint16_t>(&m_registers[  0],15, 1,0), 
    Field<uint16_t>(&m_registers[  1],0, 1,0),
    Field<uint16_t>(&m_registers[  1],1, 1,0),
    Field<uint16_t>(&m_registers[  1],2, 1,0),
    Field<uint16_t>(&m_registers[  1],3, 1,0), 
    Field<uint16_t>(&m_registers[  1],4, 1,0),
    Field<uint16_t>(&m_registers[  1],5, 1,0),
    Field<uint16_t>(&m_registers[  1],6, 1,0),
    Field<uint16_t>(&m_registers[  1],7, 1,0), 
    Field<uint16_t>(&m_registers[  1],8, 1,0),
    Field<uint16_t>(&m_registers[  1],9, 1,0),
    Field<uint16_t>(&m_registers[  1],10, 1,0),
    Field<uint16_t>(&m_registers[  1],11, 1,0), 
    Field<uint16_t>(&m_registers[  1],12, 1,0),
    Field<uint16_t>(&m_registers[  1],13, 1,0),
    Field<uint16_t>(&m_registers[  1],14, 1,0),
    Field<uint16_t>(&m_registers[  1],15, 1,0), 
    Field<uint16_t>(&m_registers[  2], 0, 2,0),
    Field<uint16_t>(&m_registers[  2], 3, 1,0),
    Field<uint16_t>(&m_registers[  2], 4, 1,0),
    Field<uint16_t>(&m_registers[  2], 5, 1,0),
    Field<uint16_t>(&m_registers[  2], 6, 1,0),
    Field<uint16_t>(&m_registers[  2], 7, 1,0),
    Field<uint16_t>(&m_registers[  2], 8, 1,0),
    Field<uint16_t>(&m_registers[  2], 9, 1,0),
    Field<uint16_t>(&m_registers[  2],10, 1,0),
    Field<uint16_t>(&m_registers[  2],11, 4,0),
    Field<uint16_t>(&m_registers[  2],15, 1,0),
    Field<uint16_t>({{&m_registers[  3], 0, 2}, {&m_registers[  5], 0, 2}},0),
    Field<uint16_t>(&m_registers[  3], 2, 6,0),
    Field<uint16_t>(&m_registers[  3], 7, 1,0),
    Field<uint16_t>(&m_registers[  3], 8, 1,0),
    Field<uint16_t>(&m_registers[  3], 9, 4,0),
    Field<uint16_t>(&m_registers[  3],13, 2,0),
    Field<uint16_t>(&m_registers[  3],15, 1,0),

    Field<uint16_t>(&m_registers[  4], 0, 1,0),
    Field<uint16_t>(&m_registers[  4], 1, 1,0),
    Field<uint16_t>(&m_registers[  4], 2, 1,0),
    Field<uint16_t>(&m_registers[  4], 3, 1,0),
    Field<uint16_t>({{&m_registers[12], 12, 1}, {&m_registers[  4], 4, 7}}, 0),
    Field<uint16_t>(&m_registers[  4],11, 5,0),
    Field<uint16_t>(&m_registers[  5], 2, 6,0),
    Field<uint16_t>(&m_registers[  5], 7, 1,0),
    Field<uint16_t>(&m_registers[  5], 8, 1,0),
    Field<uint16_t>(&m_registers[  5], 9, 4,0),
    Field<uint16_t>(&m_registers[  5],13, 2,0),
    Field<uint16_t>(&m_registers[  5],15, 1,0),
    Field<uint16_t>(&m_registers[  6], 0, 1,0),
    Field<uint16_t>(&m_registers[  6], 1, 1,0),
    Field<uint16_t>(&m_registers[  6], 2, 1,0),
    Field<uint16_t>(&m_registers[  6], 3, 1,0),
    Field<uint16_t>({{&m_registers[12], 13, 1}, {&m_registers[  6], 4, 7}}, 0),
    Field<uint16_t>(&m_registers[  6],11, 5,0),
    Field<uint16_t>(&m_registers[  7], 2, 6,0),
    Field<uint16_t>(&m_registers[  7], 7, 1,0),
    Field<uint16_t>(&m_registers[  7], 8, 1,0),
    Field<uint16_t>(&m_registers[  7], 9, 4,0),
    Field<uint16_t>(&m_registers[  7],13, 2,0),
    Field<uint16_t>(&m_registers[  7],15, 1,0),
    Field<uint16_t>(&m_registers[  8], 0, 1,0),
    Field<uint16_t>(&m_registers[  8], 1, 1,0),
    Field<uint16_t>(&m_registers[  8], 2, 1,0),
    Field<uint16_t>(&m_registers[  8], 3, 1,0),
    Field<uint16_t>({{&m_registers[12], 14, 1},{&m_registers[  8], 4, 7}}, 0),
    Field<uint16_t>(&m_registers[  8],11, 5,0),
    Field<uint16_t>(&m_registers[  9], 2, 6,0),
    Field<uint16_t>(&m_registers[  9], 7, 1,0),
    Field<uint16_t>(&m_registers[  9], 8, 1,0),
    Field<uint16_t>(&m_registers[  9], 9, 4,0),
    Field<uint16_t>(&m_registers[  9],13, 2,0),
    Field<uint16_t>(&m_registers[  9],15, 1,0),
    Field<uint16_t>(&m_registers[ 10], 0, 1,0),
    Field<uint16_t>(&m_registers[ 10], 1, 1,0),
    Field<uint16_t>(&m_registers[ 10], 2, 1,0),
    Field<uint16_t>(&m_registers[ 10], 3, 1,0),
    Field<uint16_t>({{&m_registers[12], 15, 1},{&m_registers[ 10], 4, 7}}, 0),
    Field<uint16_t>(&m_registers[ 10],11, 5,0),
    Field<uint16_t>(&m_registers[ 11],0, 4,0),
    Field<uint16_t>(&m_registers[ 11],14, 1,0),
    Field<uint16_t>(&m_registers[ 11],15, 1,1),
    Field<uint16_t>(&m_registers[ 12] ,0, 4,0)
  };



public:

  //ENUM Fields Include 1
  enum FieldType{
      LUMI_DIG_ENABLE_3,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_ANA_ENABLE_3,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_CFD_ENABLE_3,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_AMP_ENABLE_3,       /**< Address  0, bits 0x3 to 0x0 **/

      LUMI_DIG_ENABLE_2,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_ANA_ENABLE_2,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_CFD_ENABLE_2,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_AMP_ENABLE_2,       /**< Address  0, bits 0x3 to 0x0 **/

      LUMI_DIG_ENABLE_1,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_ANA_ENABLE_1,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_CFD_ENABLE_1,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_AMP_ENABLE_1,       /**< Address  0, bits 0x3 to 0x0 **/

      LUMI_DIG_ENABLE_0,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_ANA_ENABLE_0,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_CFD_ENABLE_0,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_AMP_ENABLE_0,       /**< Address  0, bits 0x3 to 0x0 **/

      ABORT_DIG_ENABLE_3,       /**< Address  0, bits 0x3 to 0x0 **/
      ABORT_ANA_ENABLE_3,       /**< Address  0, bits 0x3 to 0x0 **/
      ABORT_CFD_ENABLE_3,       /**< Address  0, bits 0x3 to 0x0 **/
      ABORT_AMP_ENABLE_3,       /**< Address  0, bits 0x3 to 0x0 **/

      ABORT_DIG_ENABLE_2,       /**< Address  0, bits 0x3 to 0x0 **/
      ABORT_ANA_ENABLE_2,       /**< Address  0, bits 0x3 to 0x0 **/
      ABORT_CFD_ENABLE_2,       /**< Address  0, bits 0x3 to 0x0 **/
      ABORT_AMP_ENABLE_2,       /**< Address  0, bits 0x3 to 0x0 **/

      ABORT_DIG_ENABLE_1,       /**< Address  0, bits 0x3 to 0x0 **/
      ABORT_ANA_ENABLE_1,       /**< Address  0, bits 0x3 to 0x0 **/
      ABORT_CFD_ENABLE_1,       /**< Address  0, bits 0x3 to 0x0 **/
      ABORT_AMP_ENABLE_1,       /**< Address  0, bits 0x3 to 0x0 **/

      ABORT_DIG_ENABLE_0,       /**< Address  0, bits 0x3 to 0x0 **/
      ABORT_ANA_ENABLE_0,       /**< Address  0, bits 0x3 to 0x0 **/
      ABORT_CFD_ENABLE_0,       /**< Address  0, bits 0x3 to 0x0 **/
      ABORT_AMP_ENABLE_0,       /**< Address  0, bits 0x3 to 0x0 **/

      


      LVDS_BIAS,           /**< Address  2, bits 0x1 to 0x0 **/
      POLARITY_3,          /**< Address  2, bits 0x3 **/
      POLARITY_2,          /**< Address  2, bits 0x4 **/
      POLARITY_1,          /**< Address  2, bits 0x5 **/
      POLARITY_0,          /**< Address  2, bits 0x6 **/
      TEST_ENABLE_3,       /**< Address  2, bits 0x7 **/
      TEST_ENABLE_2,       /**< Address  2, bits 0x8 **/
      TEST_ENABLE_1,       /**< Address  2, bits 0x9 **/
      TEST_ENABLE_0,       /**< Address  2, bits 0xA **/
      IBIAS_TRIM,          /**< Address  2, bits 0xE to 0xB **/
      IBIAS_ENABLE,        /**< Address  2, bits 0xF **/
      ATTENUATOR,          /**< Address  3, bits 0x1 to 0x0 **/ /**< Address  5, bits 0x1 to 0x0 **/

      ZDC_SCAP1_0,         /**< Address  3, bits 0x6 to 0x2 **/
      OC_LOOPPD_0,         /**< Address  3, bits 0x7 **/
      OC_GTPD_0,           /**< Address  3, bits 0x8 **/
      DELAY_CTRL_0,        /**< Address  3, bits 0xC to 0x9 **/
      AMP2GAIN_CTRL_0,     /**< Address  3, bits 0xE to 0xD **/
      RFBSHRT_0,           /**< Address  3, bits 0xF **/

      ADRVR_LOGAIN_0,      /**< Address  4, bits 0x0 **/
      ARMREF_DACZ_0,       /**< Address  4, bits 0x1 **/
      ARMREF_OVD_0,        /**< Address  4, bits 0x2 **/
      ARMHYSTPD_0,         /**< Address  4, bits 0x3 **/
      ARMREF_CTRL_0,       /**< Address  4, bits 0xA to 0x4 **/
      ZDC_SCAP2_0,         /**< Address  4, bits 0xF to 0xB **/

      ZDC_SCAP1_1,         /**< Address  5, bits 0x6 to 0x2 **/
      OC_LOOPPD_1,         /**< Address  5, bits 0x7 **/
      OC_GTPD_1,           /**< Address  5, bits 0x8 **/
      DELAY_CTRL_1,        /**< Address  5, bits 0xC to 0x9 **/
      AMP2GAIN_CTRL_1,     /**< Address  5, bits 0xE to 0xD **/
      RFBSHRT_1,           /**< Address  5, bits 0xF **/
      ADRVR_LOGAIN_1,      /**< Address  6, bits 0x0 **/
      ARMREF_DACZ_1,       /**< Address  6, bits 0x1 **/
      ARMREF_OVD_1,        /**< Address  6, bits 0x2 **/
      ARMHYSTPD_1,         /**< Address  6, bits 0x3 **/
      ARMREF_CTRL_1,       /**< Address  6, bits 0xA to 0x4 **/
      ZDC_SCAP2_1,         /**< Address  6, bits 0xF to 0xB **/

      ZDC_SCAP1_2,         /**< Address  7, bits 0x6 to 0x2 **/
      OC_LOOPPD_2,         /**< Address  7, bits 0x7 **/
      OC_GTPD_2,           /**< Address  7, bits 0x8 **/
      DELAY_CTRL_2,        /**< Address  7, bits 0xC to 0x9 **/
      AMP2GAIN_CTRL_2,     /**< Address  7, bits 0xE to 0xD **/
      RFBSHRT_2,           /**< Address  7, bits 0xF **/
      ADRVR_LOGAIN_2,      /**< Address  8, bits 0x0 **/
      ARMREF_DACZ_2,       /**< Address  8, bits 0x1 **/
      ARMREF_OVD_2,        /**< Address  8, bits 0x2 **/
      ARMHYSTPD_2,         /**< Address  8, bits 0x3 **/
      ARMREF_CTRL_2,       /**< Address  8, bits 0xA to 0x4 **/
      ZDC_SCAP2_2,         /**< Address  8, bits 0xF to 0xB **/

      ZDC_SCAP1_3,         /**< Address  9, bits 0x6 to 0x2 **/
      OC_LOOPPD_3,         /**< Address  9, bits 0x7 **/
      OC_GTPD_3,           /**< Address  9, bits 0x8 **/
      DELAY_CTRL_3,        /**< Address  9, bits 0xC to 0x9 **/
      AMP2GAIN_CTRL_3,     /**< Address  9, bits 0xE to 0xD **/
      RFBSHRT_3,           /**< Address  9, bits 0xF **/
      ADRVR_LOGAIN_3,      /**< Address  10, bits 0x0 **/
      ARMREF_DACZ_3,       /**< Address  10, bits 0x1 **/
      ARMREF_OVD_3,        /**< Address  10, bits 0x2 **/
      ARMHYSTPD_3,         /**< Address  10, bits 0x3 **/
      ARMREF_CTRL_3,       /**< Address  10, bits 0xA to 0x4 **/
      ZDC_SCAP2_3,         /**< Address  10, bits 0xF to 0xB **/

      ATTEN,               /**< Address 11, bits 0x0 to 0x4 **/
      VREF_SEL,            /**< Address 11, bits 14 to 14 **/
      VLOW_SEL,            /**< Address 11, bits 15 to 15 **/
      STEP,                /**< Address 12, bits 0x0 to 0x4 **/
      NONEFIELD
  };	       

    


    
  
};



#endif
