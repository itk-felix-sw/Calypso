#ifndef CALYPSO_H
#define CALYPSO_H

/**
 * This represents the configuration of a Calypso front-end ASIC
 * used by the BCMprime Luminosity and Abort detector in ATLAS
 * https://edms.cern.ch/document/2369649
 * Carlos.Solans@cern.ch
 **/
class Calypso{

 public:

  Calypso();

  ~Calypso();

  void EnableAmplifiers(bool enable);
  void EnableCFD(bool enable);
  void EnableAnalogDriver(bool enable);
  void EnableTimingDriver(bool enable);
  void EnableIbias(bool enable);
  void SetIbiasTrim(uint32_t value);
  void SetPulser(uint32_t channel, uint32_t value);
  void SetPolarity(uint32_t channel, uint32_t value);
  void SetRFBSHRT(uint32_t channel, bool enable);
  void SetAmp2Gain(uint32_t channel, uint32_t enable);
  void SetRFBSHRT(uint32_t channel, bool enable);

  /**
Gain control for the 2nd amplier. 0=> lowest gain , 3==> heighest gain.
  **/
  void chx_amp2gain();

  /**
   * Control the delay for CFD bipolar pulse shaping. 0=>min delay, 15==> max delay. Should be selected to minimize the output jitter 
   **/ 
  void chx_delay_ctrl();
  
  /**
   * Disable active signal's gating in the ZCD offset control loop. 0=> gating is enabled , 1=> gating is  disabled
   **/
  void chx_oc_gtpd();
  
  /**
   * diasable the entire ZCD offset calibration loop. 
   * 0=> loop is enabled ,  
   * 1=> loop is disabled
   **/
  void chx_oc_looppd();
  
  /**
   * They control the ZCD 2nd and third stages BW. They should be selected to minimize time walk.
   **/
  void chx_zcd_scap1();
  
  /**
   * They control the ZCD 2nd and third stages BW. They should be selected to minimize time walk.
   **/
  void chx_zcd_scap2();
  
  /**
   * Threshold level for arming amplifier. Here are some typical mismach free values: 
     * 16==> zero threshold, 
     * 32≈≈>1.25Ke, 
     * 64≈≈> 3.8Ke,
     * 127≈≈> 9Ke
     **/
    void chx_armref_ctrl();
    
    /**
     * Disable the hysteresis in the arming comparator. 
     * 0=> hysteresis are enabled 
     * 1=> hysteresis are  disabled
     **/
    void chx_armhystpd();

    /**
     * Enable an external threshold for the arming comparator.  
     * 0=> external threshold is blocked 
     * 1=> external threshold is conducted
     **/
    void chx_armref_ovd();

    /**
     * Block the internal threshold DAC. Must be set when using external reference. 
     * 0=> DAC threshold is conducted 
     * 1=> DAC threshold is blocked
     **/
    void chx_armref_dacz();
    
    /**
     * Reduce the gain of the analog 50 Ohm drive.
     * 0=> Driver gain is approximately 1 , 
     * 1=> Driver gain is reduced (~0.5)
     **/
    void chx_adrvr_logain();

 private:

};

#endif
