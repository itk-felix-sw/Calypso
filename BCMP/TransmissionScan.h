#ifndef BCMP_TRANSMISSIONSCAN_H
#define BCMP_TRANSMISSIONSCAN_H
#include "BCMP/Handler.h"
#include "BCMP/HitTree.h"
#include "TH2I.h"
#include "TH1D.h"
#include "TH1I.h"
#include "TTree.h"

namespace BCMP{

  /**
   * This is a test scan. It is used to debug the code.
   *
   * @brief BCMP TransmissionScan
   * @author ismet.siral@cern.ch
   * @author Ignacio.Asensi@cern.ch
   * @author Carlos.Solans@cern.ch
   * @date April 2022
   */

  class TransmissionScan: public Handler{

  public:

    /**
     * Create the scan
     */
    TransmissionScan();

    /**
     * Delete the scan
     */
    virtual ~TransmissionScan();

    /**
     * Set the value of the variables for this scan, create histograms.
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger.
     * An occupancy histogram is filled.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     */
    virtual void Analysis();


    /**
     * Method to call the stop of the scan from inside the loop
     * @param signal the signal thrown by the signal catch
     */
    static void StopScan(int);


  private:
    uint32_t m_ntrigs;
    std::map<std::string,TH1D*> m_h_L1ID;
    std::map<std::string,TH1I*> m_h_L1SubID;

    uint64_t m_MissedHitCount;
    uint64_t m_HitCount;
    
    static bool m_RunForever;

  };

}

#endif
