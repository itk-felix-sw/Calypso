#ifndef BCMP_TESTSCAN_H
#define BCMP_TESTSCAN_H
#include "BCMP/Handler.h"
#include "BCMP/HitTree.h"
#include "TH2I.h"
#include "TH1D.h"
#include "TH1I.h"
#include "TTree.h"

namespace BCMP{

  /**
   * This is a test scan. It is used to debug the code.
   *
   * @brief BCMP TestScan
   * @author ismet.siral@cern.ch
   * @author Ignacio.Asensi@cern.ch
   * @author Carlos.Solans@cern.ch
   * @date April 2022
   */

  class TestScan: public Handler{

  public:

    /**
     * Create the scan
     */
    TestScan();

    /**
     * Delete the scan
     */
    virtual ~TestScan();

    /**
     * Set the value of the variables for this scan, create histograms.
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger.
     * An occupancy histogram is filled.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     */
    virtual void Analysis();


    /**
     * Method to call the stop of the scan from inside the loop
     * @param signal the signal thrown by the signal catch
     */
    static void StopScan(int);

    /**
     * Method to enable or disable the pulsing
     * @param Enable/Disable values (bool)
     */
    void EnablePulse(bool);
    

  private:
    uint32_t m_ntrigs;
    std::map<std::string,TH1I*> m_h_tot_real;
    std::map<std::string,TH1I*> m_h_tot_raw;
    std::map<std::string,TH1I*> m_h_toa_real;
    std::map<std::string,TH1I*> m_h_toa_raw;
    std::map<std::string,TH1I*> m_h_bcid;
    std::map<std::string,TH1D*> m_h_L1ID;
    std::map<std::string,TH1I*> m_h_L1SubID;

    //Tree
    std::map<std::string,HitTree*> m_hits;
     
    static bool m_RunForever;
    bool m_EnablePulse;
  };

}

#endif
