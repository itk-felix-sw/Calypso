#ifndef BCMP_SENSOR_H
#define BCMP_SENSOR_H

#include "BCMP/DataFrame.h"
#include "BCMP/Decoder.h"
#include "BCMP/Hit.h"
#include <cstdint>
#include <mutex>
#include <queue>

namespace BCMP{

  class Sensor{

  public:

    Sensor();
    ~Sensor();
    void SetVerbose(bool enable);
    Hit * GetHit();
    bool NextHit();
    bool HasHits();


    void ClearNHeaders();
    void SetNHeaders(uint64_t);
    uint64_t GetNHeaders();
    

    /**
     * Clear more than one Hit from the FIFO.
     * If the max_hits is smaller than the maximum number of hits available it will only delete the ones available.
     * @param max_hits maximum number of hits to read
     **/
    void ClearHits(uint32_t max_hits);

    /**
     * Delete all the hits that are still in the FIFO
     */
    void ClearHits();



    /**
     * Check if the FIFO of Hits has and returns number of available hits.
     * @return Number of available hits.
     */
    uint32_t GetNHits();

    /**
     * Get more than one Hit from the FIFO as a vector of Hit pointers.
     * Ownership of the pointers is of the FrontEnd. The returned object can be deleted without problems.    
     * @param max_hits maximum number of hits to read
     * @return a vector of Hit pointers with the number of hits requested or less
     **/
    std::vector<Hit *> GetHits(uint32_t max_hits=100);



    void HandleData(const uint8_t * recv_data, const uint32_t recv_size);
    
    void SetName(std::string name);

    std::string GetName();

    bool IsActive();

    void SetActive(bool active);



  private:
    bool m_verbose;
    bool m_active;
    std::string m_name;
    
    Decoder * m_decoder;
    std::mutex m_mutex;    
    std::deque<Hit*> m_hits;
    uint64_t m_NHeaders;



  };

}

#endif
