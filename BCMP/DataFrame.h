#ifndef BCMP_DATAFRAME_H
#define BCMP_DATAFRAME_H

#include <cstdint>
#include <string>
#include <map>
#include <vector>

namespace BCMP{

  class DataFrame{

  public:
    
    DataFrame();
    DataFrame(uint32_t size, uint32_t toa, uint32_t tot);
    ~DataFrame();
    uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
    uint32_t Pack(uint8_t * bytes);
    std::string ToString();
    void SetSize(uint32_t size);
    void SetTOA(uint32_t toa);
    void SetTOT(uint32_t tot);
    uint32_t GetSize();
    uint32_t GetTOA();
    uint32_t GetTOT();
    
  private:
    uint32_t m_size;
    uint32_t m_toa;
    uint32_t m_tot;

    /**
     * Map of bitstream to TOT and TOA
     **/
    static std::map<uint32_t, std::vector<uint32_t> > m_masks;
  };

}

#endif
