#ifndef BCMP_HITTREE_H
#define BCMP_HITTREE_H

#include "BCMP/Hit.h"
#include "TTree.h"
#include <string>
#include <chrono>

namespace BCMP{

/**
 * HitTree is a tool to store RD53A Hit data as a ROOT TTree.
 * A new file can be openned with HitTree::Open.
 * New hits are added with HitTree::Add.
 * After adding the parameters, it is necessary
 * to call MaltaTree::Fill.
 *
 * Hits can be written like the following:
 *
 * @verbatim
   HitTree * ht = new HitTree();
   ht->Open("file.root","RECREATE");

   for(...){
     Hit * hit=...
     ht->Set(hit);
     ht->Fill();
   }

   ht->Close();
   delete ht;
   @endverbatim
   *
   * Hits can be read like the following:
   *
   * @verbatim
   HitTree * ht = new HitTree();
   ht->Open("file.root","READ");

   while(ht->Next()){
     Hit * hit = ht->Get();
     float timer = ht->GetTimer();
   }

   ht->Close();
   delete ht;
   @endverbatim
   *
   * @brief Tool to store Hit as a ROOT TTree.
   * @author Carlos.Solans@cern.ch
   * @date February 2022
   **/

class HitTree{

 public:

  /**
   * @brief Default constructor, emtpy.
   **/
  HitTree();

  /**
   * @brief Create an empty HitTree.
   **/
  HitTree(std::string name, std::string title);

  /**
   * @brief Delete the HitTree. Close any open file.
   **/
  ~HitTree();

  /**
   **/
  void Create(std::string name,std::string title);

  /**
   * @brief Write the tree
   **/
  void Write();

  /**
   * @brief Add the contents of Hit to the current entry.
   *        This also adds a time stamp (timer) to the entry.
   * @param hit The Hit that needs to be stored in the tree entry.
   **/
  void Fill(Hit * hit);

  /**
   * @brief Get the current entry as a Hit object.
   * @return A Hit object.
   **/
  Hit * Get();

  /**
   * @brief Get the timestamp of the entry.
   * @return The timestamp of the run number.
   **/
  float GetTimer();

  /**
   * @brief Move the internal pointer to the next entry in the tree.
   * @return 0 if there are no more entries to read, -1 if there was an error.
   **/
  int Next();


  /**
   * @brief return number of entries stored in tree
   * @return return number of entries stored in tree
   **/
  uint64_t GetEntries();

  /**
   * @brief Get a specific entry and return the Hit object
   * @return return number of entries stored in tree
   **/
  void GetEntry(Long64_t entry);


 private:

  std::string m_name;
  std::string m_title;
  TTree * m_tree;
  bool m_readonly;
  std::chrono::steady_clock::time_point m_t0;



  uint8_t m_ToT;
  uint8_t m_ToA;
  uint32_t m_BCID;
  uint32_t m_L1ID;
  uint32_t m_xL1ID;
  uint8_t m_L1SubID; 
  float    m_timer;

  Hit m_hit;
  uint64_t m_entry;
};

}

#endif
