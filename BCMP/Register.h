#ifndef ITK_REGISTER_H
#define ITK_REGISTER_H

#include <cstdint>


/**
 * Register Architecture for ITk-Online-Software
 *
 * @brief ITk Register
 * @author Carlos.Solans@cern.ch
 * @author Ismet.Siral@cern.ch
 * @date August 2024
 **/

/**
 * Template Class T is defined for Register data-type
 * This is required as each register type is different size
 **/
template<typename T = uint16_t>
class Register{

  public:

  /**
   * Create a new Register
   **/
  Register (uint32_t Address);

  /**
   * Delete the Register
   **/
  ~Register();

  /**
   * Set the new value of the Register
   **/
  void SetValue(T value);

  /**
   * Get Adress of the Register 
   * @return Return the Address of the register
   **/
  const uint32_t GetAddress();

  /**
   * Get the Value of the Register
   * @return The value of the register
   **/
  T GetValue() const;
  
  /**
   * Mark the Register as being updated
   * @param enable True if the Register has been updated
   **/
  void Update(bool enable=true);

  /**
   * Get if the Register has been updated
   * @return True if it has been updated
   */
  bool IsUpdated();

  private:

  const uint32_t m_Address;
  T m_value;
  bool m_updated;



  
};



#endif
