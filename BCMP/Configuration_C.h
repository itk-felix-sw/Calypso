#ifndef BCMP_CONFIGURATION_C_H
#define BCMP_CONFIGURATION_C_H

#include "BCMP/Field.h"
#include "BCMP/Register.h"

#include <iostream>
#include <map>
#include <vector>

namespace BCMP{

  /**
   * This class represents the configuration of Calypso that is stored in 11 16-bit registers.
   *
   * @brief BCMP Configuration_C
   * @author Carlos.Solans@cern.ch
   * @date March 2022
   **/

  class Configuration_C{
    
  public:

    enum FieldType{
      LUMI_ENABLE_0,       /**< Address  0, bits 0x3 to 0x0 **/
      LUMI_ENABLE_1,       /**< Address  0, bits 0x7 to 0x4 **/
      LUMI_ENABLE_2,       /**< Address  0, bits 0xB to 0x8 **/
      LUMI_ENABLE_3,       /**< Address  0, bits 0xF to 0xC **/
      ABORT_ENABLE_0,      /**< Address  1, bits 0x3 to 0x0 **/
      ABORT_ENABLE_1,      /**< Address  1, bits 0x7 to 0x0 **/
      ABORT_ENABLE_2,      /**< Address  1, bits 0xB to 0x0 **/
      ABORT_ENABLE_3,      /**< Address  1, bits 0xF to 0x0 **/
      LVDS_BIAS,           /**< Address  2, bits 0x1 to 0x0 **/
      POLARITY_0,          /**< Address  2, bits 0x3 **/
      POLARITY_1,          /**< Address  2, bits 0x4 **/
      POLARITY_2,          /**< Address  2, bits 0x5 **/
      POLARITY_3,          /**< Address  2, bits 0x6 **/
      TEST_ENABLE_0,       /**< Address  2, bits 0x7 **/
      TEST_ENABLE_1,       /**< Address  2, bits 0x8 **/
      TEST_ENABLE_2,       /**< Address  2, bits 0x9 **/
      TEST_ENABLE_3,       /**< Address  2, bits 0xA **/
      IBIAS_TRIM,          /**< Address  2, bits 0xE to 0xB **/
      IBIAS_ENABLE,        /**< Address  2, bits 0xF **/
      ATTENUATOR,          /**< Address  3, bits 0x1 to 0x0 **/ /**< Address  5, bits 0x1 to 0x0 **/
      ZDC_SCAP1_0,         /**< Address  3, bits 0x6 to 0x2 **/
      ZDC_SCAP1_1,         /**< Address  5, bits 0x6 to 0x2 **/
      ZDC_SCAP1_2,         /**< Address  7, bits 0x6 to 0x2 **/
      ZDC_SCAP1_3,         /**< Address  9, bits 0x6 to 0x2 **/
      OC_GTPD_0,           /**< Address  3, bits 0x8 **/
      OC_GTPD_1,           /**< Address  5, bits 0x8 **/
      OC_GTPD_2,           /**< Address  7, bits 0x8 **/
      OC_GTPD_3,           /**< Address  9, bits 0x8 **/
      OC_LOOPPD_0,         /**< Address  3, bits 0x7 **/
      OC_LOOPPD_1,         /**< Address  5, bits 0x7 **/
      OC_LOOPPD_2,         /**< Address  7, bits 0x7 **/
      OC_LOOPPD_3,         /**< Address  9, bits 0x7 **/
      DELAY_CTRL_0,        /**< Address  3, bits 0xC to 0x9 **/
      DELAY_CTRL_1,        /**< Address  5, bits 0xC to 0x9 **/
      DELAY_CTRL_2,        /**< Address  7, bits 0xC to 0x9 **/
      DELAY_CTRL_3,        /**< Address  9, bits 0xC to 0x9 **/
      AMP2GAIN_CTRL_0,     /**< Address  3, bits 0xE to 0xD **/
      AMP2GAIN_CTRL_1,     /**< Address  5, bits 0xE to 0xD **/
      AMP2GAIN_CTRL_2,     /**< Address  7, bits 0xE to 0xD **/
      AMP2GAIN_CTRL_3,     /**< Address  9, bits 0xE to 0xD **/
      RFBSHRT_0,           /**< Address  3, bits 0xF **/
      RFBSHRT_1,           /**< Address  5, bits 0xF **/
      RFBSHRT_2,           /**< Address  7, bits 0xF **/
      RFBSHRT_3,           /**< Address  9, bits 0xF **/
      ADRVR_LOGAIN_0,      /**< Address  4, bits 0x0 **/
      ADRVR_LOGAIN_1,      /**< Address  6, bits 0x0 **/
      ADRVR_LOGAIN_2,      /**< Address  8, bits 0x0 **/
      ADRVR_LOGAIN_3,      /**< Address 10, bits 0x0 **/
      ARMREF_DACZ_0,       /**< Address  4, bits 0x1 **/
      ARMREF_DACZ_1,       /**< Address  6, bits 0x1 **/
      ARMREF_DACZ_2,       /**< Address  8, bits 0x1 **/
      ARMREF_DACZ_3,       /**< Address 10, bits 0x1 **/
      ARMREF_OVD_0,        /**< Address  4, bits 0x2 **/
      ARMREF_OVD_1,        /**< Address  6, bits 0x2 **/
      ARMREF_OVD_2,        /**< Address  8, bits 0x2 **/
      ARMREF_OVD_3,        /**< Address 10, bits 0x2 **/
      ARMHYSTPD_0,         /**< Address  4, bits 0x3 **/
      ARMHYSTPD_1,         /**< Address  6, bits 0x3 **/
      ARMHYSTPD_2,         /**< Address  8, bits 0x3 **/
      ARMHYSTPD_3,         /**< Address 10, bits 0x3 **/
      ARMREF_CTRL_0,       /**< Address  4, bits 0xA to 0x4 **/
      ARMREF_CTRL_1,       /**< Address  6, bits 0xA to 0x4 **/
      ARMREF_CTRL_2,       /**< Address  8, bits 0xA to 0x4 **/
      ARMREF_CTRL_3,       /**< Address 10, bits 0xA to 0x4 **/
      ZDC_SCAP2_0,         /**< Address  4, bits 0xF to 0xB **/
      ZDC_SCAP2_1,         /**< Address  6, bits 0xF to 0xB **/
      ZDC_SCAP2_2,         /**< Address  8, bits 0xF to 0xB **/
      ZDC_SCAP2_3,         /**< Address 10, bits 0xF to 0xB **/
      NONEFIELD
    };

    static const uint32_t NUM_REGISTERS = 11;
    static const uint32_t REGISTER0 = 0;

    /**
     * Create a new configuration object. Initialize the fields to their default values.
     **/
    Configuration_C();

    /**
     * Clear internal arrays
     **/
    ~Configuration_C();

    /** 
     * Enable the verbose mode
     * @param enable Enable the verbose mode if true
     **/
    void SetVerbose(bool enable);

    /**
     * Get the number of registers in the configuration
     * @return The configuration size
     **/
    uint32_t Size();

    /**
     * Get the register object corresponsing to the given index
     * @param index Register index
     * @return The register object
     **/
    Register * GetRegister(uint32_t index);

    /**
     * Get the whole 16-bit value of a register
     * @param index Register index
     * @param update force the update of the register. Default false.
     * @return The 16-bit register value
     **/
    uint16_t GetRegisterValue(uint32_t index, bool update=false);

    /**
     * Set the whole 16-bit value of a register
     * @param index Register index
     * @param value The 16-bit register value
     **/
    void SetRegisterValue(uint32_t index, uint16_t value);

    /**
     * Set the Field value from a string. Note it is not necessarily the whole register
     * @param name Field name
     * @param value The new value
     **/
    void SetField(std::string name, uint16_t value);

    /**
     * Set the Field value from a string. Note it is not necessarily the whole register
     * @param index Field index
     * @param value The new value
     **/
    void SetField(uint32_t index, uint16_t value);

    /**
     * Get a Field inside the Configuration_C given
     * its Field index (ie: Configuration_C::GateHitOr).
     * @param index The Field index (Configuration_C::FieldType)
     * @return pointer to the field
     **/
    Field * GetField(uint32_t index);

    /**
     * Get a Field inside the Configuration_C by name
     * @param name The Field name (Configuration_C::m_name2field)
     * @return pointer to the field
     **/
    Field * GetField(std::string name);

    /**
     * Get list of addresses that have been updated
     * @return vector of addresses that have been updated
     **/
    std::vector<uint32_t> GetUpdatedRegisters();

    /**
     * Get map of addresses
     * @return map of strings to addresses
     **/
    std::map<std::string,uint32_t> GetFields();

    /**
     * Set map of addresses
     * @param fields map of strings to addresses
     **/
    void SetFields(std::map<std::string,uint32_t> fields);

  private:

    bool m_verbose;
    std::vector<Register> m_registers;
    std::map<uint32_t, Field*> m_fields;
    static std::map<std::string, uint32_t> m_name2field;
    std::vector<std::string> m_names;
  
  };

}

#endif
