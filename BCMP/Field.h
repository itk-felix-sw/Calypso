#ifndef ITK_FIELD_H
#define ITK_FIELD_H

#include <cstdint>
#include <vector>

#include "BCMP/Register.h"


/**
 * A Field contains the relevant Configuration bits for a particular setting.
 *
 * @brief ITk Configuration Field
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

template<typename T = uint16_t>
class Field{
	
public:

  
  struct Subs {
    Register<T> * Reg;
    uint32_t start;
    uint32_t len;
  };

  
  /**
   * Construct a Field given a pointer to the Configuration register.
   * @param data pointer to the Configuration register
   * @param start First bit of the data
   * @param len Length of the Field in bits
   * @param defval Default value for the Field
   * @param reversed Flip the bit order of the Field if true
   **/
  Field(Register<T> * data, uint32_t start, uint32_t len, uint32_t defval=0, bool reversed=false);


  /**
   * Construct a Field given a pointer to the Configuration register.
   * @param OtherFields, object containing list of data pointer to the Configuration register, start First bit of the data, len Length of the Field in bits 
   * @param defval Default value for the Field
   * @param reversed Flip the bit order of the Field if true
   **/

  Field(std::vector<Subs> OtherFields, uint32_t defval=0,bool reversed=false);

  
  /**
   * Empty Destructor
   **/
  ~Field();

  /**
   * Set the value of the Field. Extra bits will be truncated.
   * @param value New value of the Field
   **/
  void SetValue(uint32_t value);

  /**
   * Get the value of the Field
   * @return The value of the Field
   **/
  uint32_t GetValue();

  
private:

  std::vector<Subs> m_data;
  uint32_t m_defval;
  bool m_reversed;

  static std::vector<uint32_t> m_masks;
    
  uint32_t Reverse(uint32_t value, uint32_t sz);

};



#endif
