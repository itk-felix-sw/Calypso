flx-config set DECODING_BCM_PRIME_EMU_BCID=1
flx-config set DECODING_BCM_PRIME_ONLY_L1A=0
flx-config set DECODING_BCM_PRIME_PUBLISH_ZEROS=0
flx-config set TTC_EMU_SEL=0 
flx-config set TTC_EMU_ENA=0
flx-config set TTC_EMU_BCR_PERIOD=3563
flx-config set TTC_EMU_ECR_PERIOD=0
flx-config set TTC_EMU_L1A_PERIOD=0 
flx-config set TTC_DEC_CTRL_TT_BCH_EN=0x00


# flx-config set DECODING_BCM_PRIME_EMU_BCID=0 
# flx-config set DECODING_BCM_PRIME_ONLY_L1A=1 # Trigger only if L1A is available
# flx-config set DECODING_BCM_PRIME_PUBLISH_ZEROS=1 # Publush the data, even if it's empty
# flx-config set TTC_EMU_SEL=1 
# flx-config set TTC_EMU_ENA=1
# flx-config set TTC_EMU_BCR_PERIOD=3563
# flx-config set TTC_EMU_ECR_PERIOD=0
# flx-config set TTC_EMU_L1A_PERIOD=0  # frequency that L1A will be triggered 
# flx-config set TTC_DEC_CTRL_TT_BCH_EN=0x00


flx-config set DECODING_BCM_PRIME_LINK_00_L1A_DELAY=0
flx-config set DECODING_BCM_PRIME_LINK_00_L1A_WINDOW=63
flx-config set DECODING_BCM_PRIME_LINK_01_L1A_DELAY=0
flx-config set DECODING_BCM_PRIME_LINK_01_L1A_WINDOW=63
flx-config set DECODING_BCM_PRIME_LINK_02_L1A_DELAY=0
flx-config set DECODING_BCM_PRIME_LINK_02_L1A_WINDOW=63
flx-config set DECODING_BCM_PRIME_LINK_03_L1A_DELAY=0
flx-config set DECODING_BCM_PRIME_LINK_03_L1A_WINDOW=63
