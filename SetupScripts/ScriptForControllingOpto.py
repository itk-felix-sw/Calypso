#!/usr/bin/env python

## Libraries required for optoBoard
from collections import OrderedDict
from optoboard_felix.driver.Optoboard import *
from optoboard_felix.InitOpto.InitOpto import *
from optoboard_felix.driver.CommWrapper import CommWrapper
from optoboard_felix.driver.components import components
from optoboard_felix.driver.log import logger
from optoboard_felix.additional_driver.DB_access import configDbUnavailable, getConfigDataset


## Libraries required for PySerial
import os
import sys
import time
import signal
import PSUController
import PSURemoteController
import PSUConfig





Conf = {"OB0": {"config_path" : "",
                                  "optoboard_serial" : "4400052",
                                  "vtrx_v" : "1.3",
                                  "flx_cid" : 0,
                                  "flx_G" : 1,
                                  "debug" : False,
                                  "test_mode" : False,
                                  "configure" : True,
                "commToolName": "itk-ic-over-netio-next"
                #"commToolName": "lpgbt-com"
                }}




InitOpto(optoListConfig=Conf , commToolName="itk-ic-over-netio-next", woflxcore=None, runkeys_config=None)



# ## Variables that should be customasible for OptoBoard
# #optoboard_serial_1 = "2400021"
# optoboard_serial_1 = "4400052"
# vtrx_v = ""    # connected VTRx+ quad laser driver version

# ##Variable that are customasible for PySerial



# #### Configure OptoBoard

# # get Optoboard chip versions
# components_opto_1 = OrderedDict(components[optoboard_serial_1])
# # load configs
# config_file_default = load_default_config(components_opto_1, vtrx_v)
# config_file_default["vtrx_v"]="v1.3"
# # create wrapper to FELIX
# #communication_wrapper_1 = CommWrapper(flx_G=0, flx_d=0, lpgbt_master_addr=0x74, lpgbt_v=0, commToolName="itk-ic-over-netio-next")
# #communication_wrapper_1 = CommWrapper(flx_G=0, flx_d=0, lpgbt_master_addr=0x70, lpgbt_v=1, commToolName="itk-ic-over-netio-next")



# # communication_wrapper_1 = CommWrapper(flx_G=1, flx_d=0, lpgbt_master_addr=0x74, lpgbt_v=1, commToolName="itk-ic-over-netio-next")


# communication_wrapper_1 = CommWrapper(flx_G=1, flx_cid=0, lpgbt_master_addr=0x74, lpgbt_v=1, commToolName="itk-ic-over-netio-next")







# #communication_wrapper_1 = CommWrapper(flx_G=8, flx_d=0, lpgbt_master_addr=0x74, lpgbt_v=1, commToolName="itk-ic-over-netio-next")
# # initialise Optoboard objects
# opto1 = Optoboard(config_file_default, optoboard_serial_1, vtrx_v, components_opto_1, communication_wrapper_1)
# # Configure OptoBoard
# opto1.configure()    # configure all available chips on the Optoboard
