#flx-init -c X
## Two configs for setting the optoBoard on Fiber 2, if you want to run optoBoard on Fiber one both should be set to 0x1

LINK_MIN=0
LINK_STOP=1 # total channels / 2
LINK_MAX=11 # total channels / 2
DEVICE=0
#DEVICE=$1

DEC_EGROUP_MIN=0
DEC_EGROUP_MAX=6
ENC_EGROUP_MIN=0
ENC_EGROUP_MAX=-1



flx-config set GBT_DATA_RXFORMAT2=0x2 
flx-config set GBT_RXPOLARITY=0x0
flx-config -d ${DEVICE} set DECODING_REVERSE_10B=0x1


flx-config set DECODING_BCM_PRIME_EMU_BCID=1
flx-config set DECODING_BCM_PRIME_ONLY_L1A=0

for ((il = LINK_MIN ; il <= LINK_STOP ; il++)); do
  echo "Enabeling Settings for Link: ${il}"
    il2=$(printf '%02d\n' $il)
    flx-config -d ${DEVICE} set MINI_EGROUP_FROMHOST_${il2}_EC_ENABLE=0x1
    flx-config -d ${DEVICE} set MINI_EGROUP_TOHOST_${il2}_EC_ENABLE=0x1
    flx-config -d ${DEVICE} set MINI_EGROUP_FROMHOST_${il2}_IC_ENABLE=0x1
    flx-config -d ${DEVICE} set MINI_EGROUP_TOHOST_${il2}_IC_ENABLE=0x1


  for ((ig = DEC_EGROUP_MIN ; ig <= DEC_EGROUP_MAX ; ig++)); do
      echo "***Decoding Egroup ${ig}***"
      flx-config -d ${DEVICE} set DECODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_ENA=0x3      
      flx-config -d ${DEVICE} set DECODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_WIDTH=0x4    
      flx-config -d ${DEVICE} set DECODING_LINK${il2}_EGROUP${ig}_CTRL_PATH_ENCODING=0x33 
  done

  for ((ig = ENC_EGROUP_MIN ; ig <= ENC_EGROUP_MAX ; ig++)); do
      echo "Encoding Egroup ${ig}"
      flx-config -d ${DEVICE} set ENCODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_ENA=0x5     
      flx-config -d ${DEVICE} set ENCODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_WIDTH=0x1   
      flx-config -d ${DEVICE} set ENCODING_LINK${il2}_EGROUP${ig}_CTRL_PATH_ENCODING=0x0 
  done
done



for ((il = LINK_STOP+1 ; il <= LINK_MAX ; il++)); do
  echo "Disabling Settings for Link: ${il}"
  il2=$(printf '%02d\n' $il)
  flx-config -d ${DEVICE} set MINI_EGROUP_FROMHOST_${il2}_EC_ENABLE=0x0
  flx-config -d ${DEVICE} set MINI_EGROUP_TOHOST_${il2}_EC_ENABLE=0x0
  flx-config -d ${DEVICE} set MINI_EGROUP_FROMHOST_${il2}_IC_ENABLE=0x0
  flx-config -d ${DEVICE} set MINI_EGROUP_TOHOST_${il2}_IC_ENABLE=0x0


  for ((ig = DEC_EGROUP_MIN ; ig <= DEC_EGROUP_MAX ; ig++)); do
      echo "***Decoding Egroup ${ig}***"

      flx-config -d ${DEVICE} set DECODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_ENA=0x0      
      flx-config -d ${DEVICE} set DECODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_WIDTH=0x0    
      flx-config -d ${DEVICE} set DECODING_LINK${il2}_EGROUP${ig}_CTRL_PATH_ENCODING=0x0 

  done

  for ((ig = ENC_EGROUP_MIN ; ig <= ENC_EGROUP_MAX ; ig++)); do
      echo "Encoding Egroup ${ig}"
      flx-config -d ${DEVICE} set ENCODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_ENA=0x0      
      flx-config -d ${DEVICE} set ENCODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_WIDTH=0x0    
      flx-config -d ${DEVICE} set ENCODING_LINK${il2}_EGROUP${ig}_CTRL_PATH_ENCODING=0x0  


  done

done


# ### ElinkConfigs



#flx-config -d 0 set DECODING_LINK00_EGROUP0_CTRL_EPATH_ENA=0x1 
#flx-config -d 0 set DECODING_LINK00_EGROUP0_CTRL_EPATH_WIDTH=0x4 
#flx-config -d 0 set DECODING_LINK00_EGROUP0_CTRL_PATH_ENCODING=0x0


# # flx-config -d 0 set DECODING_LINK00_EGROUP1_CTRL_EPATH_ENA=0x1 
# # flx-config -d 0 set DECODING_LINK00_EGROUP1_CTRL_EPATH_WIDTH=0x4 
# # flx-config -d 0 set DECODING_LINK00_EGROUP1_CTRL_PATH_ENCODING=0x10101010 


# flx-config -d 0 set DECODING_LINK01_EGROUP0_CTRL_EPATH_ENA=0x1 
# flx-config -d 0 set DECODING_LINK01_EGROUP0_CTRL_EPATH_WIDTH=0x4 
# flx-config -d 0 set DECODING_LINK01_EGROUP0_CTRL_PATH_ENCODING=0x0 

# # flx-config -d 0 set DECODING_LINK01_EGROUP1_CTRL_EPATH_ENA=0x1 
# # flx-config -d 0 set DECODING_LINK01_EGROUP1_CTRL_EPATH_WIDTH=0x4 
# # flx-config -d 0 set DECODING_LINK01_EGROUP1_CTRL_PATH_ENCODING=0x10101010 


