#include "BCMP/Sensor.h"
#include <bitset>
#include <iostream>



using namespace std;
using namespace BCMP;

Sensor::Sensor(){
  m_verbose = false;
}

Sensor::~Sensor(){
  ClearHits();
}

void Sensor::SetVerbose(bool enable){
  m_verbose=enable;
}

Hit * Sensor::GetHit(){
  Hit * tmp = NULL;
  m_mutex.lock();
  tmp = m_hits.front();
  m_mutex.unlock();
  return tmp;
}

bool Sensor::NextHit(){
  if (m_hits.empty()) return false;
  m_mutex.lock();
  Hit * hit = m_hits.front();
  m_hits.pop_front();
  delete hit;
  m_mutex.unlock();
  return (!m_hits.empty());
}

vector<Hit *> Sensor::GetHits(uint32_t max_hits){
  vector<Hit *> tmp;
  m_mutex.lock();
  if(max_hits>m_hits.size())max_hits=m_hits.size();
  for(uint32_t i=0;i<max_hits;i++){
    tmp.push_back(m_hits.at(i));
  }
  m_mutex.unlock();
  return tmp;
}

uint32_t Sensor::GetNHits(){
  return m_hits.size();
}

uint64_t Sensor::GetNHeaders(){
  return m_NHeaders;
}

void Sensor::SetNHeaders(uint64_t val){
  m_mutex.lock();
  m_NHeaders=val;
  m_mutex.unlock();
}

void Sensor::ClearNHeaders(){
  m_mutex.lock();
  m_NHeaders=0;
  m_mutex.unlock();
}



bool Sensor::HasHits(){
  return (not m_hits.empty());
}


void Sensor::ClearHits(uint32_t max_hits){

  m_mutex.lock();
  if(max_hits>m_hits.size())max_hits=m_hits.size();
  for(uint32_t i=0;i<max_hits;i++){
    Hit * hit = m_hits.front();
    m_hits.pop_front();
    delete hit;
  }
  m_mutex.unlock();
}


void Sensor::ClearHits(){

  m_mutex.lock();
  uint64_t max_hits=m_hits.size();
  for(uint64_t i=0;i<max_hits;i++){
    Hit * hit = m_hits.front();
    m_hits.pop_front();
    delete hit;
  }
  m_NHeaders=0;
  m_mutex.unlock();
}


void Sensor::HandleData(const uint8_t * recv_data,  const uint32_t recv_size){

  
  if(!m_active) return; //If disabled don't store any data
  // m_Decoder->AddBytes(recv_data,0,recv_size);
  // m_Decoder->Decode(m_verbose);

  // for(auto frame: m_decoder->GetFrames()){
  //     m_mutex.lock();
  //     m_hits.push_back(frame->GetHit());      
  //     m_mutex.unlock();
  // }





  //cout <<endl<< "Sensor::HandleData size=" << recv_size << endl;
  if (recv_size<4){
    cout<<"Sensor::HandleData, wrong data size recieved shorter then 64bit, needs to be 64bit or longer"<<endl;
    return;
  }


  
  uint32_t L1ID = (uint32_t(recv_data[3]) << 24) | (uint32_t(recv_data[2]) << 16) | (uint32_t(recv_data[1]) << 8) | uint32_t(recv_data[0]) ;
  m_NHeaders+=1;
  if(m_verbose) cout<<endl<<"L1ID: "<<std::hex<<L1ID<<std::dec<<std::endl;
  for(uint32_t i=4; i+3<recv_size; i=i+4){ //Get Every 32 bits //Temproraly fxing bug

    uint8_t Safety= (recv_data[i+3] & 0xC0) >> 6; //Two bits of safety which should be set to 0x3
    uint8_t L1SubID= ((recv_data[i+3] & 0x3F) ); // 6 Bits of L1SubID 
    uint16_t BCID=    uint32_t(recv_data[i+2] & 0xFF)<<4 | (recv_data[i+1] & 0xF0)>>4  ; //12Bits of BCID

    uint8_t ToA= recv_data[i] & 0x3F; // First 6 bits is ToA
    uint8_t ToT= (recv_data[i+1] & 0x0F)<<2 | (recv_data[i] & 0xC0)>>6 ; // Second 6 is is ToT
      
    
    if(m_verbose)  cout <<"Data:"<< int(Safety)<<","<<int(ToT)<<","<<int(ToA)<<","<<int(BCID)<<","<<L1ID<<","<<int(L1SubID)<<  endl;
      

    if (Safety == 3 ){
      Hit * nhit = new Hit(ToA,ToT,BCID,L1ID,L1SubID);
      m_mutex.lock();
      m_hits.push_back(nhit);      
      m_mutex.unlock();
    }
    // else{
    //   cout<<"Safety bit is not 11, this is a big warning that some data flow is broken"<<endl;
    // }

  }
}

bool Sensor::IsActive(){
  return m_active;
}

void Sensor::SetActive(bool active){
  m_active=active;
}


void Sensor::SetName(string name){
  m_name=name;
}

string Sensor::GetName(){
  return m_name;
}














