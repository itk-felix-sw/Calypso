#include <BCMP/Handler.h>
#include <BCMP/TestScan.h>
#include <BCMP/ExternalTriggerScan.h>
#include <BCMP/ThresholdScan.h>
#include <BCMP/NoiseScan.h>
#include <BCMP/TransmissionScan.h>
#include <BCMP/CharacterizationScan.h>


#include <cmdl/cmdargs.h>
#include <signal.h>
#include <sstream>
#include <chrono>
#include <map>
#include <fstream>

using namespace std;
using namespace BCMP;

int main(int argc, char *argv[]){

  cout << "#####################################" << endl
       << "# Welcome to scan_manager_calypso   #" << endl
       << "#####################################" << endl;

  CmdArgStr  cScan(     's',"scan","scan","scan name",CmdArg::isREQ);
  CmdArgStrList cConfs( 'c',"config","chips","chip names");//,CmdArg::isREQ);
  CmdArgBool cVerbose(  'v',"verbose","turn on verbose mode");
  CmdArgStr  cBusPath('B',"buspath","folder","Network folder. Default /tmp/bus/");
  CmdArgStr  cNetFile(  'n',"netfile","file","Network file. Default connectivity.json");
  CmdArgStr  cOutPath(  'o',"outpath","path","output path. Default $ITK_DATA_PATH");
  CmdArgStr  cInterface('i',"interface","network-card","Default lo");

  CmdLine cmdl(*argv,&cScan,&cConfs,&cVerbose,&cBusPath,&cNetFile,&cOutPath,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  std::string commandLineStr="";
  for (int i=1;i<argc;i++) commandLineStr.append(std::string(argv[i]).append(" "));

  string scan(cScan);
  string buspath=(cBusPath.flags()&CmdArg::GIVEN?cBusPath:"/tmp/bus/");
  string netfile=(cNetFile.flags()&CmdArg::GIVEN?cNetFile:"connectivity.json");
  string interface=(cInterface.flags()&CmdArg::GIVEN?cInterface:"lo");

  Handler * handler = 0;

  if     (scan.find("External")!=string::npos)          {handler=new ExternalTriggerScan();}
  else if     (scan.find("TestPulse")!=string::npos)          {handler=new TestScan(); static_cast<TestScan*>(handler)->EnablePulse(true);}
  else if     (scan.find("Test")!=string::npos)          {handler=new TestScan();}
  else if     (scan.find("Threshold")!=string::npos)          {handler=new ThresholdScan();}
  else if     (scan.find("Characterization")!=string::npos)          {handler=new CharacterizationScan();}
  else if     (scan.find("QAQC")!=string::npos)          {handler=new NoiseScan();}



  else if     (scan.find("Noise")!=string::npos)          {handler=new NoiseScan();}
  else if     (scan.find("Transmission")!=string::npos)          {handler=new TransmissionScan();}

  handler->SetVerbose(cVerbose);
  handler->SetBusPath(buspath);
  handler->SetMapping(netfile);
  handler->SetScan(scan);
  handler->SetInterface(interface);

  cout << "Scan Manager: Configure the scan" << endl;
  if(cOutPath.flags()&CmdArg::GIVEN){
    handler->SetOutPath(string(cOutPath));
  }
  
  cout << "Scan Manager: Connect" << endl;
  handler->Connect();

  //cout << "Scan Manager: InitRun" << endl;
  handler->InitRun();

  
  cout << "Scan Manager: Config" << endl;
  handler->Config();

  cout << "Scan Manager: PreRun" << endl;
  handler->PreRun();

  //cout << "Scan Manager: Save configuration before the run" << endl;
  //handler->SaveConfig("before");


  cout << "Scan Manager: Run" << endl;
  handler->Run();

  cout << "Scan Manager: Analysis" << endl;
  handler->Analysis();

  cout << "Scan Manager: Disconnect" << endl;
  handler->Disconnect();

  //cout << "Scan Manager: Save configuration after the run" << endl;
  //handler->SaveConfig("after");

  //cout << "Scan Manager: Save the tuned files" << endl;
  //handler->SaveConfig("tuned");

  cout << "Scan Manager: Save the histograms" << endl;
  handler->Save();


  if(scan.find("QAQC")!=string::npos){
    unsigned mean = static_cast<NoiseScan*>(handler)->GetSCurveMean();
    unsigned sigma = static_cast<NoiseScan*>(handler)->GetSCurveSigma();
    std::cout<<"Deleting the old noise scan"<<std::endl;
    //delete handler;
  std::cout<<"Starting the Characterisation scan"<<std::endl;
    handler=new CharacterizationScan();
    static_cast<CharacterizationScan*>(handler)->SetLowThreshold(mean+2*sigma);
    
    handler->SetVerbose(cVerbose);
    handler->SetBusPath(buspath);
    handler->SetMapping(netfile);
    handler->SetScan(scan);
    handler->SetInterface(interface);

    cout << "Scan Manager: Configure the scan" << endl;
    if(cOutPath.flags()&CmdArg::GIVEN){
      handler->SetOutPath(string(cOutPath));
    }
  
    cout << "Scan Manager: Connect" << endl;
    handler->Connect();

    //cout << "Scan Manager: InitRun" << endl;
    handler->InitRun();

  
    cout << "Scan Manager: Config" << endl;
    handler->Config();

    cout << "Scan Manager: PreRun" << endl;
    handler->PreRun();

    //cout << "Scan Manager: Save configuration before the run" << endl;
    //handler->SaveConfig("before");


    cout << "Scan Manager: Run" << endl;
    handler->Run();

    cout << "Scan Manager: Analysis" << endl;
    handler->Analysis();

    cout << "Scan Manager: Disconnect" << endl;
    handler->Disconnect();

    //cout << "Scan Manager: Save configuration after the run" << endl;
    //handler->SaveConfig("after");

    //cout << "Scan Manager: Save the tuned files" << endl;
    //handler->SaveConfig("tuned");

    cout << "Scan Manager: Save the histograms" << endl;
    handler->Save();
  }

  
  cout << "Scan Manager: Cleaning the house" << endl;
  delete handler;
  
  cout << "Scan Manager: Have a nice day" << endl;
  return 0;
}
