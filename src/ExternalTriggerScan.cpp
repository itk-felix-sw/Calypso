#include "BCMP/ExternalTriggerScan.h"

#include <iostream>
#include <chrono>
#include <signal.h>

#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TSysEvtHandler.h"


using namespace std;
using namespace BCMP;

bool ExternalTriggerScan::m_scanStop = false;

TApplication* theAppETS;

ExternalTriggerScan::ExternalTriggerScan(){

}

ExternalTriggerScan::~ExternalTriggerScan(){}

void ExternalTriggerScan::PreRun(){
  cout << "PreRun()" << endl;
  cout << "ExternalTriggerScan::PreRun" << endl;
  m_totalToT = new TH1I("Total_ToT_Hits",";ToT",32,0.5,32.5);
  m_totalToA = new TH1I("Total_ToA_Hits",";ToA",10000,0.5,10000.5);
  m_totalL1ID = new TH1I("Total_L1ID",";L1ID",4294967296,0,4294967296);
  m_totalMissedL1ID = new TH1I("Total_MissedL1ID",";Missed L1ID",4294967296,0,4294967296);

  for(auto fe : GetSensors()){


    m_h_tot_raw[fe->GetName()]=new TH1I(("ToT_Raw_"+fe->GetName()).c_str(),";ToT",32,0.5,32.5);
    m_h_tot_real[fe->GetName()]=new TH1I(("ToT_Real_"+fe->GetName()).c_str(),";ToT",32,0.5,32.5);
    m_h_toa_real[fe->GetName()]=new TH1I(("ToA_Real_"+fe->GetName()).c_str(),";ToA Distance",10000,0.5,10000.5);
    m_h_toa_raw[fe->GetName()]=new TH1I(("ToA_Raw_"+fe->GetName()).c_str(),";ToA",3200,0.5,3200.5);
    m_h_bcid[fe->GetName()]=new TH1I(("BCID_"+fe->GetName()).c_str(),";BCID",3564,-0.5,3563.5);
    m_h_L1ID[fe->GetName()]=new TH1D(("L1ID_"+fe->GetName()).c_str(),";L1ID",4294967296,0,4294967296);
    m_h_L1SubID[fe->GetName()]=new TH1I(("L1SubID_"+fe->GetName()).c_str(),";L1SubID",64,-0.5,63.5);

    m_hits[fe->GetName()]  =new HitTree(("hits_"+fe->GetName()).c_str(),fe->GetName());











    
    cout << "fe name" << fe->GetName() << endl;
  }
  m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
}

void ExternalTriggerScan::StopScan(int){
  cout << endl << "You pressed ctrl+c -> quitting" << endl;
  m_scanStop = true;
}


void ExternalTriggerScan::Run(){

  //Prepare the trigger
  gROOT->SetBatch(true);
  theAppETS=new TApplication("myApp",0,0);
  TCanvas* myC=new TCanvas("can","can", 1000, 1000);
  myC->Divide(2,2);
  
  m_scanStop = false;
  signal(SIGINT, StopScan);
  signal(SIGTERM, StopScan);
  signal(SIGILL, StopScan);


  cout << "Clearing Hits"<<std::endl;
  for(auto se : GetSensors()){
    se->ClearHits();
    se->SetVerbose(false);
  }


  cout << "ExternalTriggerScan::Run: Start" << endl;
  auto start = chrono::steady_clock::now();
  uint32_t ev=0;
  uint32_t Hits=0;
  uint32_t LastL1ID =-1;
  while(!m_scanStop){
    for(auto sensor : GetSensors()){

      
      

      if(!sensor->HasHits()) continue;
      Hit* hit = sensor->GetHit();

      uint32_t ToT=hit->GetTOT();
      uint32_t ToA=hit->GetTOA();
      uint32_t BCID=hit->GetBCID();
      uint32_t L1ID=hit->GetL1ID();
      uint32_t L1SubID=hit->GetL1SubID();

      m_hits[sensor->GetName()]->Fill(hit);
      m_totalToT->Fill(ToT);
      m_totalToA->Fill(ToA);
      m_totalL1ID->Fill(L1ID);

      // if(LastL1ID+1!=L1ID && LastL1ID!=L1ID){
      // 	cout<<"L1ID is missed! Previous: "<<LastL1ID<<"-"<<L1ID<<endl;
      // }
      
      for(uint32_t Miss=LastL1ID+1; Miss<L1ID && LastL1ID!=L1ID && LastL1ID!=-1 ;Miss++){
	m_totalMissedL1ID->Fill(Miss);
      }
      LastL1ID=L1ID;
      
      //cout << ToT<<","<<ToA<<","<<BCID<<","<<L1ID<<","<<L1SubID<<  endl;
      m_h_tot_raw[sensor->GetName()]->Fill(ToT);
      m_h_toa_raw[sensor->GetName()]->Fill(ToA);
      sensor->NextHit();

      Hits++;
      std::cout<<"Hits: "<<Hits<<std::endl;      
    }
    ev++;      

    if(Hits%100000==1)
      std::cout<<"Entries: "<<Hits<<std::endl;      
    if(ev%1000000==0){

      
      myC->cd(1);
      m_totalToT->Draw();
      myC->cd(2);
      m_totalToA->Draw();
      myC->cd(3);
      m_totalL1ID->Draw();
      myC->cd(4);
      m_totalMissedL1ID->Draw();
      
      myC->Update();
    
    }

  }

  
}

void ExternalTriggerScan::Analysis(){
  
  uint64_t MissedHitCount=0;
  int64_t m_oldL1=-1;
  
  static uint32_t MaxBCID=3562;
  static uint32_t MaxL1SubID=63;
  //Now time to use the tree to do post processing
  

  for(auto fe : GetSensors()){
      //Save Raw Hits file, before any potential medelling
    m_hits[fe->GetName()]->Write();
    for(Long64_t evt=0;evt<m_hits[fe->GetName()]->GetEntries();evt++){
      m_hits[fe->GetName()]->GetEntry(evt);
      Hit * hit = m_hits[fe->GetName()]->Get();

      uint32_t ToT=hit->GetTOT();
      uint32_t ToA=hit->GetTOA();

      uint32_t BCID=hit->GetBCID();
      uint32_t L1ID=hit->GetL1ID();
      uint32_t L1SubID=hit->GetL1SubID();
      if (ToT==0 and ToA==0) continue;
      //Search for half events (Events passing through two event boundires and skip the next event)
      bool FoundMatching=false;
      uint64_t SkipDistance=0;
      if( (ToA+ToT) == 32 ) //Aka if the word extends to the next event
	{

	  for(uint64_t nextevt=1;nextevt<(64*6);nextevt++){
	    m_hits[fe->GetName()]->GetEntry(evt+nextevt);
	    hit = m_hits[fe->GetName()]->Get();
	    uint32_t ToT2=hit->GetTOT();
	    uint32_t ToA2=hit->GetTOA();
	    uint32_t BCID2=hit->GetBCID();
	    uint32_t L1ID2=hit->GetL1ID();
	    uint32_t L1SubID2=hit->GetL1SubID();
	    if (ToT2==0 and ToA2==0) continue;

	    //If a connecting event is count skip that event and ToT's together
	    if ((  ( (MaxBCID+1)*(BCID2==0 && (BCID2!=BCID)) + BCID2 + (MaxL1SubID+1)*(L1SubID2==0 && (L1SubID2!=L1SubID)) + L1SubID2 -1  ) == (BCID+L1SubID)) and (ToA2==0 and ToT2!=0) ){
	      ToT += ToT2 ;
	      FoundMatching=true;
	      SkipDistance=nextevt;
	    }
	    else if (BCID2==BCID){
	      // cout<<BCID<<" "<<BCID2<<" / "<<L1SubID<<" "<<L1SubID2<<" / "<<L1ID<<" "<<L1ID2<<" / "<<ToT<<" "<<ToT2<<" / "<<ToA<<" "<<ToA2<<endl;
	    }
	    break;
	  }
	}


      uint32_t ToT_next=0;
      uint32_t ToA_next=0;
      uint32_t BCID_next=0;
      uint32_t L1ID_next=0;
      uint32_t L1SubID_next=0;

      //Search for the next event
      for(Long64_t nextevt=(SkipDistance+1);nextevt< (m_hits[fe->GetName()]->GetEntries() - evt);nextevt++) {
	  m_hits[fe->GetName()]->GetEntry(evt+nextevt);
	  hit = m_hits[fe->GetName()]->Get();
	  ToT_next=hit->GetTOT();
	  ToA_next=hit->GetTOA();
	  BCID_next=hit->GetBCID();
	  L1ID_next=hit->GetL1ID();
	  L1SubID_next=hit->GetL1SubID();
	  if (ToT_next==0 and ToA_next==0) continue;
	  break; //If found the next evet that is not zero
      }

      
      if(m_oldL1!=-1 and (m_oldL1+1)!=L1ID)
	{
	  MissedHitCount++;
	  //std::cout<<"Packet lost "<<L1ID<<" old: "<<m_oldL1<<std::endl;
	}
      m_oldL1=L1ID;

      m_h_tot_real[fe->GetName()]->Fill(ToT);
      m_h_bcid[fe->GetName()]->Fill(BCID);
      m_h_L1ID[fe->GetName()]->Fill(L1ID);
      m_h_L1SubID[fe->GetName()]->Fill(L1SubID);

      uint64_t DistanceToNext = ((MaxBCID+1)*(BCID_next<BCID) + BCID_next-BCID)*32 + ToA_next - ToA;
      //cout<<"Calculating distance "<<DistanceToNext<<" = Bn: "<<BCID_next<<" Bo: "<<BCID<<" Tn: "<<ToA_next<<" To: "<<ToA<<std::endl;
      

      m_h_toa_real[fe->GetName()]->Fill(DistanceToNext);
			    
      if(FoundMatching){
	evt+=SkipDistance; //skip the next event,
      }

    }
    
    std::cout<<"Number of L1 packets lost are: "<<MissedHitCount<<std::endl;
  
    m_h_tot_real[fe->GetName()]->Write();
    m_h_tot_raw[fe->GetName()]->Write();
    m_h_toa_real[fe->GetName()]->Write();
    m_h_toa_raw[fe->GetName()]->Write();
    m_h_bcid[fe->GetName()]->Write();
    m_h_L1ID[fe->GetName()]->Write();
    m_h_L1SubID[fe->GetName()]->Write();




    delete m_h_tot_real[fe->GetName()];
    delete m_h_tot_raw[fe->GetName()];
    delete m_h_toa_real[fe->GetName()];
    delete m_h_toa_raw[fe->GetName()];
    delete m_h_bcid[fe->GetName()];
    delete m_h_L1ID[fe->GetName()];
    delete m_h_L1SubID[fe->GetName()];
    delete m_hits[fe->GetName()];
  }
 
}                                                                                                                                        
