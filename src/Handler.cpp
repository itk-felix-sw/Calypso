#include "BCMP/Handler.h"
#include "BCMP/RunNumber.h"
#include <nlohmann/json.hpp>
#include <iostream>
#include <fstream>
#include <mutex>
#include <chrono>
#include <sstream>
#include <iomanip>
#include "TFile.h"
#include  <iomanip>

using json = nlohmann::json;
//using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;
using namespace std;
using namespace BCMP;


struct FelixDataHeader{
  uint16_t length;
  uint16_t status;
  uint32_t elink;
};

struct FelixCmdHeader{
  uint32_t length;
  uint32_t reserved;
  uint64_t elink;
};


Handler::Handler(){

  string itkpath = getenv("ITK_PATH");
  m_config_path.push_back("./");
    // m_config_path.push_back("./tuned/");
    // m_config_path.push_back(itkpath+"/");
    // m_config_path.push_back(itkpath+"/tuned/");
    //m_config_path.push_back(itkpath+"/config/");
  //m_config_path.push_back(itkpath+"/installed/share/data/config/");
  m_config_path.push_back("/home/sbmuser/itk-felix-sw/Calypso/src/");
   
  m_verbose = true;
  m_rootfile = 0;
  m_outpath = (getenv("ITK_DATA_PATH")?string(getenv("ITK_DATA_PATH")):".");
  m_rn = new RunNumber();
  m_output = true;
  m_fullOutPath = "";
  m_config_delay=100;
  m_pulse_delay=1;

  
}

Handler::~Handler(){
  while(!m_fes.empty()){
    FrontEnd* fe=m_fes.back();
    m_fes.pop_back();
    delete fe;
  }
  m_fes.clear();
  if(m_rootfile) delete m_rootfile;
  delete m_rn;
}

void Handler::SetVerbose(bool enable){
  m_verbose=enable;
  for(auto fe: m_fes){
    fe->SetVerbose(enable);
  }
}

void Handler::SetBusPath(string BusPath){
  m_buspath = BusPath;
}

void Handler::SetInterface(string interface){
  m_interface = interface;
}

void Handler::SetEnableOutput(bool enable){
  m_output=enable;
}

bool Handler::GetEnableOutput(){
  return m_output;
}

void Handler::SetScan(string scan){
  m_scan=scan;
}

void Handler::SetOutPath(string path){
  m_outpath=path;
}

std::string Handler::GetFullOutPath(){
  return m_fullOutPath;
}

void Handler::InitRun(){
  cout << "Handler::InitRun" << endl;

  //Start a new run
  cout << "Starting run number: " << m_rn->GetNextRunNumberString(6,true) << endl;

  //Create the output directory
  if(m_verbose) cout << "Handler::InitRun Creating output directory (if doesn't exist yet)" << endl;
  ostringstream cmd;
  cmd << "mkdir -p " << m_outpath << "/" << m_rn->GetRunNumberString(6) << "_" << m_scan;
  system(cmd.str().c_str());
  m_fullOutPath = m_outpath + "/" + m_rn->GetRunNumberString(6) + "_" + m_scan;

  //give up if somebody disabled the output
  if(!m_output) return;

  //Open the output ROOT file
  ostringstream opath;
  opath << m_outpath << "/" << m_rn->GetRunNumberString(6) << "_" << m_scan << "/" << "output.root";
  if(m_verbose) cout << "Handler::InitRun Creating root file " << opath.str().c_str() << endl;
  m_rootfile=TFile::Open(opath.str().c_str(),"RECREATE");
  m_rootfile->cd();
  if(m_verbose) cout << "Handler::InitRun Created root file " << opath.str().c_str() << endl;

}


FrontEnd* Handler::GetFE(string name){
  return m_fe[name];
}

std::vector<FrontEnd*> Handler::GetFEs(){
  return m_fes;
}


Sensor* Handler::GetSensor(string name){
  return m_sensor[name];
}

std::vector<Sensor*> Handler::GetSensors(){
  return m_sensors;
}


void Handler::SetMapping(string name, string config, uint64_t config_tx, uint64_t config_rx, 
			 uint16_t LpGBTI2C, uint16_t BCMI2C, uint8_t LpGBTVer,uint8_t LpGBTI2CMaster,
			 std::string Sensor_Names[4], uint64_t Sensor_RXs[4]) {

  bool FECreated=false;
  for(uint8_t chan=0;chan<4; chan++ ){
    if(Sensor_Names[chan]=="") continue;
    if (!FECreated) { 
      AddFE(name,config, LpGBTI2C, BCMI2C , LpGBTVer, LpGBTI2CMaster); 
      FECreated=true;
    }
    std::cout<<"Handler::SetMapping Adding sensor"<<Sensor_Names[chan]<<std::endl;
    AddSensor(Sensor_Names[chan]);
    std::cout<<"Handler::SetMapping Mapping sensor"<<Sensor_Names[chan]<<std::endl;
    GetFE(name)->SetSensor(chan,GetSensor(Sensor_Names[chan]));
  }

  if(FECreated){
    AddMapping(name,config,config_tx,config_rx, Sensor_Names,Sensor_RXs);
  }

}




void Handler::SetMapping(string mapping){

  for(string const& sdir : m_config_path){
   if(m_verbose) cout << "Handler::SetMapping" << " Testing: " << sdir << mapping << endl;
   m_connectivityPath = sdir+mapping;
   std::ifstream fr(m_connectivityPath);
   if(!fr){continue;}
   cout << "Handler::SetMapping" << " Loading: " << m_connectivityPath << endl;
   json config;
   fr >> config;
   for(auto node : config["connectivity"]){

     uint64_t Sensor_RXs[4]={0,0,0,0};
     string Sensor_Name[4]={"","","",""};

     bool FECreated=false;
     for(auto nodeSens: node["Sensors"] ) {
       if(nodeSens["enable"]!=0){
	 uint8_t chan = nodeSens["Chan"];

	 if(chan>4){
	   cout<<"SetMapping::Error Channel "<<chan<<" for sensor "<<nodeSens["name"]<<" should be smaller then 4. Exiting"<<endl;
	   exit(1);
	 }

	 if (!FECreated) { 
	   AddFE(node["name"],node["config"], node["LpGBTI2C"] ,node["BCMI2C"] ,node["LpGBTVer"],node["LpGBTI2CMaster"]); 
	   FECreated=true;
	 }
	 std::cout<<"Handler::SetMapping Adding sensor "<<nodeSens["name"]<<std::endl;
	 AddSensor(nodeSens["name"]);
	 std::cout<<"Handler::SetMapping Mapping sensor "<<nodeSens["name"]<<std::endl;
	 GetFE(node["name"])->SetSensor(chan,GetSensor(nodeSens["name"]));
	 std::cout<<"Handler::SetMapping Finished Mapping sensor"<<nodeSens["name"]<<std::endl;
	 Sensor_Name[chan]=nodeSens["name"];
	 Sensor_RXs[chan]=nodeSens["rx"];
	 std::cout<<"Handler::SetMapping Looping to the next sensor"<<std::endl;
       }
     }
     std::cout<<"Handler::SetMapping Running AddMapping"<<std::endl;
     if(FECreated){
       AddMapping(node["name"],node["config"],node["tx"],node["rx"],Sensor_Name,Sensor_RXs);
     }
   }

   fr.close();
   break;
  }

}





 void Handler::AddMapping(string name, string config, uint64_t config_tx, uint64_t config_rx, std::string Sensor_Names[4], uint64_t Sensor_RXs[4]) {

  if(m_verbose){
    cout << "Handler::AddMapping Front-End: "
         << "name: " << name << ", "
         << "config: " << config << ", "
	 << "config_tx: " << config_tx<<", "
	 << "config_rx: " << config_rx;
    for (uint8_t chan=0;chan<4;chan++){
      if(Sensor_Names[chan]=="") continue;
      cout<< ", Chan"<<int(chan)<<": " << Sensor_Names[chan] <<" rx = 0x"<<std::hex<<Sensor_RXs[chan]<<std::dec;
    }
    cout<< endl;
  }

  m_fe_tx[name]=config_tx;
  m_fe_rx[name]=config_rx;
  //m_tx_fes[config_tx].push_back(GetFE(name));

  for(uint8_t chan=0;chan<4;chan++){
    if (Sensor_Names[chan]=="") continue; //Skippid not enabled channels
    m_se_rx[Sensor_Names[chan]]=Sensor_RXs[chan];
    //m_rx_se[Sensor_RXs[chan]]=GetSensor(Sensor_Names[chan]);
    m_se_fe[Sensor_Names[chan]]=GetFE(name);
    m_se_chan[Sensor_Names[chan]]=chan;
    
  }
    
}

void Handler::AddFE(string name, FrontEnd* fe){

  fe->SetName(name);
  fe->SetActive(true);
  m_fe[name]=fe;
  m_fes.push_back(fe);
  m_enabled[name]=true;
}

void Handler::AddFE(string name, string path, uint16_t LpGBTI2C, uint16_t BCMI2C, uint8_t LpGBTVer, uint8_t LpGBTI2CMaster){

  if(path==""){path=m_configs[name];}
  if(path==""){path=name+".json";}

  FrontEnd * fe = 0;
  for(string const& sdir : m_config_path){
    if(m_verbose) cout << "Handler::AddFE" << " Testing: " << sdir << path << endl;
    std::ifstream fr(path);
    if(!fr){
      cout << "1Handler::AddFE" << " File not found: " << sdir << path << endl;
      continue;
    }
    cout << "Handler::AddFE" << " Loading: " << sdir << path << endl;
    json config;
    fr >> config;
    

    //Create the front-end
    if(m_verbose) cout << "Handler::AddFE" << " Create front-end" << endl;
    fe = new FrontEnd();

    

    fe->SetVerbose(m_verbose);
    fe->SetName(name);
    fe->SetLpGBTi2c(LpGBTI2C);
    fe->SetCalypsoi2c(BCMI2C);
    fe->SetLpGBTVer(LpGBTVer);
    fe->SetLpGBTi2cMaster(LpGBTI2CMaster);
    fe->SetActive(true);

    if(m_verbose) cout << "Handler::AddFE" << " Reading configuration file" << endl;

    //Actually read the file
    if(m_verbose) cout << "Handler::AddFE" << " Reading registers" << endl;
    fe->SetConfig(config["Calypso"]["config"]);

    m_fe[name]=fe;
    m_fes.push_back(fe);
    m_enabled[name]=true;

    //File was found, no need to keep looking for it
    break;
  }
  if(!fe){cout << "Handler::AddFE Front-end not added. File not found: " << path << endl;}
  else{
    if(m_verbose) cout << "Handler::AddFE File correctly loaded: " << path << endl;
  }

}



void Handler::AddSensor(string name, Sensor* sens){

  m_sensor[name]=sens;
  cout<<"Sensor added "<<name<<endl;
  m_sensors.push_back(sens);
  m_enabled[name]=true;
}

void Handler::AddSensor(string name){
  Sensor *fe = new Sensor();
  fe->SetVerbose(m_verbose);
  fe->SetName(name);
  cout<<"Sensor added "<<name<<endl;
  fe->SetActive(true);
 
  m_sensor[name]=fe;
  m_sensors.push_back(fe);
  m_enabled[name]=true;

}




void Handler::SaveFE(FrontEnd * fe, string path){

  cout << "Handler::SaveFE " << fe->GetName() << " in: " << path << endl;
  json config;
  config["Calypso"]["name"]=fe->GetName();
  //  config["Calypso"]["config"]=fe->GetConfig();

  ofstream fw(path);
  fw << setw(4) << config;
  fw.close();

}

void Handler::Connect(){


  cout << "Handler::Connect Create the context" << endl;

  std::vector<uint64_t> vec_elinks;

  //TX FE
  for(auto it : m_fe_tx){
    if(m_enabled[it.first]==false){continue;}
    uint64_t tx_elink = it.second;
    if(std::find(vec_elinks.begin(), vec_elinks.end(), tx_elink) == vec_elinks.end()){
      vec_elinks.push_back(tx_elink);
    }
    m_tx_fes[tx_elink].push_back(m_fe[it.first]);
  }

  //RX FE
  for(auto it : m_fe_rx){
    if(m_enabled[it.first]==false){continue;}
    uint64_t rx_elink = it.second;
    if(std::find(vec_elinks.begin(), vec_elinks.end(), rx_elink) == vec_elinks.end()){
      vec_elinks.push_back(rx_elink);
    }
    m_rx_fes[rx_elink].push_back(m_fe[it.first]);
  }



  //RX Sensor
  for(auto it : m_se_rx){
    if(m_enabled[it.first]==false){continue;}
    uint64_t rx_elink = it.second;
    
    if(std::find(vec_elinks.begin(), vec_elinks.end(), rx_elink) == vec_elinks.end()){
      vec_elinks.push_back(rx_elink);
    }

    m_rx_se[rx_elink] = m_sensor[it.first];
  }

  std::cout<<"Handler::Connect Interface to be used is:"<<m_interface<<endl;
  std::cout<<"Handler::Connect bus dir   to be used is:"<<m_buspath<<endl; 

  m_NextConfig = new felix_proxy::ClientThread::Config();
  m_NextConfig->property["local_ip_or_interface"] = m_interface;//"127.0.0.1";
  
  // if(m_verbose) // Error level is defined by verbosity level
  //   m_NextConfig->property["log_level"] = "trace";
  // else
  m_NextConfig->property["log_level"] = "error";

  m_NextConfig->property["bus_dir"] = m_buspath;
  m_NextConfig->property["bus_group_name"] = "FELIX";
  // m_NextConfig.property["verbose_bus"] = "True";
  // m_NextConfig.property["netio_pages"] = "256";
  // m_NextConfig.property["timeout"] = "2";
  // m_NextConfig.property["netio_pagesize"] = "";
  // m_NextConfig.property["thread_affinity"] = "";

  

  m_NextConfig->on_init_callback = []() {
    std::cout<<"Handler::ClientThread::Initialiasing connection"<<endl;
  };

  m_NextConfig->on_connect_callback = [](const std::uint64_t fid){
    std::cout<<"Handler::ClientThread::Connecting to FID: 0x"<<std::hex<<fid<<std::dec<<endl;

  };

  m_NextConfig->on_disconnect_callback = [](const std::uint64_t fid){
    std::cout<<"Handler::ClientThread::Disconnecting to FID: 0x"<<std::hex<<fid<<std::dec<<endl;

  };




  //Shouldn't handle service frames yet
  m_NextConfig->on_data_callback =  [&](const std::uint64_t rx_elink,
						    const std::uint8_t* data,
						    const std::size_t size,
						    const std::uint8_t status){
    
    //cout << "Handler::Connect Received data from " << rx_elink << " size:" << size << endl;
    if(m_rx_se.find(rx_elink) != m_rx_se.end()){
      
      // We need to have switch this enable disables this and we need the correct elink mapping 
      
      // m_mutex[rx_elink].lock();
      m_rx_se[rx_elink]->HandleData(&data[0],size); 
      // m_mutex[rx_elink].unlock(); 
    } 
    else if(m_rx_fes.find(rx_elink) != m_rx_fes.end()){
      // cout<<"Package for FE has arrived: "<<endl;
      // for(unsigned ind=0;ind<size;ind++)
      // 	cout<<std::hex<<setfill('0')<<setw(2)<<right<<int(data[ind])<<std::dec<<" ";
      // std::cout<<std::endl;


      for(unsigned fInd=0; fInd < (m_rx_fes[rx_elink].size()); fInd++){
	m_mutex[rx_elink].lock();
	m_rx_fes[rx_elink][fInd]->HandleData(&data[0],size);
	m_mutex[rx_elink].unlock(); 
      }

    }
    else{
    std:cout<<"Handler::data_callback::ERROR! RX_Elink: "<<rx_elink<<" not defined among channels listened"<<endl;
    }
  

  };



  m_NextClient = new felix_proxy::ClientThread(*m_NextConfig);
  // m_NextClient_thread = thread([&](){m_NextClient->event_loop()->run_forever();});
  std::cout<<"Elinks to subscribe are: "<<std::endl;
  for(unsigned lind=0;lind<vec_elinks.size();lind++){
    cout<<"Subscribing to : 0x"<<std::hex<<vec_elinks.at(lind)<<std::dec<<std::endl;
    
  }
  m_NextClient->subscribe(vec_elinks);
  //Sending a packet to start communication earlier
  for(auto fe : m_fes){
    uint64_t tx_elink=m_fe_tx[fe->GetName()];
    vector<uint8_t> payload = { 0, 0, 0, 0 };
    m_NextClient->send_data(tx_elink, payload.data(), payload.size(), true ); 
  
  }


  //Wait for subscription to start. 
  std::this_thread::sleep_for(std::chrono::milliseconds(10000)); 
}


// void Handler::Connect(){

//   //Connect to FELIX
//   cout << "Handler::Connect Create the context" << endl;
//   m_context = new netio::context("posix");
//   m_context_thread = thread([&](){m_context->event_loop()->run_forever();});

//   //TX
//   for(auto it : m_fe_tx){
//     if(m_enabled[it.first]==false){continue;}
//     uint32_t tx_elink = it.second;
//     if(m_tx.count(tx_elink)==0){
//       cout << "Handler::Connect Connect to cmd elink: " << tx_elink<<endl;
//       m_tx[tx_elink]=new netio::low_latency_send_socket(m_context);
//       m_tx[tx_elink]->connect(netio::endpoint("127.0.0.1",12340));
//     }
//     m_tx_fes[tx_elink].push_back(m_fe[it.first]);
//   }

//   //RX
//   for(auto it : m_se_rx){
//     if(m_enabled[it.first]==false){continue;}
//     uint32_t rx_elink = it.second;
//     m_rx_se[rx_elink] = m_sensor[it.first];
//     m_mutex[rx_elink].unlock();
//     m_rx[rx_elink] = new netio::low_latency_subscribe_socket(m_context, [&,rx_elink](netio::endpoint& ep, netio::message& msg){

// 	if(m_verbose) cout << "Handler::Connect Received data from " << ep.address() << ":" << ep.port() << " size:" << msg.size() << endl;
// 	m_mutex[rx_elink].lock();
// 	vector<uint8_t> data = msg.data_copy();
// 	//We should remove any potential header before decoding
// 	FelixDataHeader hdr;
// 	memcpy(&hdr,(const void*)&data[0], sizeof (hdr));
// 	m_rx_se[rx_elink]->HandleData(&data[sizeof(hdr)],data.size()-sizeof(hdr));
// 	m_mutex[rx_elink].unlock();
//       });
//     cout << "Handler::Connect Subscribe to data elink: " << rx_elink <<endl;
//     m_rx[rx_elink]->subscribe(rx_elink, netio::endpoint("127.0.0.1", 12350));
//   }
// }



bool Handler::CheckFEComm(){

  bool AllGood=true;
   cout<< "Handler::CheckFEComm" << endl;
    for(auto fe : m_fes){
      fe->ClearReplies();
      fe->ReadLpGBTRegisters(0x1);
      std::this_thread::sleep_for(std::chrono::milliseconds(m_config_delay));
      //Read the data back
      AllGood= AllGood && fe->GetReplySize()>0;
      cout<< "Handler::CheckFEComm: LpGBT From " <<fe->GetName()<<" is "<< (fe->GetReplySize()>0 )<< endl;
    }

    return AllGood;
}



void Handler::Config(){

    cout<< "Handler::Config" << endl;
    for(auto fe : m_fes){
      cout << "Write configuration through EC e-link for FE "<<fe->GetName() << endl;
      //Write all registers for the front end and send, in BCM' it's not possible to send individual regsiters

      // fe->EnableChan(true,0);
      // fe->EnableChan(true,2);
      // Send(fe);
      // std::this_thread::sleep_for(std::chrono::milliseconds(10));
      // fe->EnableChan(false,0);
      // Send(fe);
      // std::this_thread::sleep_for(std::chrono::milliseconds(10));
      // fe->EnableChan(true,0);
      // Send(fe);
      std::this_thread::sleep_for(std::chrono::milliseconds(m_config_delay));
      fe->WriteRegisters();
      Send(fe);
      // //Add this line to slow down the configuration
      // std::this_thread::sleep_for(std::chrono::milliseconds(10));
      // fe->EnableChan(true,2);
      // Send(fe);
      // std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    
}


void Handler::Send(FrontEnd * fe){

  // //process the commands from the front-end
  fe->Encode();
  //quick return
  if(fe->GetLength()==0){return;}
  if(!m_enabled[fe->GetName()]){return;}
  //figure out the tx_elink
  uint64_t tx_elink=m_fe_tx[fe->GetName()];
  
  //Send one by one
  if(m_verbose) cout<<"Payload: ";
  for(uint32_t i=0;i<fe->GetLength();i++){
    vector<uint8_t> payload(fe->GetSeqLength(i));
    for(uint32_t j=0;j<fe->GetSeqLength(i);j++){
      payload[j]=fe->GetBytes(i)[j];
      if(m_verbose) cout<<std::hex<<setfill('0')<<setw(2)<<right<<int(payload[j])<<std::dec<<" ";
    };
    if(m_verbose) std::cout<<std::endl;
    m_NextClient->send_data(tx_elink, payload.data(), payload.size(), true );
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
  };


  // vector<uint8_t> payload(fe->GetLength());
  // for(uint32_t i=0;i<fe->GetLength();i++){payload[i]=fe->GetBytes()[i];};
  // m_NextClient->send_data(tx_elink, payload.data(), payload.size(), true ); 

}

void Handler::Disconnect(){

  for(auto it : m_se_rx){
    m_NextClient->unsubscribe(it.second);
  }
  

}

void Handler::PreRun(){}
/*
  void Handler::SaveConfig(std::string configuration){
  if(!m_output) return;

  if(configuration=="tuned"){system("mkdir -p ./tuned");}

  for(auto fe: m_fes){
  if(!fe->IsActive()){continue;}
  ostringstream os;
  if(configuration!="tuned"){
  os << m_fullOutPath << "/" << m_configs[fe->GetName()] << "_" << configuration << ".json";
  }else{
  os << "./tuned/" << m_configs[fe->GetName()] << ".json";
  }
  SaveFE(fe,os.str());
  }
  }*/

void Handler::Run(){
  cout << __PRETTY_FUNCTION__ << "FIXME: Handler::Run is supposed to be extended" << endl;
}

void Handler::End(){}

void Handler::Save(){
  if(!m_output) return;

  cout << "Save ROOT file: " << m_rootfile->GetName() << endl;
  m_rootfile->Close();
}

void Handler::Analysis(){}


void Handler::Pulse( Sensor * se){

  //Doesn't check if channel is armed to pulse
  FrontEnd * fe = m_se_fe[se->GetName()];
  unsigned Chan = m_se_chan[se->GetName()];
  const std::string RegName = "STEP";
  uint8_t CurrentStep = fe->GetConfig()->GetField(RegName)->GetValue();

  if(CurrentStep != 0 ){
    CurrentStep=0;
    fe->GetConfig()->SetField(RegName,CurrentStep);
    fe->WriteRegisters();
    Send(fe);
  }
  //Send Pulse Command
  CurrentStep |= 1<<Chan;
  fe->GetConfig()->SetField(RegName,CurrentStep);
  fe->WriteRegisters();
  Send(fe);
  std::this_thread::sleep_for(std::chrono::milliseconds(m_pulse_delay));

  //Reset Pulse Command
  CurrentStep=0;
  fe->GetConfig()->SetField(RegName,CurrentStep);
  fe->WriteRegisters();
  Send(fe);
  std::this_thread::sleep_for(std::chrono::milliseconds(m_pulse_delay));  
  
}


void Handler::Pulse(){

  for(auto se : GetSensors()){
    Pulse(se);
  }
}
