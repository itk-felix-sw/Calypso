#include "BCMP/Register.h"


template <typename T>
Register<T>::Register(uint32_t Address):m_Address(Address){
  m_value=0;
  m_updated=false;
}

template <typename T>
Register<T>::~Register(){}

template <typename T>
void Register<T>::SetValue(T value){
  m_updated=true;
  m_value=value;
}


template <typename T>
T Register<T>::GetValue() const {
  return m_value;
}


template <typename T>
void Register<T>::Update(bool enable){
  m_updated=enable;
}

template <typename T>
bool Register<T>::IsUpdated(){
  return m_updated;
}

template <typename T>
const uint32_t Register<T>::GetAddress(){
  return m_Address;
}

template class Register<uint8_t>;
template class Register<uint16_t>;
template class Register<uint32_t>;
