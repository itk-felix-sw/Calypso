#include "BCMP/FrontEnd.h"
#include <iomanip>
#include <iostream>
using namespace std;
using namespace BCMP;

FrontEnd::FrontEnd(){
  m_verbose = false;
  m_name = "Calypso";
  m_config = new Configuration();
  m_active = true;
  m_sensors[0]=0;
  m_sensors[1]=0;
  m_sensors[2]=0;
  m_sensors[3]=0;

  m_LpGBT_ULDATASOURCE=0x0;
  m_LpGBTVer=1;

  m_LpGBTi2cAddr=0x70;
  m_BCMi2cAddr=0x1;

}

FrontEnd::~FrontEnd(){
  delete m_config;
  for(uint32_t i=0;i<4;i++){
    delete m_sensors[i];
  }
}

void FrontEnd::SetVerbose(bool enable){
  m_verbose = enable;
  for(uint32_t i=0;i<4;i++){
    if (m_sensors[i]!=0)
      m_sensors[i]->SetVerbose(enable);
  }
}

void FrontEnd::SetName(string name){
  m_name=name;
}

string FrontEnd::GetName(){
  return m_name;
}

Configuration * FrontEnd::GetConfig(){
  return m_config;
}

void FrontEnd::SetConfig(map<string,uint32_t> config){
  cout<< "FrontEnd::SetConfig to be implemented" << endl;
  m_config->SetFields(config);
}

bool FrontEnd::IsActive(){
  return m_active;
}

void FrontEnd::SetActive(bool active){
  m_active=active;
}

void FrontEnd::EnableAll(){}


void FrontEnd::DisableAll(){}


Sensor * FrontEnd::GetSensor(uint32_t index){
  return m_sensors[index];
}

void FrontEnd::SetSensor(uint32_t index, Sensor* sens){
  m_sensors[index]=sens;
}



void FrontEnd::ReadLpGBTRegisters(uint16_t Reg){

  m_LpGBTRegisters.push_back(std::make_pair(Reg,-1));  //I2CM0DATA0_V1 Write I2C target

}




void FrontEnd::WriteRegisters(){

  uint8_t Enable=0, Reset=1;


  
  // EnableChan(true,Enable);
  // EnableChan(true,Reset);
  
  // EnableChan(false,Reset);
  
  // EnableChan(true,Reset);
  // EnableChan(true,Enable);
  
  
  if(m_verbose) std::cout<<"Register Config: "<<std::endl;
  for(unsigned regInd=0;regInd<14;regInd++){
    WriteToCalypso(regInd*2, m_config->GetRegisterValue(regInd,false) , m_BCMi2cAddr);
    if(m_verbose) std::cout<<regInd<<" : "<<setfill('0')<<setw(4)<<std::hex<<m_config->GetRegisterValue(regInd,false)<<std::dec<<std::endl;
  
  }



}



void FrontEnd::WriteToCalypso(uint8_t regInd, uint16_t Data, uint8_t I2C ){
  
    // I2C_WRITE_CR = 0x0
    // I2C_WRITE_MSK = 0x1
    // I2C_1BYTE_WRITE = 0x2
    // I2C_1BYTE_READ = 0x3
    // I2C_1BYTE_WRITE_EXT = 0x4
    // I2C_1BYTE_READ_EXT = 0x5
    // I2C_1BYTE_RMW_OR = 0x6
    // I2C_1BYTE_RMW_XOR = 0x7
    // I2C_W_MULTI_4BYTE0 = 0x8
    // I2C_W_MULTI_4BYTE1 = 0x9
    // I2C_W_MULTI_4BYTE2 = 0xA
    // I2C_W_MULTI_4BYTE3 = 0xB
    // I2C_WRITE_MULTI = 0xC
    // I2C_READ_MULTI = 0xD
    // I2C_WRITE_MULTI_EXT = 0xE
    // I2C_READ_MULTI_EXT = 0xF
    // FREQ = 3        # I2C controller bus speed
    // SCLDRIVE = 0        # I2C controller SCLDriveMode (see lpGBT manual ch. 12.2.1)


  //I2C Master 0
  // uint16_t Command = 0x106;
  // uint16_t Data0 = 0x102;
  // uint16_t Address = 0x101;
  // if(m_LpGBTVer==0){
  //   Command=0x0F6;
  //   Data0=0x0F2;
  //   Address=0x0F1;
  // }
  //I2C Master 2
  uint16_t Command = 0x114;
  uint16_t Data0 = 0x110;
  uint16_t Address = 0x10F;
  uint16_t Status = 0x019B;
  if(m_LpGBTVer==0){

    // I2C Master 2
    if(m_LpGBTi2cMaster==2){
      Command=0x104;
      Data0=0x0100;
      Address=0x0FF;
      Status=0x18B;
    }
    // I2C Master 1
    else if(m_LpGBTi2cMaster==1){
      Command=0x0fd;
      Data0=0x0f9;
      Address=0x0F8;
      Status=0x176;
    }

    // I2C Master 0
    else if(m_LpGBTi2cMaster==0){
      Command=0x0f6;
      Data0=0x0f2;
      Address=0x0f1;
      Status=0x171;
    }
  }
  else if(m_LpGBTVer==1){

    // I2C Master 2
    if(m_LpGBTi2cMaster==2){
      Command=0x114;
      Data0=0x0110;
      Address=0x10F;
      Status=0x19B;
    }
    // I2C Master 1
    else if(m_LpGBTi2cMaster==1){
      Command=0x10d;
      Data0=0x109;
      Address=0x108;
      Status=0x186;
    }

    // I2C Master 0
    else if(m_LpGBTi2cMaster==0){
      Command=0x106;
      Data0=0x102;
      Address=0x101;
      Status=0x171;
    }
  }



  uint8_t Freq=0;
  uint8_t Bytes=3;
  uint8_t DataLoad=  (1 << 7) | (( Bytes & 0x1F) << 2) | (Freq & 0x3) ; //How many bytes will we write, at what frequency
  m_LpGBTRegisters.push_back(std::make_pair(Data0,DataLoad));  //I2CM0DATA0_V1 Write I2C target
  m_LpGBTRegisters.push_back(std::make_pair(Command,0x0));  //I2CM0CMD_V1 Write the command Write_CR
  unsigned dInd=0;

  //Writting the registers to write 
  m_LpGBTRegisters.push_back(std::make_pair(Data0,   (regInd & 0xFF )  ));
  m_LpGBTRegisters.push_back(std::make_pair(Data0+1, (Data & 0xFF) ));
  m_LpGBTRegisters.push_back(std::make_pair(Data0+2, (Data & 0xFF00 )>> 8  ));
  
    

  m_LpGBTRegisters.push_back(std::make_pair(Command, 0x8) ); //This is the write command for the first 4 registers
  
  //Intialise the data-transfer
  m_LpGBTRegisters.push_back(std::make_pair(Address, I2C));
  m_LpGBTRegisters.push_back(std::make_pair(Command, 0xC )); //Send Command Write Multi

  
  //m_LpGBTRegisters.push_back(std::make_pair(Status,-1));  //I2CM0ADDRESS_V1 Write I2C target


}



void FrontEnd::EnableChan(bool enable, unsigned chan){

  std::vector<uint16_t> Regs_v0 = { 0x121,0x120,0x11F, 0x11E,0x11D };
  std::vector<uint16_t> Regs_v1 = { 0x131,0x130,0x12F, 0x12E,0x12D };
  std::vector<uint16_t> Regs;
  if(m_LpGBTVer == 0)
    for(unsigned ind=0;ind<Regs_v0.size();ind++)
      Regs.push_back(Regs_v0.at(ind));
  else
    for(unsigned ind=0;ind<Regs_v1.size();ind++)
      Regs.push_back(Regs_v1.at(ind));


  uint8_t word=0;
  if (enable)
    word=3;
  if(enable){
    m_LpGBTRegisters.push_back(std::make_pair(Regs.at(0),0xFF)); // DPDATAPATTERN0 , 0xFF
    m_LpGBTRegisters.push_back(std::make_pair(Regs.at(1),0xFF)); // DPDATAPATTERN1 , 0xFF
    m_LpGBTRegisters.push_back(std::make_pair(Regs.at(2),0xFF)); // DPDATAPATTERN2 , 0xFF
    m_LpGBTRegisters.push_back(std::make_pair(Regs.at(3),0xFF)); // DPDATAPATTERN3 , 0xFF
  }

  if(chan==0){
    m_LpGBT_ULDATASOURCE = (  m_LpGBT_ULDATASOURCE & 0xFC ) | word;
  } 
  else if(chan==1){
    m_LpGBT_ULDATASOURCE = (  m_LpGBT_ULDATASOURCE & 0xF3 ) | (word<<2);
  }
  else if(chan==2){
    m_LpGBT_ULDATASOURCE = (  m_LpGBT_ULDATASOURCE & 0xCF ) | (word<<4);
  } 
  else if(chan==3){
    m_LpGBT_ULDATASOURCE = (  m_LpGBT_ULDATASOURCE & 0x3F ) | (word<<6);
  }
      
  m_LpGBTRegisters.push_back(std::make_pair(Regs.at(4),m_LpGBT_ULDATASOURCE)); //ULDATASOURCE5 

}


void FrontEnd::Encode(){
  
  m_bytes.clear();

  for(unsigned cInd=0; cInd<m_LpGBTRegisters.size(); cInd++){
    //If data is set to -1 to read action, if any other value due a write action
    std::vector<uint8_t> tmpData;
    bool read=true;
    if( m_LpGBTRegisters[cInd].second != -1 ){
      tmpData.push_back(m_LpGBTRegisters[cInd].second);
      read=false;      
    }

    std::vector<uint8_t> tmpBytes = prepareICNetioFrame(read, m_LpGBTRegisters[cInd].first,tmpData);
    m_bytes.push_back(tmpBytes);
  }
  m_LpGBTRegisters.clear();

}

std::vector<uint8_t> FrontEnd::prepareICNetioFrame(const bool read, const uint16_t startAddr, const std::vector<uint8_t>& data) {
  std::vector<uint8_t> retVal = {};
  
  size_t header_size = 6;
  if( m_LpGBTVer==0 )
    header_size = 7;
  constexpr size_t footer_size = 1;

  if( m_LpGBTVer==1 && data.size()>511 ){
    std::cout<<"Error data-size too big thats being tried to send to Netio"<<std::endl;
    return retVal;
  }
    
  if(!read)
    retVal.reserve(header_size + data.size() + footer_size);
  else
    retVal.reserve(header_size + footer_size);

  if( m_LpGBTVer==0 )
    retVal.push_back(0); // Reserved in LpGBT v0
  retVal.push_back((m_LpGBTi2cAddr << 1) + (read?0x1:0x0)); // GBTX I2C address and read/write bit
  retVal.push_back(1); // Command (not used in GBTX v1 + 2)
  retVal.push_back(data.size() & 0xFF); // Number of data bytes
  retVal.push_back((data.size() >> 8) & 0xFF);
    
  retVal.push_back(startAddr   & 0xFF); // Register (start) address
  retVal.push_back(startAddr >> 8 & 0xFF );
  
  //cout<<"Code: Data.size"<<std::hex<<data.size()<<" addr: "<<startAddr<<" data "<<int(data[0])<<std::dec<<std::endl;
  if(!read)
    for(auto& val: data)
      retVal.push_back(val);

  // For GBTx, skip first 2 bytes in parity check (see above)
  std::size_t NUM_PARTITY_BYTES_SKIP{0};
  if( m_LpGBTVer==0 )
    NUM_PARTITY_BYTES_SKIP=2;

  uint8_t parity = 0;
  for(size_t i = NUM_PARTITY_BYTES_SKIP;i < retVal.size(); ++i)
    parity ^= retVal[i];
  retVal.push_back(parity);


  return retVal;
}



uint32_t FrontEnd::GetLength(){
  return m_bytes.size();
}

uint32_t FrontEnd::GetSeqLength(unsigned ind){
  return m_bytes.at(ind).size();
}

uint8_t * FrontEnd::GetBytes(unsigned ind){
  return m_bytes.at(ind).data();

}


void  FrontEnd::SetLpGBTi2c(uint16_t i2c){
  m_LpGBTi2cAddr= i2c;  
}
void  FrontEnd::SetLpGBTi2cMaster(uint16_t i2c){
  m_LpGBTi2cMaster= i2c;  
}



void FrontEnd::SetCalypsoi2c(uint16_t i2c){
  m_BCMi2cAddr = i2c;
}

uint16_t FrontEnd::GetLpGBTi2c(){
  return m_LpGBTi2cAddr;
}

uint16_t FrontEnd::GetLpGBTi2cMaster(){
  return m_LpGBTi2cMaster;
}


uint16_t FrontEnd::GetCalypsoi2c(){
  return m_BCMi2cAddr;
}

void FrontEnd::SetLpGBTVer(uint16_t ver){
  m_LpGBTVer=ver;
}

uint8_t FrontEnd::GetLpGBTVer(){
  return m_LpGBTVer;
}


void FrontEnd::HandleData(const uint8_t * recv_data, const uint32_t recv_size){


  std::size_t FIRST_IC_PAYLOAD_BYTE = (m_LpGBTVer == 1) ? 6 : 7;

  if (recv_size > FIRST_IC_PAYLOAD_BYTE) {
    uint8_t tmpReply = recv_data[FIRST_IC_PAYLOAD_BYTE];
    
    m_replies.push_back(tmpReply);
  }


}

void FrontEnd::ClearReplies(){
  m_replies.clear();
}

uint8_t FrontEnd::GetReply(unsigned ind){
  if (ind<m_replies.size())
    return m_replies.at(ind);
  return 0;
}


size_t FrontEnd::GetReplySize(){
  return m_replies.size();
}
