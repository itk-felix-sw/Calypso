#include "BCMP/DataFrame.h"
#include <sstream>

using namespace std;
using namespace BCMP;

map<uint32_t, vector<uint32_t> > DataFrame::m_masks = {
  {0x01,{1,7}},{0x02,{1,6}},{0x04,{1,5}},{0x08,{1,4}},{0x10,{1,3}},{0x20,{1,2}},{0x40,{1,1}},{0x80,{1,0}},
  {0x03,{2,6}},{0x06,{2,5}},{0x0C,{2,4}},{0x18,{2,3}},{0x30,{2,2}},{0x60,{2,1}},{0xC0,{2,0}},
  {0x07,{3,5}},{0x0E,{3,4}},{0x1C,{3,3}},{0x38,{3,2}},{0x70,{3,1}},{0xE0,{3,0}},
  {0x0F,{4,4}},{0x1E,{4,3}},{0x3C,{4,2}},{0x78,{4,1}},{0xF0,{4,0}}
};

DataFrame::DataFrame(){
  m_size=8;
  m_toa=-1;
  m_tot=0;
}

DataFrame::DataFrame(uint32_t size, uint32_t toa, uint32_t tot){
  m_size=size;
  m_toa=toa;
  m_tot=tot;
}

DataFrame::~DataFrame(){}

string DataFrame::ToString(){
  ostringstream os;
  for(uint32_t i=0; i<m_size; i++){
    if(m_toa==i){ os << "1";}
    else{ os << "0";}
  }
  return os.str(); 
}

uint32_t DataFrame::UnPack(uint8_t * bytes, uint32_t maxlen){
  m_toa=-1;
  m_tot=0;
  uint32_t ff=0;
  uint32_t by=0;
  uint32_t bi=0;
  for(uint32_t i=0;i<m_size;i++){
    bi=i%8;
    if(((bytes[by]>>(7-bi))&0x1)==1){
      if(ff==0){m_toa=i;m_tot=1;ff++;}
      else if(ff==1){m_tot++;}
      else{/**This should not happen**/ break;}
    }
    if(bi==7){by++;}
  }
  return m_size/8;
}

uint32_t DataFrame::Pack(uint8_t * bytes){
  uint32_t by=0;
  uint32_t bi=0;
  bytes[0]=0;
  for(uint32_t i=0; i<m_size; i++){
    bi=i%8;
    if(i>=m_toa and m_toa+m_tot<i){
      bytes[by]|= 1<<(7-bi);
    }
    if(bi==7){by++;bytes[by]=0;}
  }
  return m_size/8;
}

void DataFrame::SetSize(uint32_t size){
  if(m_size%8!=0){return;}
  m_size=size;
}

void DataFrame::SetTOA(uint32_t toa){
  m_toa=toa;
}

void DataFrame::SetTOT(uint32_t tot){
  m_tot=tot;
}

uint32_t DataFrame::GetSize(){
  return m_size;
}

uint32_t DataFrame::GetTOA(){
  return m_toa;
}

uint32_t DataFrame::GetTOT(){
  return m_tot;
}
