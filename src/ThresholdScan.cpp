#include "BCMP/ThresholdScan.h"
#include "BCMP/SCurveFitter.h"

#include <iostream>
#include <chrono>
#include <signal.h>

#include "TCanvas.h"
#include "TROOT.h"


using namespace std;
using namespace BCMP;

bool ThresholdScan::m_run_forever=true;

void ThresholdScan::StopScan(int){
  cout << endl << "You pressed ctrl+c -> quitting" << endl;
  m_run_forever=false;
}




ThresholdScan::ThresholdScan(){
  m_run_forever=true;
  m_variable="armref_ctrl";
  m_start=180;
  m_stop=255; //127 is max
  m_step=5;
  m_nsteps=(m_stop-m_start)/m_step + 1; 

  // m_variable="atten";
  // m_start=0;
  // m_stop=15; //127 is max
  // m_step=1;
  // m_nsteps=(m_stop-m_start)/m_step + 1; 

  m_ntriggers = 100; //ms
  special_regs = {"attenuator", "atten"};
  m_longdelay=200;
  m_shortdelay=5;
}

ThresholdScan::~ThresholdScan(){}

void ThresholdScan::PreRun(){
  cout << "PreRun()" << endl;
  for(auto se : GetSensors()){
    cout << "ThresholdScan::PreRun" << endl;

    FrontEnd * fe = m_se_fe[se->GetName()];
    unsigned Chan = m_se_chan[se->GetName()];
    std::string RegName = "ch"+to_string(int(Chan))+"_"+m_variable;
    if(find(special_regs.begin(), special_regs.end(), m_variable) != special_regs.end())
      RegName=m_variable;
    
    fe->GetConfig()->SetField(RegName,m_start);
  }
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
}


void ThresholdScan::Run(){

  signal(SIGINT, StopScan);
  signal(SIGTERM, StopScan);

  
  //Prepare the trigger
  cout << "ThresholdScan::Run: Start" << endl;
  int nH=0;
  vector<Hit*> hits;
  uint64_t LastHeader=0;
  
  for(uint16_t step=0; step<m_nsteps && m_run_forever; step++){
    uint16_t val = m_start + step*m_step;

    cout << "ThresholdScan::Run: Working on Step: "<<step+1<<" / "<<m_nsteps << " Val: "<<val<<endl;

    // if(val==130) {
    // 	std::cout<<"We are at 130, setting threshold to 129."<<std::endl;
    // 	val=120;
    // }


    for(auto se : GetSensors()){

      se->SetActive(false);
      FrontEnd * fe = m_se_fe[se->GetName()];

      unsigned Chan = m_se_chan[se->GetName()];
      std::string RegName = "ch"+to_string(int(Chan))+"_"+m_variable;
      if(find(special_regs.begin(), special_regs.end(), m_variable) != special_regs.end())
	RegName=m_variable;
      
      fe->GetConfig()->SetField(RegName,val);
      m_hits[se->GetName()].push_back(new HitTree(("hits_"+to_string(val)+"_"+fe->GetName()).c_str(),(to_string(val)+"_"+fe->GetName()).c_str()));
      m_NHeaders[se->GetName()].push_back(0.0);
    }
    Config(); //Config all front-ends with the new value
    cout << "Clearing Hits"<<std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(m_shortdelay));
    for(auto se : GetSensors()){
      se->ClearHits();
      // if(step>10)
      // 	se->SetVerbose(true);
      se->SetActive(true);
      LastHeader=0;
    }


    for(auto se : GetSensors()){
      unsigned Chan = m_se_chan[se->GetName()];
      cout << "Starting Pulsing Chan " <<Chan<< endl;
      //Enable pulsing for a given channel
      FrontEnd * fe = m_se_fe[se->GetName()];

      std::string RegName = "ch"+to_string(int(Chan))+"_testen";
      fe->GetConfig()->SetField(RegName,1);
	
      for(unsigned i=0;i<4;i++){
	if(i==Chan) continue;
	RegName = "ch"+to_string(int(i))+"_testen";
	fe->GetConfig()->SetField(RegName,0);
      }
      //fe->GetConfig()->SetField("ATTEN",4);
      Config();
      cout << "PulseConfig Finished " <<Chan<< endl;
      Pulse(se);
      std::this_thread::sleep_for(std::chrono::milliseconds(1));
      
      
      for(unsigned Count=0;Count<m_ntriggers;Count++){
	Pulse(se);
	if((Count+1)%50==0) cout<<(Count+1)<<" triggers sent"<<endl;
	//cout<<(Count+1)<<" triggers sent"<<endl;
      }

    }
    cout << "Pulsing finnished" << endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(m_longdelay));

    

    auto start = chrono::steady_clock::now();
    auto end = chrono::steady_clock::now();
    cout<<"Starting Storage"<<std::endl;
    bool Run=true;
    uint32_t TrigCount=0;
    while(Run){
      std::this_thread::sleep_for(std::chrono::milliseconds(m_shortdelay));  
      for(auto sensor : GetSensors()){

	sensor->SetActive(false);
	hits=sensor->GetHits(30000);
	
	m_NHeaders[sensor->GetName()][step]=sensor->GetNHeaders();
	// if( m_NHeaders[sensor->GetName()][step]>m_ntriggers ){
	//   sensor->SetActive(false); //Stops data-taking
	if( hits.size() ==0 ){
	  Run=false;
	} //Stop running when hit buffer is empty
      
	  //std::cout<<"Got nH: "<<nH<<std::endl;
	nH = hits.size();
	for (unsigned int h=0; h<nH; h++) {
	  Hit* hit = hits.at(h);

	  m_hits[sensor->GetName()][step]->Fill(hit);
	  TrigCount++;


	  
	}
      

	sensor->ClearHits(nH);
      }
	  
      end = chrono::steady_clock::now();
    }
    std::cout<<"Step: "<<step<<" finnished. "<< TrigCount<<" / "<<(GetSensors().size()*m_ntriggers) <<" triggers collected."<<std::endl;
      }
}

void ThresholdScan::Analysis(){

  Fitter::SCurveFitter scf;
  gROOT->SetBatch();
  TCanvas* can=new TCanvas("plot","plot",800,600);

  
  uint32_t MaxBCID=3562;
  static uint32_t MaxL1SubID=63;
  //Now time to use the tree to do post processing
  for(auto fe : GetSensors()){
      //Save Raw Hits file, before any potential medelling


    TH1F* h_thr_vs_tot=new TH1F(("Thr_vs_tot_"+fe->GetName()).c_str(),(";"+m_variable+";ToT").c_str(),m_nsteps,m_start-(m_step/2),m_stop+(m_step/2));
    TH1F* h_thr_vs_toa=new TH1F(("Thr_vs_toa_"+fe->GetName()).c_str(),(";"+m_variable+";ToA").c_str(),m_nsteps,m_start-(m_step/2),m_stop+(m_step/2));
    TH1F* h_thr_vs_hits=new TH1F(("Thr_vs_hits_"+fe->GetName()).c_str(),(";"+m_variable+";Hits").c_str(),m_nsteps,m_start-(m_step/2),m_stop+(m_step/2));
    TH1F* h_thr_vs_hits_corr=new TH1F(("Thr_vs_hits_corr_"+fe->GetName()).c_str(),(";"+m_variable+";Hits").c_str(),m_nsteps,m_start-(m_step/2),m_stop+(m_step/2));
    TH1F* h_thr_vs_headers=new TH1F(("Thr_vs_headers_"+fe->GetName()).c_str(),(";"+m_variable+";Headers").c_str(),m_nsteps,m_start-(m_step/2),m_stop+(m_step/2));




    for(unsigned step=0;step<m_nsteps;step++){

      std::cout<<"Analyzing step: "<<step<<"/"<<m_nsteps<<std::endl;
      TH1I* h_tot=new TH1I(("ToT_"+fe->GetName()+"_"+to_string(step)).c_str(),";ToT",32,0.5,32.5);
      TH1I* h_toa=new TH1I(("ToA_"+fe->GetName()+"_"+to_string(step)).c_str(),";ToA",5000,0.5,5000.5);

      if(m_hits[fe->GetName()].size()<=step) continue;
      
      //m_hits[fe->GetName()][step]->Write();
      cout<<"Begin: "<<m_hits[fe->GetName()][step]->GetEntries()<<"/"<<m_NHeaders[fe->GetName()][step]<<endl;
      uint32_t PrevL1ID=-1;
      uint32_t PrevL1SubID=-1;
      for(Long64_t evt=0;evt<m_hits[fe->GetName()][step]->GetEntries();evt++){
	m_hits[fe->GetName()][step]->GetEntry(evt);
	Hit * hit = m_hits[fe->GetName()][step]->Get();

	uint32_t ToT=hit->GetTOT();
	uint32_t ToA=hit->GetTOA();
	uint32_t BCID=hit->GetBCID();
	uint32_t L1ID=hit->GetL1ID();
	uint32_t L1SubID=hit->GetL1SubID();
	//If L1SubID is not zero, it means we got an an extra header since we are not supressing 0's. 
	// if( step==15)
	//   std::cout<<"ToT: "<<ToT<<" ToA: "<<ToA<<" BCID: "<<BCID<<" L1ID: "<<L1ID<<" L1SubID: "<<L1SubID<<std::endl;
	if(L1SubID!=0 && m_NHeaders[fe->GetName()][step]>0 ) {
	  uint64_t bf = m_NHeaders[fe->GetName()][step];

	  if(PrevL1ID!=L1ID){
	    m_NHeaders[fe->GetName()][step]-=1;
	    PrevL1ID=L1ID;
	    PrevL1SubID=L1SubID;
	  
	    if (bf < m_NHeaders[fe->GetName()][step]) {
	      std::cout<<"Something went wrong here: after->"<<m_NHeaders[fe->GetName()][step]<<" before->"<<bf<<" Distance forced 1 Step: "<<step<<std::endl; 
	    }
	  }
	}


	uint32_t ToT2=0;
	uint32_t ToA2=0;
	uint32_t BCID2=0;
	uint32_t L1ID2=0;
	uint32_t L1SubID2=0;

 
	if (ToT==0 and ToA==0) continue;
	//Search for half events (Events passing through two event boundires and skip the next event)
	bool FoundMatching=false;
	uint64_t SkipDistance=0;
	if( (ToA+ToT) == 32 ) //Aka if the word extends to the next event
	  {
	    uint32_t tmpToT=ToT;
	    uint32_t tmpToA=ToA;
	    uint32_t tmpBCID=BCID;
	    uint32_t tmpL1ID=L1ID;
	    uint32_t tmpL1SubID=L1SubID;

	    for(uint64_t nextevt=1;nextevt<(64*6);nextevt++){
	      m_hits[fe->GetName()][step]->GetEntry(evt+nextevt);
	      hit = m_hits[fe->GetName()][step]->Get();

	      ToT2=hit->GetTOT();
	      ToA2=hit->GetTOA();
	      BCID2=hit->GetBCID();
	      L1ID2=hit->GetL1ID();
	      L1SubID2=hit->GetL1SubID();
	      if (ToT2==0 and ToA2==0) continue;

	      //If a connecting event is count skip that event and ToT's together

	      if ((  ( (MaxBCID+1)*(BCID2==0 && (BCID2!=tmpBCID)) + BCID2 + (MaxL1SubID+1)*(L1SubID2==0 && (L1SubID2!=tmpL1SubID)) + L1SubID2 -1  ) == (tmpBCID+tmpL1SubID)) and (ToA2==0 and ToT2!=0) ){
		ToT += ToT2 ;
		
		FoundMatching=true;
		SkipDistance=nextevt;
		// if(step==15) {
		//   std::cout<<"FoundMatching: "<<FoundMatching<<" NEvt: "<<nextevt;
		//   std::cout<<" ToT: "<<ToT2<<" ToA: "<<ToA2<<" BCID: "<<BCID2<<" L1ID: "<<L1ID2<<" L1SubID: "<<L1SubID2<<std::endl;
		// } 
	      }
	      else{
		break;
	      }
	      
	      if(FoundMatching and ToT2!=32) break; 
	      tmpToT=ToT2;
	      tmpToA=ToA2;
	      tmpBCID=BCID2;
	      tmpL1ID=L1ID2;
	      tmpL1SubID=L1SubID2;
	    }
	  }

	uint32_t ToT_next=0;
	uint32_t ToA_next=0;
	uint32_t BCID_next=0;
	uint32_t L1ID_next=0;
	uint32_t L1SubID_next=0;

	//Search for the next event
	for(Long64_t nextevt=(SkipDistance+1);nextevt< (m_hits[fe->GetName()][step]->GetEntries() - evt);nextevt++) {
	  m_hits[fe->GetName()][step]->GetEntry(evt+nextevt);
	  hit = m_hits[fe->GetName()][step]->Get();
	  ToT_next=hit->GetTOT();
	  ToA_next=hit->GetTOA();
	  BCID_next=hit->GetBCID();
	  L1ID_next=hit->GetL1ID();
	  L1SubID_next=hit->GetL1SubID();
	  if (ToT_next==0 and ToA_next==0) continue;
	  break; //If found the next evet that is not zero
	}

	uint64_t DistanceToNext = ((MaxBCID+1)*(BCID_next<BCID) + BCID_next-BCID)*32 + ToA_next - ToA;
	// //cout<<"Calculating distance "<<DistanceToNext<<" = Bn: "<<BCID_next<<" Bo: "<<BCID<<" Tn: "<<ToA_next<<" To: "<<ToA<<std::endl;
      
	h_toa->Fill(DistanceToNext);
	h_tot->Fill(ToT);			    

	if(FoundMatching){
	  evt+=SkipDistance; //skip the next event,
	  uint64_t bf = m_NHeaders[fe->GetName()][step];
	  m_NHeaders[fe->GetName()][step]-=SkipDistance;
	  if(PrevL1ID==L1ID2 && PrevL1SubID==L1SubID2){
	    m_NHeaders[fe->GetName()][step]+=1;
	    PrevL1ID=L1ID2;
	    PrevL1SubID=L1SubID2;
	  }

	  if (bf < m_NHeaders[fe->GetName()][step] ) {
	    std::cout<<"Something went wrong here: after->"<<m_NHeaders[fe->GetName()][step]<<" before->"<<bf<<" Distance->"<<SkipDistance<<" Step: "<<step<<std::endl; 

	  }
	}

      }
      cout<<"End: "<<h_toa->GetEntries()<<"/"<<m_NHeaders[fe->GetName()][step]<<endl;
      h_toa->Write();
      h_tot->Write();
      
      h_thr_vs_tot->SetBinContent(step+1,h_tot->GetMean());
      h_thr_vs_tot->SetBinError(step+1,h_tot->GetStdDev());

      h_thr_vs_toa->SetBinContent(step+1,h_toa->GetMean());
      h_thr_vs_toa->SetBinError(step+1,h_toa->GetStdDev());

      h_thr_vs_hits->SetBinContent(step+1,h_toa->GetEntries());
      if(h_toa->GetEntries()< m_ntriggers*1.2 )
	h_thr_vs_hits_corr->SetBinContent(step+1,h_toa->GetEntries());
      else
	h_thr_vs_hits_corr->SetBinContent(step+1,m_ntriggers);
      h_thr_vs_headers->SetBinContent(step+1,m_NHeaders[fe->GetName()][step]);





      delete h_toa;
      delete h_tot;
      delete m_hits[fe->GetName()][step];
      
    }
    
    h_thr_vs_tot->Write();
    h_thr_vs_toa->Write();
    h_thr_vs_hits->Write();
    h_thr_vs_hits_corr->Write();
    h_thr_vs_headers->Write();

    h_thr_vs_tot->Draw("E");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_ToT_1D.pdf").c_str() );
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_ToT_1D.png").c_str() );

    h_thr_vs_toa->Draw("E");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_ToA_1D.pdf").c_str() );
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_ToA_1D.png").c_str() );

    h_thr_vs_hits->Draw("E");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_hits_1D.pdf").c_str() );
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_hits_1D.png").c_str() );

    h_thr_vs_headers->Draw("E");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_headers_1D.pdf").c_str() );
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_headers_1D.png").c_str() );


    float PreMean, mean ;
    float PreSigma, sigma;
    float max;

    uint8_t trialCount=0;

    std::cout<<"Before Smoothing"<<std::endl;
    int nBins = h_thr_vs_hits_corr->GetNbinsX();
    for (int bin = 1; bin <= nBins; ++bin) { // Start from bin 1, as bin 0 is underflow
      double binCenter = h_thr_vs_hits_corr->GetBinCenter(bin);
      double content = h_thr_vs_hits_corr->GetBinContent(bin);
      double error = h_thr_vs_hits_corr->GetBinError(bin);

      std::cout << bin << "\t"
		<< binCenter << "\t\t"
		<< content << "\t\t"
		<< error << std::endl;
    }
    do{
      scf.setDataFromHistIgnoreBin(h_thr_vs_hits_corr,130.);
      scf.guessInitialParameters();
      PreMean = scf.getSCurve().mu;
      PreSigma = scf.getSCurve().sigma;
      max = scf.getSCurve().norm;

      std::cout<<"SCurve Pre Fit: Guess "<<PreMean<<" +- "<<PreSigma<<" of max value: "<<max<<std::endl;
      scf.fit();
  
      mean = scf.getSCurve().mu;
      sigma = scf.getSCurve().sigma;

      h_thr_vs_hits_corr->Smooth();
    
    
      trialCount++;
      std::cout<<"SCurve Fit: "<<mean<<" +- "<<sigma<<" trialCount "<<int(trialCount)<<std::endl;
    }while(trialCount<=3 and ( ( (sigma>mean) || (fabs(PreMean - mean)> 2*PreSigma) ) or sigma or mean)  );
    
    std::cout<<"After Smoothing"<<std::endl;
    nBins = h_thr_vs_hits_corr->GetNbinsX();
    for (int bin = 1; bin <= nBins; ++bin) { // Start from bin 1, as bin 0 is underflow
      double binCenter = h_thr_vs_hits_corr->GetBinCenter(bin);
      double content = h_thr_vs_hits_corr->GetBinContent(bin);
      double error = h_thr_vs_hits_corr->GetBinError(bin);

      std::cout << bin << "\t"
		<< binCenter << "\t\t"
		<< content << "\t\t"
		<< error << std::endl;
    }

    

    //Fitting s-curves
    // scf.setDataFromHist(h_thr_vs_hits_corr);
    // scf.guessInitialParameters();
    // scf.fit();

    // float mean = scf.getSCurve().mu;
    // float sigma = scf.getSCurve().sigma;

    std::cout<<"SCurve Fit: "<<mean<<" +- "<<sigma<<std::endl;


    delete h_thr_vs_tot;
    delete h_thr_vs_toa;
    delete h_thr_vs_hits;
    delete h_thr_vs_headers;


  }
 
}                                                                                                                                        
