#include "BCMP/NoiseScan.h"
#include "BCMP/SCurveFitter.h"
#include <iostream>
#include <chrono>
#include <signal.h>

#include "TCanvas.h"
#include "TROOT.h"
#include "TMath.h"


using namespace std;
using namespace BCMP;


NoiseScan::NoiseScan(){

  m_variable="armref_ctrl";
  m_start=0;
  m_stop=250; //127 is max
  m_step=5;
  m_nsteps=(m_stop-m_start)/m_step + 1; 
  m_tduration=5;//seconds
  m_elapsedSeconds=0;


}

NoiseScan::~NoiseScan(){}

void NoiseScan::PreRun(){
  cout << "PreRun()" << endl;
  for(auto se : GetSensors()){
    cout << "NoiseScan::PreRun" << endl;

    FrontEnd * fe = m_se_fe[se->GetName()];
    unsigned Chan = m_se_chan[se->GetName()];
    std::string RegName = "ch"+to_string(int(Chan))+"_"+m_variable;
    if(m_variable=="attenuator")
      RegName=m_variable;
    

    fe->GetConfig()->SetField(RegName,m_start);


    // m_h_tot_raw[fe->GetName()]=new TH1I(("ToT_Raw_"+fe->GetName()).c_str(),";ToT",32,0.5,32.5);
    // m_h_tot_real[fe->GetName()]=new TH1I(("ToT_Real_"+fe->GetName()).c_str(),";ToT",32,0.5,32.5);
    // m_h_toa_real[fe->GetName()]=new TH1I(("ToA_Real_"+fe->GetName()).c_str(),";ToA Distance",10000,0.5,10000.5);
    // m_h_toa_raw[fe->GetName()]=new TH1I(("ToA_Raw_"+fe->GetName()).c_str(),";ToA",3200,0.5,3200.5);
    // m_h_bcid[fe->GetName()]=new TH1I(("BCID_"+fe->GetName()).c_str(),";BCID",3564,-0.5,3563.5);
    // m_h_L1ID[fe->GetName()]=new TH1D(("L1ID_"+fe->GetName()).c_str(),";L1ID",8388608,-0.5,8388607.5);
    // m_h_L1SubID[fe->GetName()]=new TH1I(("L1SubID_"+fe->GetName()).c_str(),";L1SubID",64,-0.5,63.5);
  }
}


void NoiseScan::Run(){

  //Prepare the trigger
  cout << "NoiseScan::Run: Start" << endl;
  int nH=0;
  vector<Hit*> hits;
  uint64_t LastHeader=0;
  
  for(unsigned step=0; step<m_nsteps; step++){
    uint16_t val = m_start + step*m_step;
    cout << "NoiseScan::Run: Working on Step: "<<step+1<<" / "<<m_nsteps <<" Val: "<<val<< endl;
    for(auto se : GetSensors()){
      se->SetActive(false);
      FrontEnd * fe = m_se_fe[se->GetName()];
      unsigned Chan = m_se_chan[se->GetName()];
      std::string RegName = "ch"+to_string(int(Chan))+"_"+m_variable;
      fe->GetConfig()->SetField(RegName,val);
      m_hits[se->GetName()].push_back(new HitTree(("hits_"+to_string(val)+"_"+fe->GetName()).c_str(),(to_string(val)+"_"+fe->GetName()).c_str()));
      m_NHeaders[se->GetName()].push_back(0.0);
    }
    Config(); //Config all front-ends with the new value
    cout << "Clearing Hits"<<std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    for(auto se : GetSensors()){
      se->ClearHits();
      se->SetVerbose(false);
      se->SetActive(true);
      LastHeader=0;
    }


    auto start = chrono::steady_clock::now();
    auto end = chrono::steady_clock::now();
    cout<<"Starting run"<<std::endl;
    bool Run=true;
    uint32_t PrevBCID=-1;
    uint32_t PrevL1ID=-1; 
    uint32_t PrevL1SubID=-1; 
    uint32_t PrevToA=-1; 
    uint32_t PrevToT=-1; 
    uint32_t TrigCount=0;
    while(Run){
      m_elapsedSeconds = chrono::duration_cast<chrono::seconds>(chrono::steady_clock::now() - start).count();
      if (m_elapsedSeconds >= m_tduration){
	Run=false;
	for(auto sensor : GetSensors()){
	  sensor->SetActive(false);
	}
      }
      for(auto sensor : GetSensors()){
        hits=sensor->GetHits(30000);
        m_NHeaders[sensor->GetName()][step]=sensor->GetNHeaders();
        nH=hits.size();
        if (nH==0) {
          this_thread::sleep_for(chrono::milliseconds(100)); // 500 Millisconds is the time to collect 20000 hits
        }
        else {
          //std::cout<<"Got nH: "<<nH<<std::endl;
          for (unsigned int h=0; h<nH; h++) {
            Hit* hit = hits.at(h);
	    	    
            m_hits[sensor->GetName()][step]->Fill(hit);
	    TrigCount++;
          }
        }
        sensor->ClearHits(nH);
      }
      m_elapsedSeconds = chrono::duration_cast<chrono::seconds>(chrono::steady_clock::now() - start).count();
      if (m_elapsedSeconds >= m_tduration) Run=false;
      end = chrono::steady_clock::now();
    }
    std::cout<<"Step: "<<step<<" finnished. "<< TrigCount <<" triggers collected."<<std::endl;

  }
}

void NoiseScan::Analysis(){


  //Analysis is different for noise scans, as at low noise the FE always is outputing 1`
  gROOT->SetBatch();
  Fitter::SCurveFitter scf;
  TCanvas* can=new TCanvas("plot","plot",800,600);

  
  uint32_t MaxBCID=3562;
  static uint32_t MaxL1SubID=63;
  //Now time to use the tree to do post processing
  for(auto fe : GetSensors()){
      //Save Raw Hits file, before any potential medelling


    TH1F* h_thr_vs_tot=new TH1F(("Thr_vs_tot_"+fe->GetName()).c_str(),";Threshold",m_nsteps,m_start-(m_step/2),m_stop+(m_step/2));
    TH1F* h_thr_vs_toa=new TH1F(("Thr_vs_toa_"+fe->GetName()).c_str(),";Threshold",m_nsteps,m_start-(m_step/2),m_stop+(m_step/2));
    TH1F* h_thr_vs_hits=new TH1F(("Thr_vs_hits_"+fe->GetName()).c_str(),";Threshold",m_nsteps,m_start-(m_step/2),m_stop+(m_step/2));
    TH1F* h_thr_vs_headers=new TH1F(("Thr_vs_headers_"+fe->GetName()).c_str(),";Threshold",m_nsteps,m_start-(m_step/2),m_stop+(m_step/2));




    for(unsigned step=0;step<m_nsteps;step++){


      float StepValue =  m_start + step*m_step;
      
      std::cout<<"Analyzing step: "<<step<<"/"<<m_nsteps<<std::endl;
      TH1I* h_tot=new TH1I(("ToT_"+fe->GetName()+"_"+to_string(step)).c_str(),";ToT",33,-0.5,32.5);
      TH1I* h_toa=new TH1I(("ToA_"+fe->GetName()+"_"+to_string(step)).c_str(),";ToA",33,-0.5,32.5);

      //m_hits[fe->GetName()][step]->Write();
      cout<<"Begin: "<<m_hits[fe->GetName()][step]->GetEntries()<<"/"<<m_NHeaders[fe->GetName()][step]<<endl;
      uint32_t PrevL1ID=-1;
      uint32_t PrevL1SubID=-1;
      
      


      for(Long64_t evt=0;evt<m_hits[fe->GetName()][step]->GetEntries();evt++){
	m_hits[fe->GetName()][step]->GetEntry(evt);
	Hit * hit = m_hits[fe->GetName()][step]->Get();

	uint32_t ToT=hit->GetTOT();
	uint32_t ToA=hit->GetTOA();
	uint32_t BCID=hit->GetBCID();
	uint32_t L1ID=hit->GetL1ID();
	uint32_t L1SubID=hit->GetL1SubID();

 
	if (ToT==0 and ToA==0) continue;

	if(PrevL1ID!=L1ID or PrevL1SubID!=L1SubID) 
	  h_thr_vs_headers->Fill(StepValue); //We count the header no matter what
	h_toa->Fill(ToA);
	h_tot->Fill(ToT);			    

	PrevL1ID=L1ID;
	PrevL1SubID=L1SubID;
      }

      
      cout<<"End: "<<h_toa->GetEntries()<<"/"<<m_NHeaders[fe->GetName()][step]<<endl;
      h_toa->Write();
      h_tot->Write();
      
      h_thr_vs_tot->SetBinContent(step+1,h_tot->GetMean());
      h_thr_vs_tot->SetBinError(step+1,h_tot->GetStdDev());

      h_thr_vs_toa->SetBinContent(step+1,h_toa->GetMean());
      h_thr_vs_toa->SetBinError(step+1,h_toa->GetStdDev());

      h_thr_vs_hits->SetBinContent(step+1,h_toa->GetEntries());
      h_thr_vs_hits->SetBinError(step+1,sqrt(h_toa->GetEntries()));

      delete h_toa;
      delete h_tot;
      delete m_hits[fe->GetName()][step];
      
    }
    
    h_thr_vs_tot->Write();
    h_thr_vs_toa->Write();
    h_thr_vs_hits->Write();
    h_thr_vs_headers->Write();


    h_thr_vs_tot->Draw();
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_ToT_1D.pdf").c_str() );
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_ToT_1D.png").c_str() );

    h_thr_vs_toa->Draw();
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_ToA_1D.pdf").c_str() );
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_ToA_1D.png").c_str() );

    h_thr_vs_hits->Draw();
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_hits_1D.pdf").c_str() );
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_hits_1D.png").c_str() );

    h_thr_vs_headers->Draw();
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_headers_1D.pdf").c_str() );
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"_Thres_headers_1D.png").c_str() );


    float PreMean, mean ;
    float PreSigma, sigma;
    float max;

    uint8_t trialCount=0;
    do{
      scf.setDataFromHist(h_thr_vs_tot);
      scf.guessInitialParameters();
      PreMean = scf.getSCurve().mu;
      PreSigma = scf.getSCurve().sigma;
      max = scf.getSCurve().norm;

      std::cout<<"SCurve Pre Fit: Guess "<<PreMean<<" +- "<<PreSigma<<" of max value: "<<max<<std::endl;
      scf.fit();
  
      mean = scf.getSCurve().mu;
      sigma = scf.getSCurve().sigma;

      h_thr_vs_tot->Smooth();
    
    
      trialCount++;
      std::cout<<"SCurve Fit: "<<mean<<" +- "<<sigma<<" trialCount "<<int(trialCount)<<std::endl;
    }while(trialCount<=3 and ((sigma>mean) || (fabs(PreMean - mean)> 2*PreSigma) ) );



    //If fitting fails, use the PreMean version
    if((sigma>mean) || (fabs(PreMean - mean)> 2*PreSigma) ){
      m_scurve_mean  = PreMean;
      m_scurve_sigma = PreSigma;
    }
    else{
      m_scurve_mean  = mean;
      m_scurve_sigma = sigma;
    }
    std::cout<<"SCurve Fit: "<<m_scurve_mean<<" +- "<<m_scurve_sigma<<std::endl;





    


    delete h_thr_vs_tot;
    delete h_thr_vs_toa;
    delete h_thr_vs_hits;
    delete h_thr_vs_headers;

  }
 
}                                                                                                                                        
