#include <cmdl/cmdargs.h>
#include <iostream>
#include <BCMP/Decoder.h>

using namespace std;
using namespace BCMP;

int main(int argc, char *argv[]){

  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  
  CmdLine cmdl(*argv,&cVerbose,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  vector<uint8_t> bytestream;
  bytestream.resize(32);
  
  Decoder * decoder = new Decoder();

  decoder->AddFrame(new DataFrame(8,1,1));
  decoder->AddFrame(new DataFrame(8,1,1));
  decoder->AddFrame(new DataFrame(8,1,1));
  
  cout << "Encode the data" << endl;
  decoder->Encode();
  
  cout << "ByteStream:" << decoder->GetByteString() << endl;
  
  cout << "Decode the data" << endl;
  decoder->Decode();
  
  for(auto frame : decoder->GetFrames()){
    cout << frame->ToString() << endl;
  }
  
  cout << "Cleanup the house" << endl;
  delete decoder;

  cout << "Have a nice day" << endl;
  return 0;
}
