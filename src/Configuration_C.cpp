#include "BCMP/Configuration_C.h"

using namespace std;
using namespace BCMP;

std::map<std::string, uint32_t> Configuration_C::m_name2field={
  {"attenuator", ATTENUATOR},
  {"ch0_lumi", LUMI_ENABLE_0},
  {"ch1_lumi", LUMI_ENABLE_1},
  {"ch2_lumi", LUMI_ENABLE_2},
  {"ch3_lumi", LUMI_ENABLE_3},
  {"ch0_abort", ABORT_ENABLE_0},
  {"ch1_abort", ABORT_ENABLE_1},
  {"ch2_abort", ABORT_ENABLE_2},
  {"ch3_abort", ABORT_ENABLE_3},
  {"ch0_polarity", POLARITY_0},
  {"ch1_polarity", POLARITY_1},
  {"ch2_polarity", POLARITY_2},
  {"ch3_polarity", POLARITY_3},
  {"ch0_testen", TEST_ENABLE_0},
  {"ch1_testen", TEST_ENABLE_1},
  {"ch2_testen", TEST_ENABLE_2},
  {"ch3_testen", TEST_ENABLE_3},
  {"ibiastrim", IBIAS_TRIM},
  {"ibiasen", IBIAS_ENABLE},
  {"ch0_zdc_scap1", ZDC_SCAP1_0},
  {"ch1_zdc_scap1", ZDC_SCAP1_1},
  {"ch2_zdc_scap1", ZDC_SCAP1_2},
  {"ch3_zdc_scap1", ZDC_SCAP1_3},
  {"ch0_oc_looppd", OC_LOOPPD_0},
  {"ch1_oc_looppd", OC_LOOPPD_1},
  {"ch2_oc_looppd", OC_LOOPPD_2},
  {"ch3_oc_looppd", OC_LOOPPD_3},
  {"ch0_oc_gtpd", OC_GTPD_0},
  {"ch1_oc_gtpd", OC_GTPD_1},
  {"ch2_oc_gtpd", OC_GTPD_2},
  {"ch3_oc_gtpd", OC_GTPD_3},
  {"ch0_delay_ctrl", DELAY_CTRL_0},
  {"ch1_delay_ctrl", DELAY_CTRL_1},
  {"ch2_delay_ctrl", DELAY_CTRL_2},
  {"ch3_delay_ctrl", DELAY_CTRL_3},
  {"ch0_amp2gain_ctrl", AMP2GAIN_CTRL_0},
  {"ch1_amp2gain_ctrl", AMP2GAIN_CTRL_1},
  {"ch2_amp2gain_ctrl", AMP2GAIN_CTRL_2},
  {"ch3_amp2gain_ctrl", AMP2GAIN_CTRL_3},
  {"ch0_rfbshrt", RFBSHRT_0},
  {"ch1_rfbshrt", RFBSHRT_1},
  {"ch2_rfbshrt", RFBSHRT_2},
  {"ch3_rfbshrt", RFBSHRT_3},
  {"ch0_adrvr_logain", ADRVR_LOGAIN_0},
  {"ch1_adrvr_logain", ADRVR_LOGAIN_1},
  {"ch2_adrvr_logain", ADRVR_LOGAIN_2},
  {"ch3_adrvr_logain", ADRVR_LOGAIN_3},
  {"ch0_armref_dacz", ARMREF_DACZ_0},
  {"ch1_armref_dacz", ARMREF_DACZ_1},
  {"ch2_armref_dacz", ARMREF_DACZ_2},
  {"ch3_armref_dacz", ARMREF_DACZ_3},
  {"ch0_armref_ovd", ARMREF_OVD_0},
  {"ch1_armref_ovd", ARMREF_OVD_1},
  {"ch2_armref_ovd", ARMREF_OVD_2},
  {"ch3_armref_ovd", ARMREF_OVD_3},
  {"ch0_armhystpd", ARMHYSTPD_0},
  {"ch1_armhystpd", ARMHYSTPD_1},
  {"ch2_armhystpd", ARMHYSTPD_2},
  {"ch3_armhystpd", ARMHYSTPD_3},
  {"ch0_armref_ctrl", ARMREF_CTRL_0},
  {"ch1_armref_ctrl", ARMREF_CTRL_1},
  {"ch2_armref_ctrl", ARMREF_CTRL_2},
  {"ch3_armref_ctrl", ARMREF_CTRL_3},
  {"ch0_zdc_scap2", ZDC_SCAP2_0},
  {"ch1_zdc_scap2", ZDC_SCAP2_1},
  {"ch2_zdc_scap2", ZDC_SCAP2_2},
  {"ch3_zdc_scap2", ZDC_SCAP2_3},
  {"lvds_bias", LVDS_BIAS}
};

Configuration_C::Configuration_C(){

  m_verbose = false;
  m_registers.resize(NUM_REGISTERS);

  m_fields[LUMI_ENABLE_3]        = new Field(&m_registers[  0], 0, 4,0); 
  m_fields[LUMI_ENABLE_2]        = new Field(&m_registers[  0], 4, 4,0); 
  m_fields[LUMI_ENABLE_1]        = new Field(&m_registers[  0], 8, 4,0); 
  m_fields[LUMI_ENABLE_0]        = new Field(&m_registers[  0],12, 4,0); 
  m_fields[ABORT_ENABLE_3]       = new Field(&m_registers[  1], 0, 4,0);
  m_fields[ABORT_ENABLE_2]       = new Field(&m_registers[  1], 4, 4,0);
  m_fields[ABORT_ENABLE_1]       = new Field(&m_registers[  1], 8, 4,0);
  m_fields[ABORT_ENABLE_0]       = new Field(&m_registers[  1],12, 4,0);
  m_fields[LVDS_BIAS]            = new Field(&m_registers[  2], 0, 2,0);
  m_fields[POLARITY_3]           = new Field(&m_registers[  2], 3, 1,0);
  m_fields[POLARITY_2]           = new Field(&m_registers[  2], 4, 1,0);
  m_fields[POLARITY_1]           = new Field(&m_registers[  2], 5, 1,0);
  m_fields[POLARITY_0]           = new Field(&m_registers[  2], 6, 1,0);
  m_fields[TEST_ENABLE_3]        = new Field(&m_registers[  2], 7, 1,0);
  m_fields[TEST_ENABLE_2]        = new Field(&m_registers[  2], 8, 1,0);
  m_fields[TEST_ENABLE_1]        = new Field(&m_registers[  2], 9, 1,0);
  m_fields[TEST_ENABLE_0]        = new Field(&m_registers[  2],10, 1,0);
  m_fields[IBIAS_TRIM]           = new Field(&m_registers[  2],11, 4,0);
  m_fields[IBIAS_ENABLE]         = new Field(&m_registers[  2],15, 1,0);
  m_fields[ATTENUATOR]           = new Field(&m_registers[  3], 0, 2, &m_registers[  5], 0, 2, 3);
  m_fields[ZDC_SCAP1_0]          = new Field(&m_registers[  3], 2, 6,0);
  m_fields[OC_LOOPPD_0]          = new Field(&m_registers[  3], 7, 1,0);
  m_fields[OC_GTPD_0]            = new Field(&m_registers[  3], 8, 1,0);
  m_fields[DELAY_CTRL_0]         = new Field(&m_registers[  3], 9, 4,0);
  m_fields[AMP2GAIN_CTRL_0]      = new Field(&m_registers[  3],13, 2,0);
  m_fields[RFBSHRT_0]            = new Field(&m_registers[  3],15, 1,0);
  m_fields[ADRVR_LOGAIN_0]       = new Field(&m_registers[  4], 0, 1,0);
  m_fields[ARMREF_DACZ_0]        = new Field(&m_registers[  4], 1, 1,0);
  m_fields[ARMREF_OVD_0]         = new Field(&m_registers[  4], 2, 1,0);
  m_fields[ARMHYSTPD_0]          = new Field(&m_registers[  4], 3, 1,0);
  m_fields[ARMREF_CTRL_0]        = new Field(&m_registers[  4], 4, 7,0);
  m_fields[ZDC_SCAP2_0]          = new Field(&m_registers[  4],11, 5,0);
  m_fields[ZDC_SCAP1_1]          = new Field(&m_registers[  5], 2, 6,0);
  m_fields[OC_LOOPPD_1]          = new Field(&m_registers[  5], 7, 1,0);
  m_fields[OC_GTPD_1]            = new Field(&m_registers[  5], 8, 1,0);
  m_fields[DELAY_CTRL_1]         = new Field(&m_registers[  5], 9, 4,0);
  m_fields[AMP2GAIN_CTRL_1]      = new Field(&m_registers[  5],13, 2,0);
  m_fields[RFBSHRT_1]            = new Field(&m_registers[  5],15, 1,0);
  m_fields[ADRVR_LOGAIN_1]       = new Field(&m_registers[  6], 0, 1,0);
  m_fields[ARMREF_DACZ_1]        = new Field(&m_registers[  6], 1, 1,0);
  m_fields[ARMREF_OVD_1]         = new Field(&m_registers[  6], 2, 1,0);
  m_fields[ARMHYSTPD_1]          = new Field(&m_registers[  6], 3, 1,0);
  m_fields[ARMREF_CTRL_1]        = new Field(&m_registers[  6], 4, 7,0);
  m_fields[ZDC_SCAP2_1]          = new Field(&m_registers[  6],11, 5,0);
  m_fields[ZDC_SCAP1_2]          = new Field(&m_registers[  7], 2, 6,0);
  m_fields[OC_LOOPPD_2]          = new Field(&m_registers[  7], 7, 1,0);
  m_fields[OC_GTPD_2]            = new Field(&m_registers[  7], 8, 1,0);
  m_fields[DELAY_CTRL_2]         = new Field(&m_registers[  7], 9, 4,0);
  m_fields[AMP2GAIN_CTRL_2]      = new Field(&m_registers[  7],13, 2,0);
  m_fields[RFBSHRT_2]            = new Field(&m_registers[  7],15, 1,0);
  m_fields[ADRVR_LOGAIN_2]       = new Field(&m_registers[  8], 0, 1,0);
  m_fields[ARMREF_DACZ_2]        = new Field(&m_registers[  8], 1, 1,0);
  m_fields[ARMREF_OVD_2]         = new Field(&m_registers[  8], 2, 1,0);
  m_fields[ARMHYSTPD_2]          = new Field(&m_registers[  8], 3, 1,0);
  m_fields[ARMREF_CTRL_2]        = new Field(&m_registers[  8], 4, 7,0);
  m_fields[ZDC_SCAP2_2]          = new Field(&m_registers[  8],11, 5,0);
  m_fields[ZDC_SCAP1_3]          = new Field(&m_registers[  9], 2, 6,0);
  m_fields[OC_LOOPPD_3]          = new Field(&m_registers[  9], 7, 1,0);
  m_fields[OC_GTPD_3]            = new Field(&m_registers[  9], 8, 1,0);
  m_fields[DELAY_CTRL_3]         = new Field(&m_registers[  9], 9, 4,0);
  m_fields[AMP2GAIN_CTRL_3]      = new Field(&m_registers[  9],13, 2,0);
  m_fields[RFBSHRT_3]            = new Field(&m_registers[  9],15, 1,0);
  m_fields[ADRVR_LOGAIN_3]       = new Field(&m_registers[ 10], 0, 1,0);
  m_fields[ARMREF_DACZ_3]        = new Field(&m_registers[ 10], 1, 1,0);
  m_fields[ARMREF_OVD_3]         = new Field(&m_registers[ 10], 2, 1,0);
  m_fields[ARMHYSTPD_3]          = new Field(&m_registers[ 10], 3, 1,0);
  m_fields[ARMREF_CTRL_3]        = new Field(&m_registers[ 10], 4, 7,0);
  m_fields[ZDC_SCAP2_3]          = new Field(&m_registers[ 10],11, 5,0);

  m_names={
    "attenuator", 
    "ch0_lumi", "ch1_lumi", "ch2_lumi", "ch3_lumi", 
    "ch0_abort", "ch1_abort","ch2_abort", "ch3_abort", 
    "ch0_polarity", "ch1_polarity", "ch2_polarity", "ch3_polarity",
    "ch0_testen", "ch1_testen", "ch2_testen", "ch3_testen",
    "ibiastrim", "ibiasen",
    "ch0_zdc_scap1", "ch1_zdc_scap1", "ch2_zdc_scap1", "ch3_zdc_scap1",
    "ch0_oc_looppd", "ch1_oc_looppd", "ch2_oc_looppd", "ch3_oc_looppd",
    "ch0_oc_gtpd", "ch1_oc_gtpd", "ch2_oc_gtpd", "ch3_oc_gtpd", 
    "ch0_delay_ctrl", "ch1_delay_ctrl", "ch2_delay_ctrl", "ch3_delay_ctrl",
    "ch0_amp2gain_ctrl", "ch1_amp2gain_ctrl", "ch2_amp2gain_ctrl", "ch3_amp2gain_ctrl",
    "ch0_rfbshrt", "ch1_rfbshrt", "ch2_rfbshrt", "ch3_rfbshrt", 
    "ch0_adrvr_logain", "ch1_adrvr_logain", "ch2_adrvr_logain", "ch3_adrvr_logain",
    "ch0_armref_dacz", "ch1_armref_dacz", "ch2_armref_dacz", "ch3_armref_dacz", 
    "ch0_armref_ovd", "ch1_armref_ovd", "ch2_armref_ovd", "ch3_armref_ovd",
    "ch0_armhystpd", "ch1_armhystpd", "ch2_armhystpd", "ch3_armhystpd",
    "ch0_armref_ctrl", "ch1_armref_ctrl", "ch2_armref_ctrl", "ch3_armref_ctrl",
    "ch0_zdc_scap2", "ch1_zdc_scap2", "ch2_zdc_scap2", "ch3_zdc_scap2",
    "lvds_bias"
  };
}

Configuration_C::~Configuration_C(){
  for(auto it : m_fields){
    delete it.second;
  }
  m_fields.clear();
}

void Configuration_C::SetVerbose(bool enable){
  m_verbose = enable;
}

uint32_t Configuration_C::Size(){
  return m_registers.size();
}

Register * Configuration_C::GetRegister(uint32_t index){
  if(index>m_registers.size()-1){return 0;}
  return &m_registers[index];
}

uint16_t Configuration_C::GetRegisterValue(uint32_t index, bool update){
  if(index>m_registers.size()-1){return 0;}
  if(update) m_registers[index].Update(false);
  return m_registers[index].GetValue();
}

void Configuration_C::SetRegisterValue(uint32_t index, uint16_t value){
  if(index>m_registers.size()-1){return;}
  m_registers[index].SetValue(value);
}

void Configuration_C::SetField(string name, uint16_t value){
  auto it=m_name2field.find(name);
  if(it==m_name2field.end()){
    if(m_verbose) cout << "Configuration_C::SetField WARNING!!! - Reached end of name2index dictionary" << endl;
    return;
  }
  m_fields[it->second]->SetValue(value);
}

void Configuration_C::SetField(uint32_t index, uint16_t value){
  m_fields[index]->SetValue(value);
}

Field * Configuration_C::GetField(uint32_t index){
  return m_fields[index];
}

Field * Configuration_C::GetField(string name){
  auto it=m_name2field.find(name);
  if(it==m_name2field.end()){return 0;}
  return m_fields[it->second];
}

vector<uint32_t> Configuration_C::GetUpdatedRegisters(){
  vector<uint32_t> ret;
  for(uint32_t i=1;i<m_registers.size();i++){
    if(m_registers[i].IsUpdated()){ret.push_back(i);m_registers[i].Update(false);}
  }
  if(m_registers[0].IsUpdated()){ret.push_back(0);m_registers[0].Update(false);}
  return ret;
}

map<string,uint32_t> Configuration_C::GetFields(){
  map<string,uint32_t> ret;
  for(auto name : m_names){
    if(m_fields.count(m_name2field[name])==0){
      cout << "Skipping register: " << name << " => " << m_name2field[name] << endl;
      continue;
    }
    ret[name]=m_fields[m_name2field[name]]->GetValue();
  }
  return ret;
}


void Configuration_C::SetFields(map<string,uint32_t> fields){
  for(auto it : fields){
    SetField(it.first,it.second);
  }
}
