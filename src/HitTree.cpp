#include <BCMP/HitTree.h>

using namespace std;
using namespace BCMP;

HitTree::HitTree(){

  m_ToT=0;
  m_ToA=0;
  m_BCID=0;
  m_L1ID=0;
  m_xL1ID=0;
  m_L1SubID=0; 

}

HitTree::HitTree(string name,string title){
  m_name=name;
  m_title=title;

  m_ToT=0;
  m_ToA=0;
  m_BCID=0;
  m_L1ID=0;
  m_xL1ID=0;
  m_L1SubID=0; 


  Create(name,title);
}

void HitTree::Create(string name, string title){
  m_tree = new TTree(name.c_str(),title.c_str());
  m_tree->Branch("ToT", &m_ToT);
  m_tree->Branch("ToA", &m_ToA);
  m_tree->Branch("BCID", &m_BCID);
  m_tree->Branch("L1ID", &m_L1ID);
  m_tree->Branch("L1ID", &m_xL1ID);
  m_tree->Branch("L1SubID", &m_L1SubID);
  m_tree->Branch("timer",&m_timer,"timer/f");

  m_t0 = chrono::steady_clock::now();
}

HitTree::~HitTree(){}

void HitTree::Fill(Hit * hit){


  m_ToT=hit->GetTOT();
  m_ToA=hit->GetTOA();
  m_BCID=hit->GetBCID();
  m_L1ID=hit->GetL1ID();
  m_xL1ID=hit->GetL1ID();
  m_L1SubID=hit->GetL1SubID();
  m_timer = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now()-m_t0).count()/1000.;
  m_tree->Fill();
}

int HitTree::Next(){
  if(m_tree->LoadTree(m_entry)<0) return 0;
  m_tree->GetEntry(m_entry);
  m_entry++;
  return m_entry;
}

Hit * HitTree::Get(){
  m_hit.SetTOT(m_ToT);
  m_hit.SetTOA(m_ToA);
  m_hit.SetBCID(m_BCID);
  m_hit.SetL1ID(m_L1ID);
  m_hit.SetL1SubID(m_L1SubID);
  return &m_hit;
}


uint64_t HitTree::GetEntries(){
  return m_tree->GetEntries();
}
void HitTree::GetEntry(Long64_t entry){
  m_tree->GetEntry(entry);
}


void HitTree::Write(){
  m_tree->Write();
}

