#include <cmdl/cmdargs.h>
#include <iostream>
#include <fstream>
//#include <BCMP/Configuration_C.h>
#include <BCMP/Configuration_D.h>
#include <nlohmann/json.hpp>
#include <iomanip>
using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;
using namespace std;
//using namespace BCMP;

using Configuration = Configuration_D;
//using Configuration = Configuration_C;

int main(int argc, char *argv[]){

  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr cInput('i',"input","file","Input file (json or txt)",CmdArg::isREQ);
  CmdArgStr cOutput('o',"output","file","Output file (json or txt). Optional");
  CmdArgStr cFieldName('k',"key","name","field name");
  CmdArgInt cFieldValue('V',"value","value","field value");

  CmdLine cmdl(*argv,&cVerbose,&cInput,&cOutput,&cFieldName,&cFieldValue,NULL);
  
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  string ifile(cInput);
  bool ijson=(ifile.find(".json")!=string::npos);
  
  ifstream fr(ifile);
  if(!fr){cout << "Cannot open input file: " << ifile << endl; return 1;}
  if(cVerbose) cout << "Loading config file: " << ifile << endl;
 
  Configuration config;
  if(ijson){ 
    json jconfig;
    fr >> jconfig;
    config.SetFields(jconfig["Calypso"]["config"]);
  }else{
    string line;
    uint32_t r=0;
    while (getline(fr,line)){
      if(cVerbose) cout << "Process line: " << line << endl;
      uint16_t v=0;
      uint32_t i=0;
      for(auto c : line){
	if (c!='0' and c!='1') continue;
	v+=(c=='1')<<i;
	i++;
	if(i>15){i=0;}
      }
      if(cVerbose) cout << "Load register: " << setw(2) << r << " value: 0x" << hex << v << dec << endl;
      config.SetRegisterValue(r,v);
      r++;
    }
  }
  fr.close();
  
  if(cFieldName.flags() & CmdArg::GIVEN and cFieldValue.flags() & CmdArg::GIVEN){
    cout << "Change " << cFieldName << ": ";
    if(!config.GetField(string(cFieldName))){
      cout << "Field not found" << endl;
    }else{
      cout<< config.GetField(string(cFieldName))->GetValue() << " => " << cFieldValue << endl;
    }
    config.SetField(string(cFieldName),cFieldValue);
  }

  for(auto [k,v] : config.GetFields()){
    cout << setw(20) << k << ": " << v << endl;
  }

  if(cOutput.flags() & CmdArg::GIVEN){
    string ofile(cOutput);
    bool ojson=(ofile.find(".json")!=string::npos);
  
    ofstream fw(ofile);
    if(!fw){cout << "Cannot open input file: " << ofile << endl; return 1;}
    if(cVerbose) cout << "Loading config file: " << ofile << endl;
 
    if(ojson){ 
      json jconfig;
      jconfig["Calypso"]["config"]=config.GetFields();
      fw << setw(4) << jconfig;
    }else{
      for(uint32_t r=0;r<config.Size();r++){
	string line;
	uint16_t v=config.GetRegisterValue(r);
	for(uint32_t i=0; i<16; i++){
	  if(i!=0) line+=",";
	  line+=((v>>i)&1?"1":"0");
	}
	fw << line << endl;
      }
    }
    fw.close();
  }

  cout << "Have a nice day" << endl;
  return 0;
}
