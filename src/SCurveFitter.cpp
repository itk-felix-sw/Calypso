#include "BCMP/SCurveFitter.h"
#include <algorithm>
#include <iostream>

using namespace Fitter;

//FIXME this should go somewhere else
#define TOL 1e-30 /* smallest value allowed in cholesky_decomp() */

SCurveFitter::SCurveFitter(){
  m_xvals = NULL;
  m_yvals = NULL;
  m_pars = new double[3];
}

SCurveFitter::~SCurveFitter(){
  delete[] m_xvals;
  delete[] m_yvals;
  delete[] m_pars;
}

void SCurveFitter::setDataFromHist(TH1* h){

  //if nonexistant, create arrays
  if( m_xvals == NULL && m_yvals == NULL ){
    m_npoints = h->GetNbinsX();
    m_xvals = new double[m_npoints];
    m_yvals = new double[m_npoints];
  }
 
  //otherwise overwrite values
  for(unsigned int i = 0; i < m_npoints; i++){
    m_xvals[i] = h->GetXaxis()->GetBinCenter(i+1);
    m_yvals[i] = h->GetBinContent(i+1);
  }
  
}


void SCurveFitter::setDataFromHistIgnoreBin(TH1* h, float skipbin){

  //if nonexistant, create arrays
  if( m_xvals == NULL && m_yvals == NULL ){
    m_npoints = h->GetNbinsX();
    m_xvals = new double[m_npoints];
    m_yvals = new double[m_npoints];
  }
 
  //otherwise overwrite values
  unsigned index=0;
  for(unsigned int i = 0; i < m_npoints; i++){

    float binCent = h->GetBinCenter(i+1);
    float binWidth = h->GetBinWidth(i+1)/2;
    if ( (binCent+binWidth)> skipbin and (binCent-binWidth)<skipbin) {

      std::cout<<"skipping bin: "<<i<<" "<<binCent<<" "<<binWidth<<std::endl;
      continue; //Skip the given bin
    }
    m_xvals[index] = h->GetXaxis()->GetBinCenter(i+1);
    m_yvals[index] = h->GetBinContent(i+1);
    index++;
  }
  
}


double SCurveFitter::getClosest(double v1, double v2, double t){

  if( fabs(v1 - t) < fabs(v2 - t) ) return v1;
  else return v2;

}

int SCurveFitter::findClosestPos(double* arr, int n, double target){
  
  // if (target <= arr[0] ) return arr[0];
  // if (target >= arr[n-1] ) return arr[n-1];

  //binary search
  int i = 0, j = n, mid = 0;

  while (i < j) {
    mid = (i + j) / 2;
    //std::cout<<"Mid is at: "<<mid<<" arr: "<<arr[mid]<<std::endl;
    if (arr[mid] == target)
      return mid;
 
    if (target > arr[mid]) {
      if (mid > 0 && target < arr[mid - 1]){
	if( fabs(arr[mid - 1] - target) > fabs(arr[mid] - target) ){
	  return mid - 1;
	}else{
	    return mid;
	}
      }
      j = mid;
    }else {
	if (mid < n - 1 && target > arr[mid + 1]){
	  if( fabs(arr[mid + 1] - target) > fabs(arr[mid] - target) ){
	    return mid + 1;
	  }else{
	    return mid;
	  }
	}
      i = mid + 1;
    }
  }

  return mid;
}


void SCurveFitter::guessInitialParameters(){

  //max is guess for norm
  double max = *std::max_element(m_yvals, m_yvals + m_npoints);
  m_curve.norm = max;

  //get intersection with half as guess for mu
  int xmid = findClosestPos(m_yvals, m_npoints, max/2.0);
  m_curve.mu = m_xvals[xmid];

  //get difference with 2.2% as guess for sigma
  double val2p2 = max*0.022;
  int x2p2 = findClosestPos(m_yvals, m_npoints, val2p2);
  m_curve.sigma = fabs((m_xvals[xmid] - m_xvals[x2p2])/1.41421356237);
  
  
}


double SCurveFitter::getFunctionValue(double x, double* par){


  double xarg = -1*(x - par[0])/(1.41421356237 * par[1]);
  return par[2]*0.5*(1.0 + m_erf.get_val(xarg));

  // double xarg = (x - par[0])/(1.41421356237 * par[1]);
  // return par[2]*0.5*(1.0 + m_erf.get_val(xarg));


  
}


void SCurveFitter::getDerivatives(double x, double* par, double* g){
  
  //par0 = mu
  //par1 = sigma
  //par2 = norm

  double xarg = -1*(x - par[0])/(1.41421356237 * par[1]);
  double sqrttwopi = 2.50662827463;
  double phi = exp( -1*(x-par[0])*(x-par[0])/(2*par[1]*par[1]) );

  g[0] = par[2]/(sqrttwopi*par[1]) * phi;
  g[1] = -1*par[2]*(par[0]-x)/(sqrttwopi*par[1]*par[1]) * phi;
  g[2] = 0.5*(1.0 + m_erf.get_val(xarg));

  // g[0]=0;
  // g[1]=0;
  // g[2]=0;

  // double xarg = (x - par[0])/(1.41421356237 * par[1]);
  // double sqrttwopi = 2.50662827463;
  // double phi = exp( -1.*(x-par[0])*(x-par[0])/(2*par[1]*par[1]) );

  // g[0] = -1*par[2]/(sqrttwopi*par[1]) * phi;
  // g[1] = par[2]*(par[0] - x)/(sqrttwopi*par[1]*par[1]) * phi;
  // g[2] = 0.5*(1.0 + m_erf.get_val(xarg));









  
}


void SCurveFitter::solve_axb_cholesky(int n, double l[3][3], double* x, double* b){

  int i,j;
  double sum;

  /* solve L*y = b for y (where x[] is used to store y) */
  for (i=0; i<n; i++) {
    sum = 0;
    for (j=0; j<i; j++)
      sum += l[i][j] * x[j];
    x[i] = (b[i] - sum)/l[i][i];
  }

  /* solve L^T*x = y for x (where x[] is used to store both y and x) */
  for (i=n-1; i>=0; i--) {
    sum = 0;
    for (j=i+1; j<n; j++)
      sum += l[j][i] * x[j];
    x[i] = (x[i] - sum)/l[i][i];
  }
}


int SCurveFitter::cholesky_decomp(int n, double l[3][3], double a[3][3]){

  int i,j,k;
  double sum;

  for (i=0; i<n; i++) {
    for (j=0; j<i; j++) {
      sum = 0;
      for (k=0; k<j; k++)
	sum += l[i][k] * l[j][k];
      l[i][j] = (a[i][j] - sum)/l[j][j];
    }

    sum = 0;
    for (k=0; k<i; k++)
      sum += l[i][k] * l[i][k];
    sum = a[i][i] - sum;
    
    if (sum<TOL) return 1; /* not positive-definite */
    l[i][i] = sqrt(sum);
    
  }
  return 0;
}

void SCurveFitter::levmarq_init(){

  //FIXME make this tuneable?
  m_lmstat.verbose = 1;
  m_lmstat.max_it = 20;
  m_lmstat.init_lambda = 0.00001;
  m_lmstat.up_factor = 10;
  m_lmstat.down_factor = 10;
  m_lmstat.target_derr = 1e-12;

}


double SCurveFitter::chi2(){
  
  double res = 0.0;
  double chi2 = 0.0;
  
  for(unsigned int i = 0; i < m_npoints; i++){
    res = getFunctionValue(m_xvals[i], m_pars) - m_yvals[i];
    //std::cout<<"ind: "<<i<<" Res: "<<res<<" Val: "<<getFunctionValue(m_xvals[i], m_pars)<<" - "<<m_yvals[i]<<" Par: "<<m_pars[0]<<" "<<m_pars[1]<<" "<<m_pars[2]<<std::endl;
    chi2 += res*res;
  }

  return chi2;

}


int SCurveFitter::fit(){
  
  //slightly adapted from https://gist.github.com/rbabich/3539146
  //FIXME: maybe we can rely here on publically available algorithms? e.g. if we don't care about external dependencies, Eigen has quick ways of doing this...

  levmarq_init();

  m_pars[0] = m_curve.mu;
  m_pars[1] = m_curve.sigma;
  m_pars[2] = m_curve.norm;

  const int npar = 3;

  unsigned int x,i,j,it,nit,ill,verbose;
  double lambda,up,down,mult,err,newerr,derr,target_derr;
  double h[npar][npar],ch[npar][npar];
  double g[npar],d[npar],delta[npar];
  
  verbose = m_lmstat.verbose;
  nit = m_lmstat.max_it;
  lambda = m_lmstat.init_lambda;
  up = m_lmstat.up_factor;
  down = 1/m_lmstat.down_factor;
  target_derr = m_lmstat.target_derr;
  derr = newerr = 0; 

  err = chi2();

  for (it=0; it<nit; it++) {
    /* calculate the approximation to the Hessian and the "derivative" d */
    for (i=0; i<npar; i++) {
      d[i] = 0;
      for (j=0; j<=i; j++)
	h[i][j] = 0;
    }

    for (x=0; x<m_npoints; x++) {
      getDerivatives(m_xvals[x], m_pars, g);
      for (i=0; i<npar; i++) {
	d[i] += (m_yvals[x] - getFunctionValue(m_xvals[x], m_pars))*g[i];
	for (j=0; j<=i; j++)
	  h[i][j] += g[i]*g[j];


	
      }
    }

    /*  make a step "delta."  If the step is rejected, increase
	lambda and try again */
    mult = 1 + lambda;
    ill = 1; /* ill-conditioned? */
    while (ill && (it<nit)) {
      for (i=0; i<npar; i++)
	h[i][i] = h[i][i]*mult;

      ill = cholesky_decomp(npar, ch, h);
      if (!ill) {
	solve_axb_cholesky(npar, ch, delta, d);
	for (i=0; i<npar; i++)
	  m_pars[i] = m_pars[i] + delta[i];
	newerr = chi2();

	derr = newerr - err;
	ill = (derr > 0);
      } 
      
      if (verbose){
	std::cout << "it = " << it << ", lambda = " << lambda << ", newerr = " << newerr <<", err = " << err << ", derr = " << derr << std::endl;
      }

      if (ill) {
	mult = (1 + lambda*up)/(1 + lambda);
	lambda *= up;
	it++;
      }
    }
    
    err = newerr;
    lambda *= down;  

    if ((!ill)&&(-derr<target_derr)) break;
  }

  m_lmstat.final_it = it;
  m_lmstat.final_err = err;
  m_lmstat.final_derr = derr;

  m_curve.mu = m_pars[0];
  m_curve.sigma = m_pars[1];
  m_curve.norm = m_pars[2];

  return (it==nit);

}
