#include "BCMP/Configuration_D.h"
#include <iostream>
using namespace std;



std::map<std::string, uint32_t> Configuration_D::m_name2index={
  {"attenuator", ATTENUATOR},
  {"ch0_lumi_amp_enable", LUMI_AMP_ENABLE_0},
  {"ch0_lumi_cfd_enable", LUMI_CFD_ENABLE_0},
  {"ch0_lumi_ana_enable", LUMI_ANA_ENABLE_0},
  {"ch0_lumi_dig_enable", LUMI_DIG_ENABLE_0},

  {"ch1_lumi_amp_enable", LUMI_AMP_ENABLE_1},
  {"ch1_lumi_cfd_enable", LUMI_CFD_ENABLE_1},
  {"ch1_lumi_ana_enable", LUMI_ANA_ENABLE_1},
  {"ch1_lumi_dig_enable", LUMI_DIG_ENABLE_1},

  {"ch2_lumi_amp_enable", LUMI_AMP_ENABLE_2},
  {"ch2_lumi_cfd_enable", LUMI_CFD_ENABLE_2},
  {"ch2_lumi_ana_enable", LUMI_ANA_ENABLE_2},
  {"ch2_lumi_dig_enable", LUMI_DIG_ENABLE_2},

  {"ch3_lumi_amp_enable", LUMI_AMP_ENABLE_3},
  {"ch3_lumi_cfd_enable", LUMI_CFD_ENABLE_3},
  {"ch3_lumi_ana_enable", LUMI_ANA_ENABLE_3},
  {"ch3_lumi_dig_enable", LUMI_DIG_ENABLE_3},

  {"ch0_abort_amp_enable", ABORT_AMP_ENABLE_0},
  {"ch0_abort_cfd_enable", ABORT_CFD_ENABLE_0},
  {"ch0_abort_ana_enable", ABORT_ANA_ENABLE_0},
  {"ch0_abort_dig_enable", ABORT_DIG_ENABLE_0},

  {"ch1_abort_amp_enable", ABORT_AMP_ENABLE_1},
  {"ch1_abort_cfd_enable", ABORT_CFD_ENABLE_1},
  {"ch1_abort_ana_enable", ABORT_ANA_ENABLE_1},
  {"ch1_abort_dig_enable", ABORT_DIG_ENABLE_1},

  {"ch2_abort_amp_enable", ABORT_AMP_ENABLE_2},
  {"ch2_abort_cfd_enable", ABORT_CFD_ENABLE_2},
  {"ch2_abort_ana_enable", ABORT_ANA_ENABLE_2},
  {"ch2_abort_dig_enable", ABORT_DIG_ENABLE_2},

  {"ch3_abort_amp_enable", ABORT_AMP_ENABLE_3},
  {"ch3_abort_cfd_enable", ABORT_CFD_ENABLE_3},
  {"ch3_abort_ana_enable", ABORT_ANA_ENABLE_3},
  {"ch3_abort_dig_enable", ABORT_DIG_ENABLE_3},

  {"ch0_polarity", POLARITY_0},
  {"ch1_polarity", POLARITY_1},
  {"ch2_polarity", POLARITY_2},
  {"ch3_polarity", POLARITY_3},
  {"ch0_testen", TEST_ENABLE_0},
  {"ch1_testen", TEST_ENABLE_1},
  {"ch2_testen", TEST_ENABLE_2},
  {"ch3_testen", TEST_ENABLE_3},
  {"ibiastrim", IBIAS_TRIM},
  {"ibiasen", IBIAS_ENABLE},
  {"ch0_zdc_scap1", ZDC_SCAP1_0},
  {"ch1_zdc_scap1", ZDC_SCAP1_1},
  {"ch2_zdc_scap1", ZDC_SCAP1_2},
  {"ch3_zdc_scap1", ZDC_SCAP1_3},
  {"ch0_oc_looppd", OC_LOOPPD_0},
  {"ch1_oc_looppd", OC_LOOPPD_1},
  {"ch2_oc_looppd", OC_LOOPPD_2},
  {"ch3_oc_looppd", OC_LOOPPD_3},
  {"ch0_oc_gtpd", OC_GTPD_0},
  {"ch1_oc_gtpd", OC_GTPD_1},
  {"ch2_oc_gtpd", OC_GTPD_2},
  {"ch3_oc_gtpd", OC_GTPD_3},
  {"ch0_delay_ctrl", DELAY_CTRL_0},
  {"ch1_delay_ctrl", DELAY_CTRL_1},
  {"ch2_delay_ctrl", DELAY_CTRL_2},
  {"ch3_delay_ctrl", DELAY_CTRL_3},
  {"ch0_amp2gain_ctrl", AMP2GAIN_CTRL_0},
  {"ch1_amp2gain_ctrl", AMP2GAIN_CTRL_1},
  {"ch2_amp2gain_ctrl", AMP2GAIN_CTRL_2},
  {"ch3_amp2gain_ctrl", AMP2GAIN_CTRL_3},
  {"ch0_rfbshrt", RFBSHRT_0},
  {"ch1_rfbshrt", RFBSHRT_1},
  {"ch2_rfbshrt", RFBSHRT_2},
  {"ch3_rfbshrt", RFBSHRT_3},
  {"ch0_adrvr_logain", ADRVR_LOGAIN_0},
  {"ch1_adrvr_logain", ADRVR_LOGAIN_1},
  {"ch2_adrvr_logain", ADRVR_LOGAIN_2},
  {"ch3_adrvr_logain", ADRVR_LOGAIN_3},
  {"ch0_armref_dacz", ARMREF_DACZ_0},
  {"ch1_armref_dacz", ARMREF_DACZ_1},
  {"ch2_armref_dacz", ARMREF_DACZ_2},
  {"ch3_armref_dacz", ARMREF_DACZ_3},
  {"ch0_armref_ovd", ARMREF_OVD_0},
  {"ch1_armref_ovd", ARMREF_OVD_1},
  {"ch2_armref_ovd", ARMREF_OVD_2},
  {"ch3_armref_ovd", ARMREF_OVD_3},
  {"ch0_armhystpd", ARMHYSTPD_0},
  {"ch1_armhystpd", ARMHYSTPD_1},
  {"ch2_armhystpd", ARMHYSTPD_2},
  {"ch3_armhystpd", ARMHYSTPD_3},
  {"ch0_armref_ctrl", ARMREF_CTRL_0},
  {"ch1_armref_ctrl", ARMREF_CTRL_1},
  {"ch2_armref_ctrl", ARMREF_CTRL_2},
  {"ch3_armref_ctrl", ARMREF_CTRL_3},
  {"ch0_zdc_scap2", ZDC_SCAP2_0},
  {"ch1_zdc_scap2", ZDC_SCAP2_1},
  {"ch2_zdc_scap2", ZDC_SCAP2_2},
  {"ch3_zdc_scap2", ZDC_SCAP2_3},
  {"atten",ATTEN},
  {"VREF_SEL",VREF_SEL},
  {"VLOW_SEL",VLOW_SEL},
  {"STEP",STEP},
  {"lvds_bias", LVDS_BIAS}
};

Configuration_D::Configuration_D(){

    
  m_verbose=false;
  m_backup.resize(m_registers.size());
}


Configuration_D::Configuration_D( const Configuration_D& obj) {
  for (unsigned i=0;i<m_registers.size();i++){
    this->SetRegisterValue(i,obj.GetRegisterValue(i));
  }
}


Configuration_D::~Configuration_D(){
}

void Configuration_D::SetVerbose(bool enable){
  m_verbose = enable;
}

uint32_t Configuration_D::Size(){
  return m_registers.size();
}

Register<uint16_t> * Configuration_D::GetRegister(uint32_t index){
  if(index>m_registers.size()-1){return 0;}
  return &m_registers[index];
}

uint16_t Configuration_D::GetRegisterValue(uint32_t index) const{
  if(index>m_registers.size()-1){return 0;}
  return m_registers[index].GetValue();
}

uint16_t Configuration_D::GetRegisterValue(uint32_t index, bool update){
  if(index>m_registers.size()-1){return 0;}
  if(update) m_registers[index].Update(false);
  return m_registers[index].GetValue();
}

void Configuration_D::SetRegisterValue(uint32_t index, uint16_t value){
  if(index>m_registers.size()-1){return;}
  m_registers[index].SetValue(value);
}

void Configuration_D::SetField(string name, uint16_t value){

  if(m_verbose) std::cout << __PRETTY_FUNCTION__ << "Setting field: " << name << ", " << value << std::endl;
  auto it=m_name2index.find(name);
  if(it==m_name2index.end()){
    if(m_verbose) std::cout << __PRETTY_FUNCTION__ << " WARNING!!! - Reached end of name2index dictionary" << std::endl;
    return;
  }
  else{
    //    if(m_verbose) std::cout << __PRETTY_FUNCTION__ << " Found in dictionary: " << it -> first << ", " << it -> second << std::endl;
  }
  m_fields[it->second].SetValue(value);
}

void Configuration_D::SetField(uint32_t index, uint16_t value){
  m_fields[index].SetValue(value);
}

Field<uint16_t> * Configuration_D::GetField(uint32_t index){
  return &m_fields[index];
}

Field<uint16_t> * Configuration_D::GetField(string name){
  auto it=m_name2index.find(name);
  if(it==m_name2index.end()){return 0;}
  return &m_fields[it->second];
}

vector<uint32_t> Configuration_D::GetUpdatedRegisters(){
  vector<uint32_t> ret;
  for(uint32_t i=1;i<m_registers.size();i++){
    if(m_registers[i].IsUpdated()){ret.push_back(i);m_registers[i].Update(false);}
  }
  if(m_registers[0].IsUpdated()){ret.push_back(0);m_registers[0].Update(false);}
  return ret;
}

map<string,uint32_t> Configuration_D::GetFields(){
  map<string,uint32_t> ret;
  for(auto it: m_name2index){
    string name=it.first;
    ret[name]=m_fields[m_name2index[name]].GetValue();
  }
  return ret;
}

void Configuration_D::SetFields(map<string,uint32_t> config){
  for(auto it : config){
    if(m_verbose) std::cout << __PRETTY_FUNCTION__ << "Setting field: " << it.first << ", " << it.second << std::endl;
    SetField(it.first,it.second);
  }
}

void Configuration_D::Backup(){
  
  for(uint32_t i=0; i<m_registers.size(); i++){
    m_backup[i]=m_registers[i].GetValue();
  }
  
}

void Configuration_D::Restore(){

  for(uint32_t i=0; i<m_registers.size(); i++){
    if(m_backup[i]==m_registers[i].GetValue()){continue;}
    m_registers[i].SetValue(m_backup[i]);
  }
  
}
