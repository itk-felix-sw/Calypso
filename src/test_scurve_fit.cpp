#include "BCMP/SCurveFitter.h"
#include "TROOT.h"
#include "TFile.h"
#include "TH1I.h"
#include "iostream"
#include <cmdl/cmdargs.h>
#include "string"

int main(int argc, char *argv[]){


  CmdArgStr  cFile('f',"file","file","file",CmdArg::isREQ);
  CmdArgStr  cHist('h',"histname","histname","histname",CmdArg::isREQ);

  CmdLine cmdl(*argv,&cFile,&cHist,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  std::string File=(cFile.flags()&CmdArg::GIVEN?cFile:"/home/itkpix/data/000763_Threshold/output.root");
  std::string HistName=(cHist.flags()&CmdArg::GIVEN?cHist:"Thr_vs_hits_BCM1_1");
  
  TFile rFile = TFile(File.c_str(),"open");
  TH1I *Hist = (TH1I*) rFile.Get(HistName.c_str());

  Fitter::SCurveFitter scf;
  


  float PreMean, mean ;
  float PreSigma, sigma;
  float max;

  uint8_t trialCount=0;

  std::cout<<"Before Smoothing"<<std::endl;
  int nBins = Hist->GetNbinsX();
  for (int bin = 1; bin <= nBins; ++bin) { // Start from bin 1, as bin 0 is underflow
    double binCenter = Hist->GetBinCenter(bin);
    double content = Hist->GetBinContent(bin);
    double error = Hist->GetBinError(bin);

    std::cout << bin << "\t"
	      << binCenter << "\t\t"
	      << content << "\t\t"
	      << error << std::endl;
  }
  do{
    scf.setDataFromHistIgnoreBin(Hist,130);
    scf.guessInitialParameters();
    PreMean = scf.getSCurve().mu;
    PreSigma = scf.getSCurve().sigma;
    max = scf.getSCurve().norm;

    std::cout<<"SCurve Pre Fit: Guess "<<PreMean<<" +- "<<PreSigma<<" of max value: "<<max<<std::endl;
    scf.fit();
  
    mean = scf.getSCurve().mu;
    sigma = scf.getSCurve().sigma;

    Hist->Smooth();
    
    
    trialCount++;
    std::cout<<"SCurve Fit: "<<mean<<" +- "<<sigma<<" trialCount "<<int(trialCount)<<std::endl;
  }while(trialCount<=3 and ( ( (sigma>mean) || (fabs(PreMean - mean)> 2*PreSigma) ) or sigma or mean)  );
    
  std::cout<<"After Smoothing"<<std::endl;
  nBins = Hist->GetNbinsX();
  for (int bin = 1; bin <= nBins; ++bin) { // Start from bin 1, as bin 0 is underflow
    double binCenter = Hist->GetBinCenter(bin);
    double content = Hist->GetBinContent(bin);
    double error = Hist->GetBinError(bin);

    std::cout << bin << "\t"
	      << binCenter << "\t\t"
	      << content << "\t\t"
	      << error << std::endl;
  }
  


}
