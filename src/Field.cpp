#include "BCMP/Field.h"


template<typename T >
std::vector<uint32_t> Field<T>::m_masks = {
  0x0000,
  0x0001,0x0003,0x0007,0x000F,
  0x001F,0x003F,0x007F,0x00FF,
  0x01FF,0x03FF,0x07FF,0x0FFF,
  0x1FFF,0x3FFF,0x7FFF,0xFFFF,
};

template<typename T >
Field<T>::Field(Register<T> * data, uint32_t start, uint32_t len, uint32_t defval, bool reversed){
  m_data.push_back( { data, start, len} );
  m_defval=defval;
  m_reversed=reversed;
  SetValue(m_defval);
}

template<typename T >
Field<T>::Field(std::vector<Subs> OtherFields, uint32_t defval,bool reversed){
  m_data.insert(m_data.end(),OtherFields.begin(),OtherFields.end());

  m_defval=defval;
  m_reversed=reversed;
  SetValue(m_defval);
}






template<typename T >
Field<T>::~Field(){}






template<typename T >
uint32_t Field<T>::Reverse(uint32_t value, uint32_t sz){
  uint32_t ret=0;
  for(uint32_t i=0;i<sz;i++){
    ret |= ((value>>i)&0x1)<<(sz-i-1);
  }
  //cout << "sz:" << sz << " value:" << value << " reversed:" << ret << endl;
  return ret;
}

template<typename T >
void Field<T>::SetValue(uint32_t value){
  uint32_t loc=0;
  uint32_t sze =0;
  for(unsigned i=0; i<m_data.size();i++)
    sze += m_data.at(i).len;
  
  if(m_reversed){value=Reverse(value,sze);}
  for(int8_t i=(m_data.size()-1); i>=0;i--){
    uint32_t tmpVal = m_data.at(i).Reg->GetValue();
    tmpVal&=~(m_masks[m_data.at(i).len]<<m_data.at(i).start);
    tmpVal|=(((value>>loc)& m_masks[m_data.at(i).len])<<m_data.at(i).start);  
    m_data.at(i).Reg->SetValue(tmpVal);
    loc += m_data.at(i).len;
  }
}

template<typename T >
uint32_t Field<T>::GetValue(){
  uint32_t value=0;
  uint32_t loc=0;
  
  for(int8_t i=(m_data.size()-1); i>=0;i--){
    value += ((m_data.at(i).Reg->GetValue()>>m_data.at(i).start)&m_masks[m_data.at(i).len] )<<loc;
    loc += m_data.at(i).len;
  }
  if(m_reversed){value=Reverse(value,loc);}
  return value;
}


template class Field<uint8_t>;
template class Field<uint16_t>;
template class Field<uint32_t>;

