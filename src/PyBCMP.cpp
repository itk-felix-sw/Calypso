#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
//#include <BCMP/Configuration_C.h>
#include <BCMP/Configuration_D.h>



namespace py = pybind11;
//using namespace BCMP;

using Configuration = Configuration_D;
//using Configuration = Configuration_C;



// PYBIND11_MODULE(libPyBCMP, m) {

//   m.doc() = "Python binding for BCMP.";

//   py::class_<Configuration>(m, "Configuration")
//     .def(py::init())
//     .def("SetVerbose", &Configuration::SetVerbose)
//     .def("Size", &Configuration::Size)
//     .def("GetRegister", &Configuration::GetRegister)
//     .def("GetRegisterValue", &Configuration::GetRegisterValue)
//     .def("SetRegisterValue", &Configuration::SetRegisterValue)
//     .def("SetField", py::overload_cast<std::string, uint16_t>(&Configuration::SetField))
//     .def("SetField", py::overload_cast<uint32_t, uint16_t>(&Configuration::SetField))
//     .def("GetField", py::overload_cast<uint32_t>(&Configuration::GetField))
//     .def("GetField", py::overload_cast<std::string>(&Configuration::GetField))
//     .def("GetUpdatedRegisters", &Configuration::GetUpdatedRegisters)
//     .def("GetFields", &Configuration::GetFields)
//     .def("SetFields", &Configuration::SetFields);
// };
