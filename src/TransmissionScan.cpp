#include "BCMP/TransmissionScan.h"

#include <iostream>
#include <chrono>
#include <signal.h>


using namespace std;
using namespace BCMP;

bool TransmissionScan::m_RunForever = false;

TransmissionScan::TransmissionScan(){

}

TransmissionScan::~TransmissionScan(){}

void TransmissionScan::PreRun(){
  cout << "PreRun()" << endl;
  for(auto fe : GetSensors()){
    cout << "TransmissionScan::PreRun" << endl;

    m_h_L1ID[fe->GetName()]=new TH1D(("L1ID_"+fe->GetName()).c_str(),";L1ID",8388608,-0.5,8388607.5);
    m_h_L1SubID[fe->GetName()]=new TH1I(("L1SubID_"+fe->GetName()).c_str(),";L1SubID",64,-0.5,63.5);



    cout << "fe name" << fe->GetName() << endl;
  }
  m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
}

void TransmissionScan::StopScan(int){
  cout << endl << "You pressed ctrl+c -> quitting" << endl;
  m_RunForever = false;
}


void TransmissionScan::Run(){

  //Prepare the trigger
  
  signal(SIGINT, StopScan);

  cout << "Clearing Hits"<<std::endl;
  for(auto se : GetSensors()){
    se->ClearHits();
  }


  cout << "TransmissionScan::Run: Start" << endl;
  
  m_RunForever=true;

  m_MissedHitCount=0;
  m_HitCount=0;

  // uint32_t old_ToT=0;
  // uint32_t old_ToA=0;
  // uint32_t old_BCID=0;
  uint64_t old_L1ID=0;
  uint64_t old_L1SubID=0;

  static uint64_t L1IDMax=16777215;

  int nH=0;
  vector<Hit*> hits;
  auto start = chrono::steady_clock::now();
  auto end = chrono::steady_clock::now();

  while(m_RunForever){
    for(auto sensor : GetSensors()){

      

      hits=sensor->GetHits(30000);
      nH=hits.size();

      if (nH==0) {
	this_thread::sleep_for(chrono::milliseconds(100)); // 500 Millisconds is the time to collect 20000 hits
      }
      else {
	//std::cout<<"Got nH: "<<nH<<std::endl;
	for (unsigned int h=0; h<nH; h++) {
	  Hit* hit = hits.at(h);

	  // uint32_t ToT=hit->GetTOT();
	  // uint32_t ToA=hit->GetTOA();
	  // uint32_t BCID=hit->GetBCID();
	  uint32_t L1ID=hit->GetL1ID();
	  uint32_t L1SubID=hit->GetL1SubID();
      
	  // m_h_L1ID[sensor->GetName()]->Fill(L1ID);
	  // m_h_L1SubID[sensor->GetName()]->Fill(L1SubID);


	  m_HitCount+=1;
	  if( m_HitCount> 1 and ( ((old_L1ID+1)!=( L1ID + (L1IDMax+1)*(L1ID==0)))  and (old_L1ID!=L1ID) ) )
	    {
	      
	      end = chrono::steady_clock::now();

	      m_MissedHitCount++;
	      std::cout<<"nH: "<<nH<<" Packet lost Packet / Prev Packet: "
		       <<L1ID<<" / "<<old_L1ID<<" - "
		       <<L1SubID<<" / "<<old_L1SubID;//<<" - "
		       // <<ToT<<" / "<<old_ToT<<" - "
		       // <<ToA<<" / "<<old_ToA<<" - "
		       // <<BCID<<" / "<<old_BCID;
	      std::cout<<" Time since last data-loss: "<<chrono::duration_cast<chrono::milliseconds>(end - start).count()<<std::endl;
	      std::cout<<"Total Miss/ Total Hits: "<<m_MissedHitCount<<" / "<<m_HitCount<<std::endl;
	  

	      start = chrono::steady_clock::now();

	    }
	  old_L1ID=L1ID;
	  // old_ToT=ToT;
	  // old_ToA=ToA;
	  // old_BCID=BCID;
	  old_L1SubID=L1SubID;

	  if ((m_HitCount)%1000000000==0 && m_HitCount>0) 
	    std::cout<<"Total Miss/ Total Hits: "<<m_MissedHitCount<<" / "<<m_HitCount<<std::endl;

	}
	sensor->ClearHits(nH);
      }
    }
  }
}

void TransmissionScan::Analysis(){

  std::cout<<"Data transmission scan is complete:"<<std::endl;
  std::cout<<"Total Miss/ Total Hits: "<<m_MissedHitCount<<" / "<<m_HitCount<<std::endl;  


  for(auto fe : GetSensors()){

    m_h_L1ID[fe->GetName()]->Write();
    m_h_L1SubID[fe->GetName()]->Write();

    delete m_h_L1ID[fe->GetName()];
    delete m_h_L1SubID[fe->GetName()];

  }
 
}                                                                                                                                        
