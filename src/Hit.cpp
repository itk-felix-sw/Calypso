#include "BCMP/Hit.h"

using namespace BCMP;

Hit::Hit(){
  m_tot=0;
  m_toa=0;
  m_BCID=0;
  m_L1ID=0;
  m_L1SubID=0;
}


Hit::Hit(uint8_t toa, uint8_t tot, uint16_t BCID, uint32_t L1ID, uint8_t L1SubID){
  m_toa=toa;
  m_tot=tot;
  m_BCID=BCID;
  m_L1ID=L1ID;
  m_L1SubID=L1SubID;
}

Hit::~Hit(){}

Hit* Hit::Clone(){
  Hit * hit = new Hit(m_toa,m_tot,m_BCID,m_L1ID, m_L1SubID);
  return hit;
}

uint8_t Hit::GetTOT(){
  return m_tot;
}

uint8_t Hit::GetTOA(){
  return m_toa;
}

uint16_t Hit::GetBCID(){
  return m_BCID;
}

uint32_t Hit::GetL1ID(){
  return m_L1ID;
}
uint8_t Hit::GetL1SubID(){
  return m_L1SubID;
}



void Hit::SetTOT(uint8_t tot){
  m_tot=tot;
}

void Hit::SetTOA(uint8_t toa){
  m_toa=toa;
}


void Hit::SetBCID(uint16_t BCID){
  m_BCID=BCID;
}

void Hit::SetL1ID(uint32_t L1ID){
  m_L1ID=L1ID;
}

void Hit::SetL1SubID(uint8_t L1SubID){
  m_L1SubID=L1SubID;
}



void Hit::Update(uint8_t toa, uint8_t tot, uint16_t BCID, uint32_t L1ID, uint8_t L1SubID){
  m_toa=toa;
  m_tot=tot;
  m_BCID=BCID;
  m_L1ID = L1ID;
  m_L1SubID = L1SubID;
}

