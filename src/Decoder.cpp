#include "BCMP/Decoder.h"
#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace BCMP;

Decoder::Decoder(){
  m_bytes.reserve(1000000);
  m_bytes.resize(1000000,0);
  m_length = 0;
  m_fD=new DataFrame();
}

Decoder::~Decoder(){
  m_bytes.clear();
  ClearFrames();
  delete m_fD;
}

void Decoder::AddBytes(uint8_t *bytes, uint32_t pos, uint32_t len){
  for(uint32_t i=pos;i<pos+len;i++){
    m_bytes[m_length]=bytes[i];
    m_length++;
  }
}

void Decoder::SetBytes(uint8_t *bytes, uint32_t len, bool reversed){
  m_length=len;
  if(len>m_bytes.size()){m_bytes.resize(len);}
  if(!reversed){for(uint32_t i=0;i<len;i++){m_bytes[i]=bytes[i];}}
  else         {for(uint32_t i=0;i<len;i++){m_bytes[len-i-1]=bytes[i];}}
}

void Decoder::AddFrame(DataFrame *frame){
  m_frames.push_back(frame);
}
  
void Decoder::ClearBytes(){
  m_length=0;
}
  
void Decoder::ClearFrames(){
  while(!m_frames.empty()){
    DataFrame* frame = m_frames.back();
    delete frame;
    m_frames.pop_back();
  }
}

void Decoder::Clear(){
  ClearBytes();
  ClearFrames();
}
  
string Decoder::GetByteString(){
  ostringstream os;
  //os << "length=" << m_length << " ";
  os << hex; 
  for(uint32_t pos=0; pos<m_length; pos++){
    os << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << " ";
  }
  return os.str();
}

void Decoder::GetBytes(uint8_t * bytes, uint32_t & length, bool reversed){
  length=m_length;
  if(!reversed){for(uint32_t i=0;i<m_length;i++){bytes[i]=m_bytes[i];}}
  else         {for(uint32_t i=0;i<m_length;i++){bytes[m_length-i-1]=m_bytes[i];}}
}

uint8_t * Decoder::GetBytes(bool reversed){
  if(!reversed){return m_bytes.data();}
  m_rbytes.resize(m_length);
  for(uint32_t i=0;i<m_length;i++){
    m_rbytes[m_length-i-1]=m_bytes[i];
  }
  return m_rbytes.data();
}
  
uint32_t Decoder::GetLength(){
  return m_length;
}
  
vector<DataFrame*> & Decoder::GetFrames(){
  return m_frames;
}

void Decoder::Encode(){
  uint32_t pos=0;
  for(uint32_t i=0;i<m_frames.size();i++){
    pos+=m_frames[i]->Pack(&m_bytes[pos]);
  }
  m_length=pos;
}

void Decoder::Decode(const bool verbose){
  ClearFrames();
  uint32_t pos=0;
  uint32_t nb=0;
  uint32_t tnb=m_length;
  DataFrame * frame=0;

  while(pos<tnb){
    if((nb=m_fD->UnPack(&m_bytes[pos],tnb-pos))>0){
      if(verbose) cout << "Decoder::Decode new data frame" << endl;
      frame=m_fD; m_fD=new DataFrame();
    }
    else{
      cout << __PRETTY_FUNCTION__ << "Cannot decode byte sequence: "
	   << "0x" << hex << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << dec
	   << " at index: " << pos
	   << " ...skipping" << endl;
      pos++;
      continue;
    }
    if(!frame){pos++; continue;}
    m_frames.push_back(frame);
    pos+=nb;
    if(verbose) {cout << "Decoder::Decode pos=" << pos << " tnb=" << tnb << endl;}
  }

}
